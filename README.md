# SofTiSim

[![pipeline status](https://gitlab.com/benzwick/softisim/badges/master/pipeline.svg)](https://gitlab.com/benzwick/softisim/-/commits/master)
[![coverage report](https://gitlab.com/benzwick/softisim/badges/master/coverage.svg)](https://gitlab.com/benzwick/softisim/-/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://benzwick.gitlab.io/softisim/)

Soft Tissue Simulator

## Downloading

Clone the repository using [Git](https://git-scm.com):

    git clone https://gitlab.com/benzwick/softisim.git

## Installing

SofTiSim is developed using the
[Julia](https://julialang.org)
programming language.

You can install SofTiSim using Julia's package manager:

    pkg> add https://gitlab.com/benzwick/softisim#master

Alternatively, you can install your local version:

    pkg> add path/to/softisim#master

where `master` is the name of the branch to install.

For help with installing packages, please refer to the Julia
[documentation](https://julialang.github.io/Pkg.jl/v1/managing-packages).

## Testing

To run all tests:

    pkg> test SofTiSim

See the [runtests.jl](./test/runtests.jl) file for environment variables
that can be used to change the test behaviour.

## Running

SofTiSim can be used like any other Julia package.
Simply start Julia, `import SofTiSim` and start running.

Alternatively, to run a demo as a script:

    julia -i --color=yes path_to_demo.jl

or with all optimisations turned on:

    julia -i --color=yes -O3 --inline=yes --check-bounds=no path_to_demo.jl

By default, Julia starts up with a single thread of execution.
To use multi-threading the environment variable `JULIA_NUM_THREADS`
must be defined before starting Julia, for example as follows:

    export JULIA_NUM_THREADS=4

## Copying

SofTiSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SofTiSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Funding

This project was supported by an
Australian Government Research Training Program (RTP) Scholarship.
The financial support of the Australian Research Council
(ARC Discovery Project Grants DP120100402 and DP160100714)
and National Health and Medical Research Council
(NHMRC Grant APP1144519)
is gratefully acknowledged.

## Citing

If you use SofTiSim for your research,
please consider adding the following citations:

- Zwick, B.F. (2020)
  Numerical Methods and Software for Soft Tissue Simulations,
  PhD thesis, The University of Western Australia, Perth, WA.

- Zwick, B.F. (2020)
  SofTiSim: Soft Tissue Simulator.
  Available at: https://gitlab.com/benzwick/softisim

BibTeX:

    @PhdThesis{zwick_2020_phd,
      author  = {Benjamin F. Zwick},
      title   = {Numerical Methods and Software for Soft Tissue Simulations},
      school  = {The University of Western Australia},
      address = {Perth, WA},
      year    = 2020,
    }

    @Misc{zwick_2020_softisim,
      author =   {Benjamin F. Zwick},
      title =    {{SofTiSim}: {S}oft {T}issue {S}imulator},
      howpublished = {\url{https://gitlab.com/benzwick/softisim}},
      year = 2020
    }
