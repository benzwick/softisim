# SofTiSim

SofTiSim is (not only) a Soft Tissue Simulator.

# Documentation

```@contents
```

```@meta
CurrentModule = SofTiSim
```

## Geometry

### Elements

Abstract geometrical elements.
```@docs
Point
Elem
```

Abstract elements.
```@docs
Edge
Face
Cell
```

```@docs
Tet
Hex
```

```@docs
Hex8
Hex20
Tet4
Tet10
```

### Properties

The following properties are constant for each element type.
```@docs
n_dims
n_nodes
n_sides
n_edges
n_children
n_nodes_per_side
n_nodes_per_edge
reference_coordinates
reference_coordinates_as_matrix
reference_coordinates_as_points
reference_element
```

### Functions

```@docs
plot_element
volume
```

## Mesh

```@docs
meshbox
```

## IO

```@docs
readmesh
```

## Material

```@docs
Material
NeoHookean
Ogden
spkstress
elastic_moduli
```

## Boundary Conditions

```@docs
DirichletBC
apply!
smooth345
```

## Assembly
```@docs
assemble_force!
compute_and_scatter_force!
compute_displacement_gradient
```

## Postprocess

```@docs
nrmse
exact_cube_uniaxial
```

# Index

## Functions

```@index
Order   = [:function]
```

## Types

```@index
Order   = [:type]
```
