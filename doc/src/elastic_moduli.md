# Elastic moduli for homogeneous isotropic materials

* Bulk modulus (<math>K</math>)
* Young's modulus (<math>E</math>)
* Lamé's first parameter (<math>\lambda</math>)
* Shear modulus (<math>G, \mu</math>)
* Poisson's ratio (<math>\nu</math>)
* P-wave modulus (<math>M</math>)

## Conversion formulae

Homogeneous isotropic linear elastic materials
have their elastic properties uniquely determined
by any two moduli among these;
thus, given any two, any other of the elastic moduli
can be calculated according to these formulas.

See: [`elastic_moduli`](@ref)
