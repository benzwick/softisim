using Documenter, SofTiSim

# See: https://juliadocs.github.io/Documenter.jl/stable/man/doctests/#Module-level-metadata-1
setup = :(using SofTiSim; import Logging; Logging.disable_logging(Logging.Warn))
DocMeta.setdocmeta!(SofTiSim, :DocTestSetup, setup; recursive=true)

makedocs(sitename = "SofTiSim",
         repo = "https://gitlab.com/benzwick/softisim/blob/{commit}{path}#{line}",
         modules = [SofTiSim],
         linkcheck = true)
