# To ensure correct image size when batch processing, please search
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
# from paraview.simple import *
import paraview.simple as ps
#### disable automatic camera reset on 'Show'
ps._DisableFirstRenderCameraReset()

# get active view
renderView1 = ps.GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [795, 791]

# Properties modified on renderView1
renderView1.CameraParallelProjection = 1

# get active source.
source = ps.GetActiveSource()

# create a new 'Warp By Vector' (displacement)
warpByDisplacement = ps.WarpByVector(Input=source)
warpByDisplacement.Vectors = ['POINTS', 'u_h']
warpByDisplacementDisplay = ps.Show(warpByDisplacement, renderView1)
warpByDisplacementDisplay.SetRepresentationType('Wireframe')

# create a new 'Glyph' (force)
glyphForce = ps.Glyph(Input=warpByDisplacement, GlyphType='Arrow')
glyphForce.OrientationArray = ['POINTS', 'f_t2']
glyphForce.ScaleArray = ['POINTS', 'f_t2']
glyphForce.ScaleFactor = -4.0
glyphForce.GlyphTransform = 'Transform2'
glyphForce.GlyphMode = 'All Points'
# show data in view
glyphForceDisplay = ps.Show(glyphForce, renderView1)
# set scalar coloring
ps.ColorBy(glyphForceDisplay, ('POINTS', 'f_t2', 'Magnitude'))
# rescale color and/or opacity maps used to include current data range
glyphForceDisplay.RescaleTransferFunctionToDataRange(True, False)
# show color bar/color legend
glyphForceDisplay.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()
