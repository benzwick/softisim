include("../init.jl")

# Indentation of a Cylinder

model = Dict()

#------------------------------------------------------------------------------
# Configuration for mesh-free methods

# Shape functions
model[:use_exact_SF_derivatives] = true
model[:use_base_functions] = 2
model[:support_radius_dilatation] = 1.2
model[:use_variable_support_radius] = true

# Essential Boundary Condition Impostion
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = true

# Adaptive numerical integration
model[:use_adaptive_integration] = false
model[:number_of_tetrahedral_divisions] = 4
model[:integration_eps] = 0.1
model[:quadrature_degree] = 2

model[:scale_mass] = true
# using Statistics
# model[:mass_scaling_timestep] = dt -> mean(dt)

#------------------------------------------------------------------------------
# Other configurations
model[:progress_num_steps] = 1000

#------------------------------------------------------------------------------
# Solver

using SofTiSim: DynamicRelaxationParameters

solver = DynamicRelaxationParameters(
    time_load = 0.5,
    time_eq = 1.0,
    time_increment_safety_factor = 4,
)

#------------------------------------------------------------------------------
# Materials

using SofTiSim: Ogden, elastic_moduli

model[:density] = 1000
alpha = -1.10
mu = 643.6
D1 = 1.2598*1e-004
kappa = 2/D1
model[:material] = Ogden(alpha, mu, kappa)

#------------------------------------------------------------------------------
# Mesh

using SofTiSim: readmesh

# Is this the right mesh?
mesh = readmesh("../mesh/MAbaqusIndent-February.inp")

# TODO: move meshes
# mesh = readmesh("../swelling_cylinder/mesh_new_indent/cylindent_339_default.inp")
# mesh = readmesh("../swelling_cylinder/mesh_new_indent/cylindent_1867.inp")
# mesh = readmesh("../mesh/CompressMFebruary.inp")

#------------------------------------------------------------------------------
# Dirichlet boundary conditions

using SofTiSim: DirichletBC, smooth345

max_disp = -0.0125

# Circle-1: Phi * x * ubar
# with Phi = r^2
# circ_disp = 1.37e-3 * 0.25 * max_disp
# circ_disp = -0.003003
# circ_disp = 0.75 * max_disp

rc1 = 1.37e-3

bcs = (
    fixed_bot_xyz = DirichletBC(
        nodes = mesh.nsets["Fixed"], dofs = 1:3,
    ),
#     displace_circle_1_x = DirichletBC(
#         nodes = mesh.nsets["Circle-1"], dofs = 1:1,
#         magnitude = (x, t, g, n) -> (-x[1]/norm([x[1], x[2]]) *
#                                      rc1 * smooth345(t / solver.time_load))
#     ),
#     displace_circle_1_y = DirichletBC(
#         nodes = mesh.nsets["Circle-1"], dofs = 2:2,
#         magnitude = (x, t, g, n) -> (-x[2]/norm([x[1], x[2]]) *
#                                      rc1 * smooth345(t / solver.time_load))
#     ),
    fix_Displaced = DirichletBC(
        nodes = mesh.nsets["Displaced"], dofs = 1:2,
    ),
    displace_Displaced = DirichletBC(
        nodes = mesh.nsets["Displaced"], dofs = 3:3,
        magnitude = (x, t, g, n) -> max_disp * smooth345(t / solver.time_load),
    ),
)

#------------------------------------------------------------------------------
# Output

using SofTiSim: OutputFieldVtk, OutputHistory

output_field = OutputFieldVtk(interval=200)

output_history = OutputHistory(
    user_functions = (
        # displacement
        disp_min             = G -> minimum(G.disp[mesh.nsets["Displaced"],3]),
        disp_max             = G -> maximum(G.disp[mesh.nsets["Displaced"],3]),
        # forc_tot
        forc_tot_Fixed_1     = G -> sum(G.forc_tot[mesh.nsets["Fixed"],1]),
        forc_tot_Fixed_2     = G -> sum(G.forc_tot[mesh.nsets["Fixed"],2]),
        forc_tot_Fixed_3     = G -> sum(G.forc_tot[mesh.nsets["Fixed"],3]),
        forc_tot_Displaced_1 = G -> sum(G.forc_tot[mesh.nsets["Displaced"],1]),
        forc_tot_Displaced_2 = G -> sum(G.forc_tot[mesh.nsets["Displaced"],2]),
        forc_tot_Displaced_3 = G -> sum(G.forc_tot[mesh.nsets["Displaced"],3]),
        forc_tot_Circle1_1   = G -> sum(G.forc_tot[mesh.nsets["Circle-1"],1]),
        forc_tot_Circle1_2   = G -> sum(G.forc_tot[mesh.nsets["Circle-1"],2]),
        forc_tot_Circle1_3   = G -> sum(G.forc_tot[mesh.nsets["Circle-1"],3]),
        # forc_ebc
        forc_ebc_Fixed_1     = G -> sum(G.forc_ebc[mesh.nsets["Fixed"],1]),
        forc_ebc_Fixed_2     = G -> sum(G.forc_ebc[mesh.nsets["Fixed"],2]),
        forc_ebc_Fixed_3     = G -> sum(G.forc_ebc[mesh.nsets["Fixed"],3]),
        forc_ebc_Displaced_1 = G -> sum(G.forc_ebc[mesh.nsets["Displaced"],1]),
        forc_ebc_Displaced_2 = G -> sum(G.forc_ebc[mesh.nsets["Displaced"],2]),
        forc_ebc_Displaced_3 = G -> sum(G.forc_ebc[mesh.nsets["Displaced"],3]),
        forc_ebc_Circle1_1   = G -> sum(G.forc_ebc[mesh.nsets["Circle-1"],1]),
        forc_ebc_Circle1_2   = G -> sum(G.forc_ebc[mesh.nsets["Circle-1"],2]),
        forc_ebc_Circle1_3   = G -> sum(G.forc_ebc[mesh.nsets["Circle-1"],3]),
    ),
)

#------------------------------------------------------------------------------
# Solve with MTLED

include("../../src/mtled.jl")

#------------------------------------------------------------------------------
# Postprocess

# TODO: nrmse of u on EBC - should be almost exact (checked in paraview)

# pb is the new results
# au is the gold standard
# if error(pb) < error(au) in file
    # write csv
    # jobname * .au.new.timestamp
# print("Congratulations, you found n bits of gold!")
# print please see file...
