julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_535.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_535_ebciem.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_6710.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_6710_ebciem.jl --overwrite=true
