
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # TODO: name figure with PROGRAM_NAME as the prefix to see easlily all demo outputs!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

from postproc import data_from_csv, rmse
import numpy as np
import matplotlib.pyplot as plt

jobs = [
    # # Before EBCIEM bugfix
    # # 'swelling_cylinder_neohookean_535_ebciem_pv',
    # # 'swelling_cylinder_neohookean_6710_ebciem_pv',
    # # After EBCIEM bugfix
    # 'swelling_cylinder_neohookean_535_ebciem_pv_BUGFIX',
    # 'swelling_cylinder_neohookean_6710_ebciem_pv_BUGFIX',
    # # 'cylinder_indentation_1.2_ip4',

    'swelling_cylinder_neohookean_535_ebciem',
    'swelling_cylinder_neohookean_6710_ebciem',
]

# TODO: make these a single dictionary instead with label as key?
# or better a list of dicts?
nnodes_list = []
radius_list = []
coord_z_list = []
ux_list = []
uy_list = []
uz_list = []
ur_list = []
forc_int_bot_list = []
forc_int_top_list = []
forc_int_radial_bot_list = []
forc_int_radial_top_list = []
forc_int_radial_free_list = []
forc_int_z_free_list = []
for job in jobs:

    print(80*'=')
    print(job)

    data = data_from_csv(job + "/" + job + '.field.csv')

    radius = data['radius']
    height = data['height']

    # TODO: plot sum force total?
    # TODO: compare top and bottom total reaction forces
    # top = data['surf_top']
    # bot = data['surf_bot']
    # force_int = data['forc_int_z'][top]
    # forc_int_top = sum

    # Average and deviation of displacements at the midline
    # IMPORTANT
    # TODO: make functions plot(**data) that takes data returned from postproc as keyword arguments

    if True:
        nset = data['surf_mid']
        r = data['r'][nset]
        z = data['z'][nset]
        ux = data['ux'][nset]
        uy = data['uy'][nset]
        uz = data['uz'][nset]
        ur = data['ur'][nset]
        del nset
    else:
        r = data['r']
        z = data['z']
        ux = data['ux']
        uy = data['uy']
        uz = data['uz']
        ur = data['ur']

    forc_int_top = data['forc_int'][data['surf_cyl_top'], :]
    forc_int_bot = data['forc_int'][data['surf_cyl_bot'], :]
    forc_int_free = data['forc_int'][data['surf_not_top_or_bot'], :]
    forc_int_radial_top = data['forc_int_radial'][data['surf_cyl_top']]
    forc_int_radial_bot = data['forc_int_radial'][data['surf_cyl_bot']]
    forc_int_radial_free = data['forc_int_radial'][data['surf_not_top_or_bot']]
    forc_int_z_free = data['forc_int_radial'][data['surf_not_top_or_bot']]
    h = 0.05

    print('num nodes: ', len(z), '('+str(len(z)/len(data['z'])*100)+' %)')

    # ur
    print(80*'-')
    print('ur mean: ', np.mean(ur))
    print('ur std: ', np.std(ur))

    # uz
    print(80*'-')
    print('uz error norm: ', np.linalg.norm(uz))
    # print('uz RMS error: ', rmse(uz, 0))
    print('uz min: ', min(uz))
    print('uz max: ', max(uz))
    print('uz mean: ', np.mean(uz))
    print('uz std: ', np.std(uz))

    # z
    print(80*'-')
    print('z min: ', min(z))
    print('z max: ', max(z))
    print('z mean: ', np.mean(z))
    print('z std: ', np.std(z))
    print('z error norm: ', np.linalg.norm(z-h))
    # print('z RMS error: ', rmse(z, h))

    nnodes = len(data['x'])
    nnodes_list.append(nnodes)
    radius_list.append(r)
    coord_z_list.append(z)
    ux_list.append(ux)
    uy_list.append(uy)
    uz_list.append(uz)
    ur_list.append(ur)
    forc_int_bot_list.append(forc_int_bot)
    forc_int_top_list.append(forc_int_top)
    forc_int_radial_bot_list.append(forc_int_radial_bot)
    forc_int_radial_top_list.append(forc_int_radial_top)
    forc_int_z_free_list.append(forc_int_radial_free)
    forc_int_radial_free_list.append(forc_int_radial_free)

import matplotlib as mpl
sf = mpl.ticker.EngFormatter(unit='m')

# boxplot
fig, axs = plt.subplots(1, 2, sharex=True)
axs[0].set_title('$z=h/2$')
# axs[0].boxplot(ux_list); axs[0].set_ylabel('$u_x$')  # makes no sense to plot ux!
# axs[1].boxplot(uy_list); axs[1].set_ylabel('$u_y$')  # makes no sense to plot uy!
axs[0].boxplot(ur_list)
axs[0].set_ylabel('$u_r$')
axs[1].boxplot(uz_list)
axs[1].set_ylabel('$u_z$')
# for debugging num nodes in nset
# axs[2].boxplot(radius_list)
# axs[2].set_ylabel('$r$')
# axs[3].boxplot(coord_z_list)
# axs[3].set_ylabel('$z$')
for ax in axs:
    # ax.set_xlabel('')
    ax.yaxis.set_major_formatter(sf)
fig.savefig('fig_boxplot.pdf')

# Histogram
# fig, axs = plt.subplots(1, 4, figsize=(12, 6))
fig, axs = plt.subplots(1, 2, figsize=(12, 6))
# fig.subplots_adjust(top=-0.1)
fig.subplots_adjust(bottom=0.3)
fig.subplots_adjust(wspace=0.3)
rwidth=0.7
opts = {
    # 'histtype': 'step',
    # 'stacked': True,
    # 'fill': False,
    # 'linewidth': 2,

    # 'stacked': False,
    # 'rwidth': 0.5,
    # 'bins': 5,
    }
# e.g. C3D4 and MTLED with the same mesh
# for now I plot individually to be safe
print('WARNING: only plot data with same number of samples on same plots')
for i in range(len(ur_list)):
    for ax in axs:
        ax.cla()

    # TODO: for interior swelling and for cylinder indentation include also
    # for r = r0/2.5 or whatever the inside radius is that we want to displace
    # especially if there is symmetry but also without
    # (e.g. cylinder indentation)

    # ----------------------------------------------------------------------

    # TODO: plot EBCIEM bcs to show they are exact

    # FIXME: automate this for all plots
    # fig.suptitle('$r=' + str(round(1000*radius)) + '$ mm, $h=' + str(round(1000*height)) + '$ mm, $N_{nodes}=' + str(nnodes_list[i]) + '$')
    fig.suptitle('$r=50$ mm, $h=100$ mm, $N_{nodes}=' + str(nnodes_list[i]) + '$')

    axs[0].set_title('$z=h/2$')
    axs[0].hist(ur_list[i], **opts)
    axs[0].set_xlabel('$u_r$')

    axs[1].set_title('$z=h/2$')
    axs[1].hist(uz_list[i], **opts)
    axs[1].set_xlabel('$u_z$')

#     # FIXME: use actual force not f_t2
#     axs[2].set_title('f_t2 $0 < z < h$ (wrong!)')
#     axs[2].hist(forc_int_radial_free_list[i], **opts)
#     axs[2].set_xlabel('$f_r$')
# 
#     # FIXME: use actual force not f_t2
#     axs[3].set_title('f_t2 $0 < z < h$ (wrong!)')
#     axs[3].hist(forc_int_z_free_list[i], **opts)
#     axs[3].hist(forc_int_radial_free_list[i], **opts)
#     axs[3].set_xlabel('$f_z$')

    # ------------------------------------------------------------------------------

    # axs[2].set_title('Radial force')
    # # axs[2].hist(forc_int_radial_bot_list[i][data['surf_bot']], label='$z=0$', color='lightblue', **opts)
    # # # TODO: Fe
    # # axs[2].hist(forc_int_radial_top_list[i][data['surf_bot']], label='$z=h$', color='darkblue', **opts)
    # axs[2].hist(forc_int_radial_bot_list[i], label='$z=0$', color='pink', **opts)
    # axs[2].hist(forc_int_radial_top_list[i], label='$z=h$', color='darkred', **opts)
    # axs[2].set_xlabel('$f_r=\sqrt{f_x^2 + f_y^2}$')

    # axs[3].set_title('Axial force')
    # # F(h) = -F(0) negative because opposite bottom and top
    # axs[3].hist(forc_int_radial_bot_list[i][2], label='$z=0$', color='pink', **opts)
    # axs[3].hist(forc_int_radial_top_list[i][2], label='$z=h$', color='darkred', **opts)
    # # TODO: Fe
    # axs[3].set_xlabel('$f_z$')

    # ------------------------------------------------------------------------------

    # axs[2].set_title('Radial force')
    # # axs[2].hist(forc_int_radial_bot_list[i][data['surf_bot']], label='$z=0$', color='lightblue', **opts)
    # # # TODO: Fe
    # # axs[2].hist(forc_int_radial_top_list[i][data['surf_bot']], label='$z=h$', color='darkblue', **opts)
    # axs[2].hist(forc_int_radial_top_list[i], label='$z=h$', color='darkblue', **opts)
    # axs[2].set_xlabel('$f_r=\sqrt{f_x^2 + f_y^2}$')

    # axs[3].set_title('Axial force')
    # # F(h) = -F(0) negative because opposite bottom and top
    # axs[3].hist(forc_int_bot_list[i][:, 2], label='$z=0$', color='lightblue', **opts)
    # axs[3].hist(-forc_int_top_list[i][:, 2], label='$z=h$', color='darkblue', **opts)
    # # TODO: Fe
    # axs[3].set_xlabel('$f_z$')

    # ------------------------------------------------------------------------------

    # Others:
    # Forces on circle at top
    # TODO: Use ux and uy etc not ur because ux=uy so they will overlap!
    # just make sure they are symmetric on the surface and equal magnitude e.g. on a line

    for ax in (axs[0:2]):
        ax.xaxis.set_major_formatter(mpl.ticker.EngFormatter(unit='m'))
    for ax in (axs[2:4]):
        ax.xaxis.set_major_formatter(mpl.ticker.EngFormatter(unit='N'))
    for ax in axs:
        pass
        # ax.legend(frameon=False)

    # for debugging num nodes in nset
    # axs[2].hist(radius_list, bins=bins)
    # axs[2].set_xlabel('$r$')
    # axs[3].hist(coord_z_list, bins=bins)
    # axs[3].set_xlabel('$z$')
    for ax in axs:
        ax.tick_params(labelrotation=90)
        ax.set_ylabel('Number of nodes')

    fig.savefig('fig_histogram_' + str(i) + '.pdf', bbox_inches='tight') # better to have some space?


# ------------------------------------------------------------------------------
# Move each plot to separate file
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# TODO: name figure with PROGRAM_NAME as the prefix to see easlily all demo outputs!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
PROGRAM_NAME = 'plot_ur_vs_uz_.py'.split('.')[0]


# TODO: move somewhere near where the jobs are named.
# name jobs consistently so can easlily be split
# e.g. swelling_cylinder_neohookean!n_nodes=553!ebciem=1.jl
def parameters_from_job_name(job):
    n_nodes = job.split('_')[3]
    return locals()


jobs = {
    # # Before and after EBCIEM bugfix
    # 'swelling_cylinder_neohookean_535_ebciem_pv': {
    #     'plot_opts': {'c': 'lightblue', 'marker': 'x'},
    #     'label_prefix': ' (bug)'},
    # 'swelling_cylinder_neohookean_535_ebciem_pv_BUGFIX': {
    #     'plot_opts': {'c': 'darkblue', 'marker': '+'}},
    # 'swelling_cylinder_neohookean_6710_ebciem_pv': {
    #     'plot_opts': {'c': 'lightgreen', 'marker': 'x'},
    #     'label_postfix': ' (bug)'},
    # 'swelling_cylinder_neohookean_6710_ebciem_pv_BUGFIX': {
    #     'plot_opts': {'c': 'darkgreen', 'marker': '+'}},

    'swelling_cylinder_neohookean_535_ebciem': {
        'plot_opts': {'c': 'darkblue', 'marker': '+'}},
    'swelling_cylinder_neohookean_6710_ebciem': {
        'plot_opts': {'c': 'darkgreen', 'marker': '+'}},
}
for job in jobs.keys():
    jobs[job]['plot_opts']['marker'] = None

fig, ax = plt.subplots()
fig.suptitle('Title')

for job, prm in jobs.items():
    print(job)

    p2 = parameters_from_job_name(job)
    label = '$n_{nodes}=$' + p2['n_nodes']

    # Data
    data = data_from_csv(job + "/" + job + '.field.csv')
    x = data['ur'][data['surf_cyl']]
    y = data['uz'][data['surf_cyl']]

    # Plot
    ax.scatter(x, y, label=label, alpha=0.6, s=20, **prm['plot_opts'])
    ax.set_xlabel('$u_r$')
    ax.set_ylabel('$u_z$')
    ax.xaxis.set_major_formatter(mpl.ticker.EngFormatter(unit='m'))
    ax.yaxis.set_major_formatter(mpl.ticker.EngFormatter(unit='m'))
    ax.legend()

fig.savefig(PROGRAM_NAME + '_MTLED' + '.pdf', bbox_inches='tight') # better to have some space?



# plt.show()

    # ------------------------------------------------------------
    # Rotation of nodes around axis
