@info "Postprocess"

using SofTiSim: nrmse
using PyPlot

# simple variable names for plotting
x = getindex.(glob.coord, 1);
y = getindex.(glob.coord, 2);
z = getindex.(glob.coord, 3);
ux = glob.disp[:,1];
uy = glob.disp[:,2];
uz = glob.disp[:,3];

r = sqrt.(x.^2 + y.^2);
ur = sqrt.(ux.^2 + uy.^2);

h = maximum(z)
R = maximum(r)

mid_nodes = (r .>= (R - tol_coords)) .& (h/2-tol_coords .<= z) .& (z .<= h/2+tol_coords)
# debug:
# scatter3D(x[mid_nodes], y[mid_nodes])

# Displacement histograms
fig, ax = subplots(1, 4, figsize=(16, 4))
ax[1].hist(ur[mid_nodes], label="\$u_r\$ (mid)");
ax[2].hist(uz[mid_nodes], label="\$u_z\$ (mid)");
ax[3].hist(uz[top_nodes], label="\$u_z\$ (top)");
ax[4].hist(uz[bot_nodes], label="\$u_z\$ (bot)");
for a in ax
    a.set_xlabel("Displacement")
    a.set_ylabel("Number of nodes")
    a.legend()
    a.set_aspect("auto")
end
savefig(output_filename * ".plot.displacement-histograms.pdf")

# Mass scaling factors
fig, ax = subplots(figsize=(4, 4))
ax.hist(glob.qpoints.mass_scaling_factor, label="\$u_z\$ (bot)")
ax.set_xlabel("Mass scaling factor")
ax.set_ylabel("Number of integration points")
ax.set_title("Mass scaling factors")
savefig(output_filename * ".plot.mass-scaling-factors.pdf")

# Total reaction forces

df = output_history_dataframe

fig, ax = plt.subplots(1, 3, figsize=(12, 4))
ax[1].plot(df.time, df.forc_tot_bot_1, "b:", label="\$f_1^{int}\$ (bottom)")
ax[1].plot(df.time, -df.forc_tot_top_1, "r:", label="\$f_1^{int}\$ (top)")
ax[1].plot(df.time, df.forc_ebc_bot_1, "b--", label="\$f_1^{ebc}\$ (bottom)")
ax[1].plot(df.time, -df.forc_ebc_top_1, "r--", label="\$f_1^{ebc}\$ (top)")
ax[2].plot(df.time, df.forc_tot_bot_2, "b:", label="\$f_2^{int}\$ (bottom)")
ax[2].plot(df.time, -df.forc_tot_top_2, "r:", label="\$f_2^{int}\$ (top)")
ax[2].plot(df.time, df.forc_ebc_bot_2, "b--", label="\$f_2^{ebc}\$ (bottom)")
ax[2].plot(df.time, -df.forc_ebc_top_2, "r--", label="\$f_2^{ebc}\$ (top)")
ax[3].plot(df.time, df.forc_tot_bot_3, "b:", label="\$f_3^{int}\$ (bottom)")
ax[3].plot(df.time, -df.forc_tot_top_3, "r:", label="\$f_3^{int}\$ (top)")
ax[3].plot(df.time, df.forc_ebc_bot_3, "b--", label="\$f_3^{ebc}\$ (bottom)")
ax[3].plot(df.time, -df.forc_ebc_top_3, "r--", label="\$f_3^{ebc}\$ (top)")
for a in ax
    a.legend()
    a.set_xlabel("Time (s)")
    a.set_ylabel("Force (N)")
    a.set_aspect("auto")
end
savefig(output_filename * ".plot.total-reaction-forces.pdf")

using Statistics

@info string("ur (mid):   ", "mean: ", mean(ur[mid_nodes]), "   std: ", std(ur[mid_nodes]))
@info string("uz (mid):   ", "mean: ", mean(uz[mid_nodes]), "   std: ", std(uz[mid_nodes]), "   normInf: ", norm(uz[mid_nodes], Inf), "   norm2: ", norm(uz[mid_nodes], 2))
@info string("uz (bot):   ", "mean: ", mean(uz[top_nodes]), "   std: ", std(uz[top_nodes]), "   normInf: ", norm(uz[top_nodes], Inf), "   norm2: ", norm(uz[top_nodes], 2))
@info string("uz (top):   ", "mean: ", mean(uz[bot_nodes]), "   std: ", std(uz[bot_nodes]), "   normInf: ", norm(uz[bot_nodes], Inf), "   norm2: ", norm(uz[bot_nodes], 2))
