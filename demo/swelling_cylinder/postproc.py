# TODO: 1. create new data frame with renamed columnes
#          a. from_softisim_paraview
#          b. from_abaqus
#       2. add columns to data frame with additional variable names
#          postproc.load(job + '.csv', format='abaqus', compute=('cyl_disp, cyl_forc'))


# This import registers the 3D projection, but is otherwise unused.
# for 3d scatter (not needed?) use something else to plot 3d mesh points for debugging?
# used for debugging
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

# TODO:
# put this in SofTiSim/tools directory

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def rmse(y, Y):
    return np.sqrt(np.mean((y-Y)**2)) / (np.max(y)-np.min(y))


def data_from_csv(csv_file, eps=1e-8):
    """Read data from SofTiSim."""

    df = pd.read_csv(csv_file)

    # Coordinates
    x = df['coord_1'].values
    y = df['coord_2'].values
    z = df['coord_3'].values
    coords = df[['coord_1', 'coord_2', 'coord_3']].values

    # TODO: change this name to
    r = np.sqrt(x**2 + y**2)
    cyl_coords = np.array([r, z])

    # Cylinder dimensions
    radius = max(r)            # 0.05
    height = max(z)             # 0.1

    # These were used temporarily
    # TODO
    # del x, y, z, r

    # Displacements
    ux = df['disp_1'].values
    uy = df['disp_2'].values
    uz = df['disp_3'].values         # axial displacement
    dx = df['dnod_next_1'].values
    dy = df['dnod_next_2'].values
    dz = df['dnod_next_3'].values         # axial displacement
    forc_int = df[['forc_tot_1', 'forc_tot_2', 'forc_tot_3']].values
    forc_int_z = df['forc_tot_3'].values  # delete this after rewrite (use forc_int[2] instead)

    # radial displacement magnitude
    ur = np.sqrt(ux**2 + uy**2)
    forc_int_radial = np.sqrt(forc_int[:, 0]**2 + forc_int[:, 1]**2)
    dr = np.sqrt(dx**2 + dy**2)

    # Nodes on cylindrical surface
    surf_cyl = (radius - eps) < r
    surf_cyl_bot = surf_cyl & (z < eps)
    surf_cyl_top = surf_cyl & (z > height - eps)
    # Nodes NOT on cylindrical surface
    surf_not_cyl = (radius - eps) > r
    # Nodes on cylindrical surface away from ends
    surf_cyl2 = surf_cyl & (0.01 < z) & (z < 0.09)
    # Nodes on top surface
    surf_top = (height - eps) < z
    # Nodes on middle surface
    surf_mid = (height/2-eps < z) & (z < height/2 + eps)
    # Nodes on bottom surface
    surf_bot = z < eps
    # Nodes between top and bottom
    surf_not_top_or_bot = (eps < z) | (z < eps)
    # Nodes along center axis
    surf_axis = (abs(x) < eps) & (abs(y) < eps)

    # For debugging
    # FIXME: move this constant
    DEBUG = False
    if DEBUG:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x, y, z, label='cyl', color='b', marker='x')
        ax.scatter(x[surf_cyl], y[surf_cyl], z[surf_cyl], label='cyl', color='b')
        ax.scatter(x[surf_cyl2], y[surf_cyl2], z[surf_cyl2], label='cyl2', color='b')
        ax.scatter(x[surf_top], y[surf_top], z[surf_top], label='top', color='r')
        ax.scatter(x[surf_mid], y[surf_mid], z[surf_mid], label='mid', color='g')
        ax.scatter(x[surf_bot], y[surf_bot], z[surf_bot], label='bot', color='m')
        plt.show()

    # # Values restricted to cylindrical surface
    # # Coordinates
    # xrs = xr[surf_cyl]
    # xrm = xr[surf_mid]
    # zs = z[surf_cyl]
    # zm = z[surf_mid]
    # # Displacements
    # uxs = ux[surf_cyl]
    # uys = uy[surf_cyl]
    # uzs = uz[surf_cyl]
    # dzs = dz[surf_cyl]
    # urs = ur[surf_cyl]
    # drs = ur[surf_cyl]
    # urm = ur[surf_mid]

    # Deformed nodal coordinates
    x_ux = x + ux; y_uy = y + uy; z_uz = z + uz
    x_dx = x + dx; y_dy = y + dy; z_dz = z + dz
    # radial displacement
    r_ur = np.sqrt(x_ux**2 + y_uy**2)
    r_dr = np.sqrt(x_dx**2 + y_dy**2)
    # r_ur = x_ux + x_uy
    # r_dr = x_dx + x_dy
    # r_ur = radius + ur
    # r_dr = radius + dr
    # r_ur_m = np.sqrt(r_ur**2 + x_uy**2)
    # r_dr_m = np.sqrt(r_dr**2 + x_dy**2)

    # delete this?
    # radius_s = np.sqrt(ux**2 + uy**2)
    # plt.scatter(radius_s, z)
    # radius_s = np.sqrt(uxs**2 + uys**2)
    # zs = z[radx > 0.04999]
    # plt.scatter(radius_s, zs)

    # Forces
    # forc_int_x = df['
    # febc

    return locals()


def scatter(ax, data, x, y, nset=None, label=None, **opts):
    x = data[x]
    y = data[y]
    if nset is not None:
        nset = data[nset]
        x = x[nset]
        y = y[nset]

    ax.scatter(x, y, label=label, **opts)


def quiver(ax, data, x, y, u, v, nset=None, **opts):
    x = data[x]
    y = data[y]
    u = data[u]
    v = data[v]
    if nset is not None:
        nset = data[nset]
        x = x[nset]
        y = y[nset]
        u = u[nset]
        v = v[nset]

    ax.quiver(x, y, u, v,
                angles='xy', scale_units='xy', scale=1,
                **opts)
