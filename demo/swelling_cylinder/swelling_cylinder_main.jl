#------------------------------------------------------------------------------
# Model parameters

# Shape functions
model[:use_exact_SF_derivatives] = true
model[:use_base_functions] = 2
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true

# Adaptive numerical integration
model[:use_adaptive_integration] = false
model[:number_of_tetrahedral_divisions] = 4
model[:integration_eps] = 0.1
model[:quadrature_degree] = 2

model[:scale_mass] = true

model[:progress_num_steps] = 500       # number of iteration after which progress is displayed

#------------------------------------------------------------------------------
# Materials

using SofTiSim: Swelling, NeoHookean, elastic_moduli

model[:density] = 1000
# FIXME: make elastic_moduli return a struct so it cannot be modified accidentaly
em = elastic_moduli(E=3000, nu=0.49)
model[:material] = Swelling(NeoHookean(em),
                            # FIXME: why is this called time?
                            time -> 1 + 1*smooth345(time/solver.time_load)
)

#------------------------------------------------------------------------------
# Dirichlet boundary conditions

using SofTiSim: DirichletBC

height = 0.1
tol_coords = 1e-6
top_nodes = mesh.nodes[findall(getindex.(mesh.coordinates, 3) .>= (height - tol_coords))]
bot_nodes = mesh.nodes[findall(getindex.(mesh.coordinates, 3) .<= tol_coords)]
# why sort? for easy access in vector calcs?
fixed_nodes = sort([bot_nodes..., top_nodes...])

bcs = (
    fixed = DirichletBC(
        nodes = fixed_nodes,
        dofs = 1:3,
    ),
)

#------------------------------------------------------------------------------
# Solve

using SofTiSim: OutputFieldVtk, OutputHistory, DynamicRelaxationParameters

output_field = OutputFieldVtk(interval=200)

output_history = OutputHistory(
    user_functions = (
        # f_tot
        forc_tot_bot_1 = G -> sum(G.forc_tot[bot_nodes,1]),
        forc_tot_bot_2 = G -> sum(G.forc_tot[bot_nodes,2]),
        forc_tot_bot_3 = G -> sum(G.forc_tot[bot_nodes,3]),
        forc_tot_top_1 = G -> sum(G.forc_tot[top_nodes,1]),
        forc_tot_top_2 = G -> sum(G.forc_tot[top_nodes,2]),
        forc_tot_top_3 = G -> sum(G.forc_tot[top_nodes,3]),
        # f_ebc
        forc_ebc_bot_1 = G -> sum(G.forc_ebc[bot_nodes,1]),
        forc_ebc_bot_2 = G -> sum(G.forc_ebc[bot_nodes,2]),
        forc_ebc_bot_3 = G -> sum(G.forc_ebc[bot_nodes,3]),
        forc_ebc_top_1 = G -> sum(G.forc_ebc[top_nodes,1]),
        forc_ebc_top_2 = G -> sum(G.forc_ebc[top_nodes,2]),
        forc_ebc_top_3 = G -> sum(G.forc_ebc[top_nodes,3]),
    ),
)

solver = DynamicRelaxationParameters(time_load = 1, time_eq = 1)

include("../../src/mtled.jl")

#------------------------------------------------------------------------------
# Postprocess

include("post_cylinder_swelling.jl")
