include("../init.jl")

model = Dict()

model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = true

mesh = SofTiSim.readmesh("../mesh/3D_Cylinder_T4_535.inp")

include("swelling_cylinder_main.jl")
