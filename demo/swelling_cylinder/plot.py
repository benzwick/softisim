from postproc import data_from_csv, scatter, quiver
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

jobs = [
    # # Before EBCIEM bugfix
    # 'swelling_cylinder_neohookean_535_ebciem_pv',
    # 'swelling_cylinder_neohookean_6710_ebciem_pv',
    # # After EBCIEM bugfix
    # 'swelling_cylinder_neohookean_535_ebciem_pv_BUGFIX',
    # 'swelling_cylinder_neohookean_6710_ebciem_pv_BUGFIX',
    # # 'cylinder_indentation_1.2_ip4',

    'swelling_cylinder_neohookean_535_ebciem',
    'swelling_cylinder_neohookean_6710_ebciem',
]

colors = ['r', 'g']

opts = {
    's': 15.0,
    # 'linewidths': 0.5,
    # 'marker': 'o',
    # 'alpha': 0.6,
    # 'facecolors': 'none',
    # 'edgecolors': 'black',
}

nset = 'surf_cyl' # 'surf_cyl'

# plt.rcParamsDefault  # check default parameters
# fig, axs = plt.subplots(1, 2, sharex=True, sharey=True) # default size
# fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(6.4, 4.8)) # default size?
# fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(12, 6)) # twice as wide as default
fig, ax = plt.subplots(figsize=(12, 6))
sf = mpl.ticker.EngFormatter(unit='m')
# for ax, job in zip(axs, jobs):
for job in jobs:

    ax.cla()

    data = data_from_csv(job + "/" + job + '.field.csv')

    ax.set_title('MTLED ($n_{nodes}=' + job.split('_')[3] + '$)')

    # x
    scatter(ax, data, 'x', 'z', color='k', alpha=0.1, **opts)

    PLOT_U_H = True
    PLOT_FORC_INT = False       # not working
    PLOT_D = False

    # u_h (this is the actual displacement approximation)
    if PLOT_U_H:
        if PLOT_D:
            label = '$u(x_j) = \Phi_i(x_j) d_i$',
        else:
            # FIXME
            label = 'MTLED'
        scatter(ax, data, 'r_ur', 'z_uz', nset=nset,
                label=label,
                color='darkblue', alpha=0.6, **opts)
        quiver(ax, data, 'r', 'z', 'ur', 'uz', nset=nset,
               # label='$u(x_j) = \Phi_i(x_j) d_i$',
               color='lightblue', alpha=0.2)

    # d_i (these are the nodal coefficients)
    if PLOT_D:
        scatter(ax, data, 'r_dr', 'z_dz', nset=nset,
                label='$u(x_j) = d_j$',
                color='r', alpha=0.6, **opts)
        quiver(ax, data, 'r', 'z', 'dr', 'dz', nset=nset,
               # label='$u(x_j) = d_j$',
               color='r', alpha=0.2)

    # f_int internal force
    # FIXME
    if PLOT_FORC_INT:
        scatter(ax, data, 'cyl_forc_int', 'forc_int_z', nset=nset,
                label='$u(x_j) = d_j$',
                color='r', alpha=0.6, **opts)
        quiver(ax, data, 'r', 'z', 'cyl_forc_int', 'forc_int_z', nset=nset,
               # label='$u(x_j) = d_j$',
               color='r', alpha=0.2)

    ax.set_xlabel('$r = r_0 + \sqrt{u_x^2 + u_y^2}$')
    ax.legend(loc='upper left', frameon=False)
    ax.set_aspect('equal')
    ax.xaxis.set_major_formatter(sf)
    ax.yaxis.set_major_formatter(sf)
    # ax.set_xlim([-0.075, 0.175])
    ax.set_xlim([0, 0.15])
    ax.set_ylim([-0.025, 0.125])
    # ax.set_xticks([-0.05, 0.0, 0.05, 0.1, 0.15])
    ax.set_xticks([0.0, 0.05, 0.1, 0.15])
    ax.set_yticks([0.0, 0.05, 0.1])
    ax.minorticks_on()
    ax.tick_params(which='both') #, direction='in',
                    # bottom=True, top=True, left=True, right=True)

    # axs[0].set_ylabel('$z = z_0 + u_z$')
    ax.set_ylabel('$z = z_0 + u_z$')

    fig.savefig('fig_mtled_ur__' + job + '.pdf', bbox_inches='tight')

# plt.show()
