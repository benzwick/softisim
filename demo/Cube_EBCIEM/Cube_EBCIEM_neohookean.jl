using SofTiSim

# Meshless Simulation : Unconstrained Deformation of a Cube
# EBC Impostion : Using 'EBCIEM/SEBCIEM' or 'Correction' Method

model = Dict()

#------------------------------------------------------------------------------
# Configuration for mesh-free methods

# model[:approximation] = InterpMLS_3D_1
# model[:approximation] = InterpMLS_3D_2

model[:use_exact_SF_derivatives] = true
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true
model[:use_base_functions] = 2

# Essential Boundary Condition Impostion
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = false

# Adaptive numerical integration
model[:use_adaptive_integration] = false
model[:number_of_tetrahedral_divisions] = 4
model[:integration_eps] = 0.1
model[:quadrature_degree] = 2

model[:scale_mass] = true
using Statistics
model[:mass_scaling_timestep] = dt -> mean(dt)

#------------------------------------------------------------------------------
# Other configurations

model[:progress_num_steps] = 500

#------------------------------------------------------------------------------
# Solver

solver = DynamicRelaxationParameters()

#------------------------------------------------------------------------------
# Materials

model[:sections] = (
    all = (
        region = :all,
        density = 1000,
        material = NeoHookean(elastic_moduli(E=3000, nu=0.49))
    ),
)

#------------------------------------------------------------------------------
# Mesh

mesh = readmesh("../mesh/abaqus_mesh_final_cube.inp")

#------------------------------------------------------------------------------
# Dirichlet boundary conditions

model[:bcs] = (
    dx_0 = DirichletBC(
        nodes = mesh.nsets["DX_0"],
        dofs = 1:1,
    ),
    dy_0 = DirichletBC(
        nodes = mesh.nsets["DY_0"],
        dofs = 2:2,
    ),
    dz_0 = DirichletBC(
        nodes = mesh.nsets["DZ_0"],
        dofs = 3:3,
    ),
    disp = DirichletBC(
        nodes = mesh.nsets["DISPLACED"],
        dofs = 3:3,
        magnitude = -0.02,
        amplitude = smooth345,
    ),
)

#------------------------------------------------------------------------------
# Output

using Statistics

model[:output_field] = OutputFieldVtk(interval=200)

model[:output_history] = OutputHistory(
    user_functions = (
        # Average vertical displacement of displaced surface nodes
        disp_displaced = G -> mean(G.disp[mesh.nsets["DISPLACED"], 3]),
        # Reaction forces
        forc_tot_z_displaced = G ->  sum(G.forc_tot[mesh.nsets["DISPLACED"], 3]),
        forc_ebc_z_displaced = G ->  sum(G.forc_ebc[mesh.nsets["DISPLACED"], 3]),
        forc_tot_z_fixed = G -> sum(G.forc_tot[mesh.nsets["DZ_0"], 3]),
        forc_ebc_z_fixed = G -> sum(G.forc_ebc[mesh.nsets["DZ_0"], 3]),
    ),
)

#------------------------------------------------------------------------------
# Solve with MTLED

glob, output_filename, output_history_dataframe, results = solve(
    model,
    mesh,
    solver,
    outdir = "Cube_EBCIEM_neohookean");

#------------------------------------------------------------------------------
# Postprocess

using SofTiSim: nrmse
using DelimitedFiles: readdlm

uz_exact = -0.2 .* getindex.(glob.coord, 3)

# Compare against Abaqus results
u_abq = readdlm("disp.txt")

@info string("NRMSE Ux MTLED compared to Abaqus: ", nrmse(glob.disp[:,1], u_abq[:,1]))
@info string("NRMSE Uy MTLED compared to Abaqus: ", nrmse(glob.disp[:,2], u_abq[:,2]))
@info string("NRMSE Uz MTLED compared to Abaqus: ", nrmse(glob.disp[:,3], u_abq[:,3]))

@info string("NRMSE Uz Abaqus compared to analytical: ", nrmse(u_abq[:,3], uz_exact))
@info string("NRMSE Uz MTLED compared to analytical: ", nrmse(glob.disp[:,3], uz_exact))

import PyPlot
# using DataFrames

# df = DataFrame([values(output_history.data)...], [keys(output_history.data)...])
df = output_history_dataframe

fig, ax = PyPlot.subplots(figsize=(4, 4))
ax.plot(-df.disp_displaced, df.forc_tot_z_fixed, "r:", label="\$f^{int}\$ (fixed)")
ax.plot(-df.disp_displaced, df.forc_ebc_z_fixed, "r--", label="\$f^{EBC}\$ (fixed)")
ax.plot(-df.disp_displaced, -df.forc_tot_z_displaced, "b:", label="\$f^{int}\$ (displaced)")
ax.plot(-df.disp_displaced, -df.forc_ebc_z_displaced, "b--", label="\$f^{EBC}\$ (displaced)")
ax.legend()
ax.set_xlabel("Vertical displacement (m)")
ax.set_ylabel("Vertical Force (N)")
fig.savefig(output_filename * ".plot.total-reaction-forces.pdf")

# Mass scaling factors
# TODO: put this in a separate file so it can be included from any demo
fig, ax = PyPlot.subplots(figsize=(4, 4))
ax.hist(glob.qpoints.mass_scaling_factor, label="\$u_z\$ (bot)")
ax.set_xlabel("Mass scaling factor")
ax.set_ylabel("Number of integration points")
ax.set_title("Mass scaling factors")
fig.savefig(output_filename * ".plot.mass-scaling-factors.pdf")
