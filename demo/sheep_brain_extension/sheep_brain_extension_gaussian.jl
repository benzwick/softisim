include("../init.jl")

# Meshless Simulation : Unconstrained Deformation of a Cube
# EBC Impostion : Using 'EBCIEM/SEBCIEM' or 'Correction' Method

model = Dict()

#------------------------------------------------------------------------------
# Configuration for mesh-free methods

# Shape functions
model[:use_exact_SF_derivatives] = true
model[:use_base_functions] = 2
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true
model[:basic_function] = SofTiSim.BasicGaussian()

# Essential Boundary Condition Impostion
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = true

# Adaptive numerical integration
model[:use_adaptive_integration] = false
model[:number_of_tetrahedral_divisions] = 4
model[:integration_eps] = 0.1
model[:quadrature_degree] = 1

model[:scale_mass] = true

#------------------------------------------------------------------------------
# Other configurations

model[:progress_num_steps] = 500

#------------------------------------------------------------------------------
# Solver

using SofTiSim: DynamicRelaxationParameters

solver = DynamicRelaxationParameters(
    configured_time_step = 0.0001,
)

#------------------------------------------------------------------------------
# Materials

using SofTiSim: NeoHookean, elastic_moduli

model[:density] = 1000
model[:material] = NeoHookean(elastic_moduli(E=3000, nu=0.49))

#------------------------------------------------------------------------------
# Mesh

using SofTiSim: readmesh

mesh = readmesh("../mesh/sheep_mesh_C3D4H.inp")

#------------------------------------------------------------------------------
# Dirichlet boundary conditions

using SofTiSim: DirichletBC, smooth345

bcs = (
    fix_bot = DirichletBC(
        nodes = mesh.nsets["BOT"],
        dofs = 1:3,
    ),
    fix_top = DirichletBC(
        nodes = mesh.nsets["TOP"],
        dofs = 1:2,
    ),
    disp = DirichletBC(
        nodes = mesh.nsets["TOP"],
        dofs = 3:3,
        magnitude = 0.001,
        amplitude = smooth345,
    ),
)

#------------------------------------------------------------------------------
# Output

using SofTiSim: OutputFieldVtk

output_field = OutputFieldVtk(interval=1000)

#------------------------------------------------------------------------------
# Solve with MTLED

include("../../src/mtled.jl")

#------------------------------------------------------------------------------
# Postprocess

using SofTiSim: nrmse
using DelimitedFiles

# FIXME: check this
uz_exact = -0.001/0.00525 .* getindex.(glob.coord, 3)

# Compare against Abaqus results
u_abq = readdlm("sheep_model_C3D4H.odb_disp.txt")

@info string("NRMSE Ux compared to Abaqus: ", nrmse(glob.disp[:,1], u_abq[:,1]))
@info string("NRMSE Uy compared to Abaqus: ", nrmse(glob.disp[:,2], u_abq[:,2]))
@info string("NRMSE Uz compared to Abaqus: ", nrmse(glob.disp[:,3], u_abq[:,3]))

@info string("NRMSE Uz Abaqus compared to analytical: ", nrmse(u_abq[:,3], uz_exact))
@info string("NRMSE Uz MTLED compared to analytical: ", nrmse(glob.disp[:,3], uz_exact))
