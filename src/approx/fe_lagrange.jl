# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

struct FELagrange{Degree} <: FEInterp
    FELagrange(degree) = new{degree}()
end

"Return an empty shape function values matrix."
shape_init_value(::FELagrange{1}, ::Hex8)  = zeros(8)
shape_init_value(::FELagrange{1}, ::Tet4)  = zeros(4)
shape_init_value(::FELagrange{1}, ::Hex20) = zeros(8)
shape_init_value(::FELagrange{1}, ::Tet10) = zeros(4)
shape_init_value(::FELagrange{2}, ::Hex20) = zeros(20)
shape_init_value(::FELagrange{2}, ::Tet10) = zeros(10)

"Return an empty shape function derivatives matrix."
shape_init_deriv(::FELagrange{1}, ::Hex8)  = zeros(8, 3)
shape_init_deriv(::FELagrange{1}, ::Tet4)  = zeros(4, 3)
shape_init_deriv(::FELagrange{1}, ::Hex20) = zeros(8, 3)
shape_init_deriv(::FELagrange{1}, ::Tet10) = zeros(4, 3)
shape_init_deriv(::FELagrange{2}, ::Hex20) = zeros(20, 3)
shape_init_deriv(::FELagrange{2}, ::Tet10) = zeros(10, 3)

"Return element nodes, shape function values and first derivatives."
function nodes_shape_value_and_deriv(approx::FEInterp, elem::Elem, r::Point)
    return elem.nodes, shape_value(approx, elem, r), shape_deriv(approx, elem, r)
end

"Return matrix of shape function values."
function shape_value(interp::FELagrange, elem::Elem, r::Point)
    N = shape_init_value(interp, elem)
    for i = 1:length(N)
        N[i] = shape_value(interp, elem, r, i)
    end
    return N
end

"Return matrix of shape function first derivatives w.r.t. reference coordinates."
function shape_deriv_ref(interp::FELagrange, elem::Elem{Dim}, r::Point{Dim}) where Dim
    dNdr = shape_init_deriv(interp, elem)
    for i = 1:size(dNdr, 1)
        for j = 1:Dim
            dNdr[i,j] = shape_deriv_ref(interp, elem, r, i, j)
        end
    end
    return dNdr
end

"Return matrix of shape function first derivatives w.r.t. global coordinates."
function shape_deriv(interp::FELagrange, elem::Elem, r::Point{3})
    dNdr = shape_deriv_ref(interp, elem, r)
    dNdx = collect(transpose((inv_jacobian(elem.point, dNdr) * dNdr')))
end

"Return the inverse Jacobian."
function inv_jacobian(elem_points, dNdr)
    p = eltorows(elem_points)
    J = [dNdr[:,1]'*p[:,1] dNdr[:,1]'*p[:,2] dNdr[:,1]'*p[:,3]
         dNdr[:,2]'*p[:,1] dNdr[:,2]'*p[:,2] dNdr[:,2]'*p[:,3]
         dNdr[:,3]'*p[:,1] dNdr[:,3]'*p[:,2] dNdr[:,3]'*p[:,3]]
    return inv(J)
end

function throw_no_shape_value(interp, elem, i)
    elemtype = typeof(elem)
    throw(ArgumentError("no shape function values $i " *
                        "for $interp interpolation " *
                        "on $elemtype element."))
end

function throw_no_shape_deriv(interp, elem, i, j)
    elemtype = typeof(elem)
    throw(ArgumentError("no shape function derivative $i w.r.t. $j " *
                        "for $interp interpolation " *
                        "on $elemtype element."))
end

# Shape function value and derivatives implementations
include("fe_lagrange_shape_3d.jl")
