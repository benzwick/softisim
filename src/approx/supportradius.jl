# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    supportradius(points, simplices)

Return a vector of `length(points)` containing the support radius
of each point in the vector `points` computed using the connectivity
of 4-node tetrahedral `simplices`.

The support radius of each point is computed as the average distance
between that point and the points of the surrounding tetrahedrons.
"""
function supportradius(
    points::Vector{Point{3}},
    simplices::Vector{SVector{4, Int}}
)::Vector{Float64}

    num_points = length(points)
    suppradius = zeros(num_points)      # support radius of each point
    nneighbors = zeros(Int, num_points) # num neighbors connected to each point

    for simplex in simplices
        p1 = points[simplex[1]]::Point{3}
        p2 = points[simplex[2]]::Point{3}
        p3 = points[simplex[3]]::Point{3}
        p4 = points[simplex[4]]::Point{3}

        # Distances between simplex points
        d12 = sqrt((p1[1]-p2[1]) * (p1[1]-p2[1]) +
                   (p1[2]-p2[2]) * (p1[2]-p2[2]) +
                   (p1[3]-p2[3]) * (p1[3]-p2[3]))

        d13 = sqrt((p1[1]-p3[1]) * (p1[1]-p3[1]) +
                   (p1[2]-p3[2]) * (p1[2]-p3[2]) +
                   (p1[3]-p3[3]) * (p1[3]-p3[3]))

        d14 = sqrt((p1[1]-p4[1]) * (p1[1]-p4[1]) +
                   (p1[2]-p4[2]) * (p1[2]-p4[2]) +
                   (p1[3]-p4[3]) * (p1[3]-p4[3]))

        d23 = sqrt((p2[1]-p3[1]) * (p2[1]-p3[1]) +
                   (p2[2]-p3[2]) * (p2[2]-p3[2]) +
                   (p2[3]-p3[3]) * (p2[3]-p3[3]))

        d24 = sqrt((p2[1]-p4[1]) * (p2[1]-p4[1]) +
                   (p2[2]-p4[2]) * (p2[2]-p4[2]) +
                   (p2[3]-p4[3]) * (p2[3]-p4[3]))

        d34 = sqrt((p3[1]-p4[1]) * (p3[1]-p4[1]) +
                   (p3[2]-p4[2]) * (p3[2]-p4[2]) +
                   (p3[3]-p4[3]) * (p3[3]-p4[3]))

        suppradius[simplex[1]] += (d12 + d13 + d14)
        suppradius[simplex[2]] += (d12 + d23 + d24)
        suppradius[simplex[3]] += (d13 + d23 + d34)
        suppradius[simplex[4]] += (d14 + d24 + d34)

        nneighbors[simplex[1]] += 3
        nneighbors[simplex[2]] += 3
        nneighbors[simplex[3]] += 3
        nneighbors[simplex[4]] += 3
    end

    # Check number of neighbors for each point to avoid divide by zero
    # (each point must have at least one neighbor)
    nneighbors_min = minimum(nneighbors)
    nneighbors_max = maximum(nneighbors)
    @info string("Number of neighbors (min, max):",
                 " (", nneighbors_min, ", ", nneighbors_max, ")")
    if nneighbors_min < 1
        throw(DivideError(string("The following points have no neighbors (divide by zero): ",
                                 findall(nneighbors .< 1))))
    end

    suppradius ./= nneighbors

    return suppradius
end

# Abstract elements
function supportradius(
    points::Vector{Point{3}},
    elements::Vector{Elem{3}}
)::Vector{Float64}

    @assert all([elem isa Tet4 for elem in elements]) "Only Tet4 elements are supported"

    tets = Vector{Tet4}(undef, length(elements))
    tets .= elements

    return supportradius(points, tets)
end

# Tet4 simplices
function supportradius(
    points::Vector{Point{3}},
    simplices::Vector{Tet4}
)::Vector{Float64}

    # Convert the Vector{Tet4} to a Vector{SVector{4, Int}
    simplices = [SVector{4, Int}(simplex.nodes) for simplex in simplices]

    return supportradius(points, simplices)
end

# Matrix{Int} simplices
function supportradius(
    points::Vector{Point{3}},
    simplices::Matrix{Int}
)::Vector{Float64}

    # Convert the Matrix{Int} to a Vector{SVector{4, Int}
    simplices = [SVector{4, Int}(simplices[i,:]) for i=1:size(simplices, 1)]

    return supportradius(points, simplices)
end

"""
    supportradius(points)

Return a vector of `length(points)` containing the support radius
of each point in the vector `points` computed using a triangulation
of the points.

The support radius of each point is computed as the average distance
between that point and the points of the surrounding simplices.
"""
function supportradius(
    points::Vector{Point{Dim}}
)::Vector{Float64} where Dim

    simplices = delaunay(points)

    return supportradius(points, simplices)
end
