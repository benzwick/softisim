# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
Abstract base type for approximation and interpolation methods.

It is helpful to differentiate between approximation and interpolation methods.
Approximating methods (e.g. moving least squares)
do not satisfy the Kronecker delta property and
require special care for e.g. BCs which can be circumvented
if an interpolating method (e.g. finite element method) is used.
"""
abstract type ApproxOrInterp end

abstract type Approx <: ApproxOrInterp end
abstract type Interp <: ApproxOrInterp end

"Abstract meshfree approximant"
abstract type MeshfreeApprox <: Approx end

"Abstract meshfree interpolant"
abstract type MeshfreeInterp <: Interp end

"Abstract finite element interpolant"
abstract type FEInterp <: Interp end

is_interpolating(::Interp) = true
is_interpolating(::Approx) = false

is_meshfree(::FEInterp) = false
is_meshfree(::Union{MeshfreeApprox, MeshfreeInterp}) = true

# ------------------------------------------------------------------------------
# includes

# Finite element
include("fe_lagrange.jl")

# Meshfree MLS
include("basic_functions.jl")
include("mls.jl")
include("mls_3d.jl")
