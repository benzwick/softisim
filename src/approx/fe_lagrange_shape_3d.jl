# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

@doc raw"""
    shape_value(approx, elem, r, i)

Return the $i^{th}$ shape function value at the point $r$.
"""
function shape_value(
    interp::FELagrange{1}, elem::Tet4, r::Point{3}, i::Int
)::Float64
    if     i == 1 return 1. - r[1] - r[2] - r[3]
    elseif i == 2 return r[1]
    elseif i == 3 return r[2]
    elseif i == 4 return r[3]
    end
    throw_no_shape_value(interp, elem, i)
end

function shape_value(
    interp::FELagrange{2}, elem::Tet10, r::Point{3}, i::Int
)::Float64

    # Area coordinates, pg. 205, Vol. I, Carey, Oden, Becker FEM
    z1 = r[1]::Float64
    z2 = r[2]::Float64
    z3 = r[3]::Float64
    z0 = 1. - z1 - z2 - z3

    if     i == 1  return z0*(2*z0 - 1)
    elseif i == 2  return z1*(2*z1 - 1)
    elseif i == 3  return z2*(2*z2 - 1)
    elseif i == 4  return z3*(2*z3 - 1)
    elseif i == 5  return 4*z0*z1
    elseif i == 6  return 4*z1*z2
    elseif i == 7  return 4*z2*z0
    elseif i == 8  return 4*z0*z3
    elseif i == 9  return 4*z1*z3
    elseif i == 10 return 4*z2*z3
    end
    throw_no_shape_value(elem, i)
end

function shape_value(
    interp::FELagrange{1}, elem::Hex8, r::Point{3}, i::Int
)::Float64
    if     i == 1 return 0.125*(1 - r[1])*(1 - r[2])*(1 - r[3])
    elseif i == 2 return 0.125*(1 + r[1])*(1 - r[2])*(1 - r[3])
    elseif i == 3 return 0.125*(1 + r[1])*(1 + r[2])*(1 - r[3])
    elseif i == 4 return 0.125*(1 - r[1])*(1 + r[2])*(1 - r[3])
    elseif i == 5 return 0.125*(1 - r[1])*(1 - r[2])*(1 + r[3])
    elseif i == 6 return 0.125*(1 + r[1])*(1 - r[2])*(1 + r[3])
    elseif i == 7 return 0.125*(1 + r[1])*(1 + r[2])*(1 + r[3])
    elseif i == 8 return 0.125*(1 - r[1])*(1 + r[2])*(1 + r[3])
    end
    throw_no_shape_value(interp, elem, i)
end

function shape_value(
    interp::FELagrange{2}, elem::Hex20, r::Point{3}, i::Int
)::Float64

    # these functions are defined for (x,y,z) in [0,1]^3
    # so transform the locations
    x = 0.5*(r[1] + 1)
    y = 0.5*(r[2] + 1)
    z = 0.5*(r[3] + 1)

    if     i == 1  return (1 - x)*(1 - y)*(1 - z)*(1 - 2x - 2y - 2z)
    elseif i == 2  return x*(1 - y)*(1 - z)*(2x - 2y - 2z - 1)
    elseif i == 3  return x*y*(1 - z)*(2x + 2y - 2z - 3)
    elseif i == 4  return (1 - x)*y*(1 - z)*(2y - 2x - 2z - 1)
    elseif i == 5  return (1 - x)*(1 - y)*z*(2z - 2x - 2y - 1)
    elseif i == 6  return x*(1 - y)*z*(2x - 2y + 2z - 3)
    elseif i == 7  return x*y*z*(2x + 2y + 2z - 5)
    elseif i == 8  return (1 - x)*y*z*(2y - 2x + 2z - 3)
    elseif i == 9  return 4x*(1 - x)*(1 - y)*(1 - z)
    elseif i == 10 return 4x*y*(1 - y)*(1 - z)
    elseif i == 11 return 4x*(1 - x)*y*(1 - z)
    elseif i == 12 return 4(1 - x)*y*(1 - y)*(1 - z)
    elseif i == 13 return 4x*(1 - x)*(1 - y)*z
    elseif i == 14 return 4x*y*(1 - y)*z
    elseif i == 15 return 4x*(1 - x)*y*z
    elseif i == 16 return 4(1 - x)*y*(1 - y)*z
    elseif i == 17 return 4(1 - x)*(1 - y)*z*(1 - z)
    elseif i == 18 return 4x*(1 - y)*z*(1 - z)
    elseif i == 19 return 4x*y*z*(1 - z)
    elseif i == 20 return 4(1 - x)*y*z*(1 - z)
    end
    throw_no_shape_value(interp, elem, i)
end

@doc raw"""
    shape_deriv_ref(approx, elem, r, i, j)

Return the $j^{th}$ derivative of the $i^{th}$ shape function at the point $r$.
"""
function shape_deriv_ref(
    interp::FELagrange{1}, elem::Tet4, r::Point{3}, i::Int, j::Int
)::Float64
    if     j == 1
        if     i == 1 return -1.
        elseif i == 2 return  1.
        elseif i == 3 return  0.
        elseif i == 4 return  0.
        end
    elseif j == 2
        if     i == 1 return -1.
        elseif i == 2 return  0.
        elseif i == 3 return  1.
        elseif i == 4 return  0.
        end
    elseif j == 3
        if     i == 1 return -1.
        elseif i == 2 return  0.
        elseif i == 3 return  0.
        elseif i == 4 return  1.
        end
    end
    throw_no_shape_deriv(interp, elem, i, j)
end

function shape_deriv_ref(
    interp::FELagrange{2}, elem::Tet10, r::Point{3}, i::Int, j::Int
)::Float64

    # See: libmesh/src/fe/fe_lagrange_shape_3D.C

    # Area coordinates, pg. 205, Vol. I, Carey, Oden, Becker FEM
    z1 = r[1]::Float64
    z2 = r[2]::Float64
    z3 = r[3]::Float64
    z0 = 1. - z1 - z2 - z3

    dz0dr = -1.
    dz1dr =  1.
    dz2dr =  0.
    dz3dr =  0.

    dz0ds = -1.
    dz1ds =  0.
    dz2ds =  1.
    dz3ds =  0.

    dz0dt = -1.
    dz1dt =  0.
    dz2dt =  0.
    dz3dt =  1.

    if     j == 1 # d()/dr
        if     i == 1  return (4z0 - 1)*dz0dr
        elseif i == 2  return (4z1 - 1)*dz1dr
        elseif i == 3  return (4z2 - 1)*dz2dr
        elseif i == 4  return (4z3 - 1)*dz3dr
        elseif i == 5  return 4(z0*dz1dr + dz0dr*z1)
        elseif i == 6  return 4(z1*dz2dr + dz1dr*z2)
        elseif i == 7  return 4(z0*dz2dr + dz0dr*z2)
        elseif i == 8  return 4(z0*dz3dr + dz0dr*z3)
        elseif i == 9  return 4(z1*dz3dr + dz1dr*z3)
        elseif i == 10 return 4(z2*dz3dr + dz2dr*z3)
        end
    elseif j == 2 # d()/ds
        if     i == 1  return (4z0 - 1)*dz0ds
        elseif i == 2  return (4z1 - 1)*dz1ds
        elseif i == 3  return (4z2 - 1)*dz2ds
        elseif i == 4  return (4z3 - 1)*dz3ds
        elseif i == 5  return 4(z0*dz1ds + dz0ds*z1)
        elseif i == 6  return 4(z1*dz2ds + dz1ds*z2)
        elseif i == 7  return 4(z0*dz2ds + dz0ds*z2)
        elseif i == 8  return 4(z0*dz3ds + dz0ds*z3)
        elseif i == 9  return 4(z1*dz3ds + dz1ds*z3)
        elseif i == 10 return 4(z2*dz3ds + dz2ds*z3)
        end
    elseif j == 3 # d()/dt
        if     i == 1  return (4z0 - 1)*dz0dt
        elseif i == 2  return (4z1 - 1)*dz1dt
        elseif i == 3  return (4z2 - 1)*dz2dt
        elseif i == 4  return (4z3 - 1)*dz3dt
        elseif i == 5  return 4(z0*dz1dt + dz0dt*z1)
        elseif i == 6  return 4(z1*dz2dt + dz1dt*z2)
        elseif i == 7  return 4(z0*dz2dt + dz0dt*z2)
        elseif i == 8  return 4(z0*dz3dt + dz0dt*z3)
        elseif i == 9  return 4(z1*dz3dt + dz1dt*z3)
        elseif i == 10 return 4(z2*dz3dt + dz2dt*z3)
        end
    end
    throw_no_shape_deriv(interp, elem, i, j)
end

function shape_deriv_ref(
    interp::FELagrange{1}, elem::Hex8, r::Point{3}, i::Int, j::Int
)::Float64
    if     j == 1
        if     i == 1 return -0.125*(1-r[2])*(1-r[3])
        elseif i == 2 return  0.125*(1-r[2])*(1-r[3])
        elseif i == 3 return  0.125*(1+r[2])*(1-r[3])
        elseif i == 4 return -0.125*(1+r[2])*(1-r[3])
        elseif i == 5 return -0.125*(1-r[2])*(1+r[3])
        elseif i == 6 return  0.125*(1-r[2])*(1+r[3])
        elseif i == 7 return  0.125*(1+r[2])*(1+r[3])
        elseif i == 8 return -0.125*(1+r[2])*(1+r[3])
        end
    elseif j == 2
        if     i == 1 return -0.125*(1-r[1])*(1-r[3])
        elseif i == 2 return -0.125*(1+r[1])*(1-r[3])
        elseif i == 3 return  0.125*(1+r[1])*(1-r[3])
        elseif i == 4 return  0.125*(1-r[1])*(1-r[3])
        elseif i == 5 return -0.125*(1-r[1])*(1+r[3])
        elseif i == 6 return -0.125*(1+r[1])*(1+r[3])
        elseif i == 7 return  0.125*(1+r[1])*(1+r[3])
        elseif i == 8 return  0.125*(1-r[1])*(1+r[3])
        end
    elseif j == 3
        if     i == 1 return -0.125*(1-r[1])*(1-r[2])
        elseif i == 2 return -0.125*(1+r[1])*(1-r[2])
        elseif i == 3 return -0.125*(1+r[1])*(1+r[2])
        elseif i == 4 return -0.125*(1-r[1])*(1+r[2])
        elseif i == 5 return  0.125*(1-r[1])*(1-r[2])
        elseif i == 6 return  0.125*(1+r[1])*(1-r[2])
        elseif i == 7 return  0.125*(1+r[1])*(1+r[2])
        elseif i == 8 return  0.125*(1-r[1])*(1+r[2])
        end
    end
    throw_no_shape_deriv(interp, elem, i, j)
end

function shape_deriv_ref(
    interp::FELagrange{2}, elem::Hex20, r::Point{3}, i::Int, j::Int
)::Float64

    # See: libmesh/src/fe/fe_lagrange_shape_3D.C
    # The node ordering here is slightly different (same as VTK and Abaqus)
    # with the last two rows (13:16 and 17:20) of points swapped.

    # these functions are defined for (x,y,z) in [0,1]^3
    # so transform the locations
    x = 0.5*(r[1] + 1)::Float64
    y = 0.5*(r[2] + 1)::Float64
    z = 0.5*(r[3] + 1)::Float64

    # and don't forget the chain rule!

    if     j == 1 # d/dx*dx/dr
        if     i == 1  return .5*(1 - y)*(1 - z)*((1 - x)*(-2) + (-1)*(1 - 2x - 2y - 2z))
        elseif i == 2  return .5*(1 - y)*(1 - z)*(x*(2) + (1)*(2x - 2y - 2z - 1))
        elseif i == 3  return .5*y*(1 - z)*(x*(2) + (1)*(2x + 2y - 2z - 3))
        elseif i == 4  return .5*y*(1 - z)*((1 - x)*(-2) + (-1)*(2y - 2x - 2z - 1))
        elseif i == 5  return .5*(1 - y)*z*((1 - x)*(-2) + (-1)*(2z - 2x - 2y - 1))
        elseif i == 6  return .5*(1 - y)*z*(x*(2) + (1)*(2x - 2y + 2z - 3))
        elseif i == 7  return .5*y*z*(x*(2) + (1)*(2x + 2y + 2z - 5.))
        elseif i == 8  return .5*y*z*((1 - x)*(-2) + (-1)*(2y - 2x + 2z - 3))
        elseif i == 9  return 2(1 - y)*(1 - z)*(1 - 2x)
        elseif i == 10 return 2y*(1 - y)*(1 - z)
        elseif i == 11 return 2y*(1 - z)*(1 - 2x)
        elseif i == 12 return 2y*(1 - y)*(1 - z)*(-1)
        elseif i == 13 return 2(1 - y)*z*(1 - 2x)
        elseif i == 14 return 2y*(1 - y)*z
        elseif i == 15 return 2y*z*(1 - 2x)
        elseif i == 16 return 2y*(1 - y)*z*(-1)
        elseif i == 17 return 2(1 - y)*z*(1 - z)*(-1)
        elseif i == 18 return 2(1 - y)*z*(1 - z)
        elseif i == 19 return 2y*z*(1 - z)
        elseif i == 20 return 2y*z*(1 - z)*(-1)
        end
    elseif j == 2 # d/dy*dy/ds
        if     i == 1  return .5*(1 - x)*(1 - z)*((1 - y)*(-2) + (-1)*(1 - 2x - 2y - 2z))
        elseif i == 2  return .5*x*(1 - z)*((1 - y)*(-2) + (-1)*(2x - 2y - 2z - 1))
        elseif i == 3  return .5*x*(1 - z)*(y*(2) + (1)*(2x + 2y - 2z - 3))
        elseif i == 4  return .5*(1 - x)*(1 - z)*(y*(2) + (1)*(2y - 2x - 2z - 1))
        elseif i == 5  return .5*(1 - x)*z*((1 - y)*(-2) + (-1)*(2z - 2x - 2y - 1))
        elseif i == 6  return .5*x*z*((1 - y)*(-2) + (-1)*(2x - 2y + 2z - 3))
        elseif i == 7  return .5*x*z*(y*(2) + (1)*(2x + 2y + 2z - 5.))
        elseif i == 8  return .5*(1 - x)*z*(y*(2) + (1)*(2y - 2x + 2z - 3))
        elseif i == 9  return 2x*(1 - x)*(1 - z)*(-1)
        elseif i == 10 return 2x*(1 - z)*(1 - 2y)
        elseif i == 11 return 2x*(1 - x)*(1 - z)
        elseif i == 12 return 2(1 - x)*(1 - z)*(1 - 2y)
        elseif i == 13 return 2x*(1 - x)*z*(-1)
        elseif i == 14 return 2x*z*(1 - 2y)
        elseif i == 15 return 2x*(1 - x)*z
        elseif i == 16 return 2(1 - x)*z*(1 - 2y)
        elseif i == 17 return 2(1 - x)*z*(1 - z)*(-1)
        elseif i == 18 return 2x*z*(1 - z)*(-1)
        elseif i == 19 return 2x*z*(1 - z)
        elseif i == 20 return 2(1 - x)*z*(1 - z)
        end
    elseif j == 3 # d/dz*dz/dt
        if     i == 1  return .5*(1 - x)*(1 - y)*((1 - z)*(-2) + (-1)*(1 - 2x - 2y - 2z))
        elseif i == 2  return .5*x*(1 - y)*((1 - z)*(-2) + (-1)*(2x - 2y - 2z - 1))
        elseif i == 3  return .5*x*y*((1 - z)*(-2) + (-1)*(2x + 2y - 2z - 3))
        elseif i == 4  return .5*(1 - x)*y*((1 - z)*(-2) + (-1)*(2y - 2x - 2z - 1))
        elseif i == 5  return .5*(1 - x)*(1 - y)*(z*(2) + (1)*(2z - 2x - 2y - 1))
        elseif i == 6  return .5*x*(1 - y)*(z*(2) + (1)*(2x - 2y + 2z - 3))
        elseif i == 7  return .5*x*y*(z*(2) + (1)*(2x + 2y + 2z - 5.))
        elseif i == 8  return .5*(1 - x)*y*(z*(2) + (1)*(2y - 2x + 2z - 3))
        elseif i == 9  return 2x*(1 - x)*(1 - y)*(-1)
        elseif i == 10 return 2x*y*(1 - y)*(-1)
        elseif i == 11 return 2x*(1 - x)*y*(-1)
        elseif i == 12 return 2(1 - x)*y*(1 - y)*(-1)
        elseif i == 13 return 2x*(1 - x)*(1 - y)
        elseif i == 14 return 2x*y*(1 - y)
        elseif i == 15 return 2x*(1 - x)*y
        elseif i == 16 return 2(1 - x)*y*(1 - y)
        elseif i == 17 return 2(1 - x)*(1 - y)*(1 - 2z)
        elseif i == 18 return 2x*(1 - y)*(1 - 2z)
        elseif i == 19 return 2x*y*(1 - 2z)
        elseif i == 20 return 2(1 - x)*y*(1 - 2z)
        end
    end
    throw_no_shape_deriv(interp, elem, i, j)
end
