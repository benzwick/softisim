# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using SofTiSim
using BenchmarkTools

r = rand(30);

b = BasicGaussian()
@benchmark basic_value($b, $r)
@benchmark basic_dwdr_div_r($b, $r)

b = BasicQuarticSpline()
@benchmark basic_value($b, $r)
@benchmark basic_dwdr_div_r($b, $r)
