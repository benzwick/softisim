# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

###
### SEE ALSO values.jl
###

using NearestNeighbors

"""
Influence nodes are known beforehand (predefined in FEM and precompute in MLS)
Usage (compare with FEM):

approx = MLS(...)
# NOTE: need to pass also the influencers and corresponding epoints
phi, dhpi = nodes_shape_value_and_deriv(approx::MLS, elem::Elem, qpoints::Vector{Point})
phi, _ = nodes_shape_value_and_deriv(approx::MLS, elem::Elem, glob.coord::Vector{Point}, compute_dphi=false)

"""

"Meshfree moving least squares approximant."
mutable struct MLS{Dim} <: MeshfreeApprox
    "Coordinates of basic function center points"
    centers::AbstractArray{Point{Dim}, 1} # why not vector?
    "Support radius of basic functions at each center point"
    support_radius::Vector{Float64}
    "Dilatation parameter"
    dilatation::Float64
    basic_function::BasicFunction
    diffuse_derivatives::Bool
    # TODO: tree does not need to be stored
    # and cannot be reused if by epoints, dsites instead of centers
    # "Nearest neighbor tree"
    # tree::NNTree

    # basis?
    # find_nearest_neighbors?
    # shape_value?
    # shape_deriv?
    function MLS(
        centers::AbstractArray{Point{Dim}, 1};
        # Options:
        simplices::Union{Vector{Tet4}, Nothing} = nothing,
        dilatation::Float64 = 1.6,
        basic_function::String = "radial", # make this a BasicFunction type
        degree::Int = 2,
        diffuse_derivatives = false
    ) where{Dim}

        mls = new{Dim}()

        # # Simple check to avoid some simple misunderstandings.
        # # Either define dilatation and let the support_radius be computed
        # # or define the support_radius manually.
        # if !isa(support_radius, Nothing) && !isa(dilatation, Nothing)
        #     error("Cannot define support radius and dilatation concurrently.")
        # end

        # Point cloud coordinates
        mls.centers = centers

        # # FIXME: only do this if variable support radius is used
        # #        (otherwise it should already be defined)
        # # Support radius
        # # Only do this if user hasn't already defined support radius e.g. constant vector
        # if simplices isa Nothing
        #     @info "MLS: triangulating point cloud and computing support radius"
        #     mls.support_radius = supportradius(centers)
        # else
        #     @info "MLS: computing support radius using supplied simplices"
        #     mls.support_radius = supportradius(centers, simplices)
        # end
        # # Scale the support radius by dilatation parameter
        # mls.support_radius .*= dilatation

        # TODO: Should this be in mls function instead and use dsites and epoints not centers?
        # TODO: Probably not. If centers then only one MLS object is needed to get phi at dsites and epoints
        # but maybe it will be slower.
        # We loop over each epoint so we should have a list with length(epoints) that has the neighbours and distances
        # Nearest neighbor tree
        # @info "MLS: creating nearest neighbor tree"
        # @time mls.tree = KDTree(centers)

        ####
        # In the MLS code for each epoint:
        # influencers = inrange(tree, centers, support_radius, false)
        ####

        return mls
    end
end
