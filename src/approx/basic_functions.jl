# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Parameters

abstract type BasicFunction end

# ------------------------------------------------------------------------------
# Functions

"""
    basic_value

Return the value of the basic function.
"""
function basic_value end

"""
    basic_dwdr_div_r

Return the derivative of the basic function wrt r divided by r, i.e.
(dw/dr)/r.
"""
function basic_dwdr_div_r end

# ------------------------------------------------------------------------------

"""
Gaussian basic function
"""
@with_kw struct BasicGaussian <: BasicFunction
    alpha::Float64 = 0.4
end

function basic_value(b::BasicGaussian, r::Vector{Float64})
    # FIXME: why divide by max?
    exp.(-((r ./ maximum(r)) / b.alpha).^2)
end

function basic_dwdr_div_r(b::BasicGaussian, r::Vector{Float64})
    # FIXME: why divide by max?
    -2 ./ (b.alpha^2 * maximum(r)) .* basic_value(b, r)
end

# ------------------------------------------------------------------------------

"""
Quartic spline basic function
"""
struct BasicQuarticSpline <: BasicFunction
end

function basic_value(b::BasicQuarticSpline, r::Vector{Float64})
    @. 1 - 6*r*r + 8*r*r*r - 3*r*r*r*r
end

function basic_dwdr_div_r(b::BasicQuarticSpline, r::Vector{Float64})
    @. -12 + 24*r - 12*r*r
end
