# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    dim(a) -> Integer

Return the dimensionality of `a`.
"""
dim

"""
    n_nodes(collection) -> Integer

Return the number of nodes in the collection.
"""
n_nodes
