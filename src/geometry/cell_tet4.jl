# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
using LinearAlgebra

"""
    Tet4(nodes, point)

3D tetrahedral cell with 4 `nodes` and `point` coordinates.
"""
struct Tet4 <: Tet
    nodes::Vector{Int}
    # FIXME: should use SubArray but adaptive refinement needs local coordinates
    # point::SubArray{Point{3}}
    point::AbstractArray{Point{3}, 1}
end

# ------------------------------------------------------------------------------
# Properties

# Geometric constants for Tet4.
n_nodes(::Type{Tet4}) = 4
n_sides(::Type{Tet4}) = 4
n_edges(::Type{Tet4}) = 6
n_children(::Type{Tet4}) = 8
n_nodes_per_side(::Type{Tet4}) = 3
n_nodes_per_edge(::Type{Tet4}) = 2

reference_coordinates(::Type{Tet4}) = [
    [0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [0., 0., 1.],
]

# ------------------------------------------------------------------------------
# Functions

function volume(cell::Tet4)
    # The volume of a tetrahedron is 1/6 the box product
    # formed by its base and apex vectors
    a = cell.point[4] - cell.point[1]

    # b is the vector pointing from 1 to 2
    b = cell.point[2] - cell.point[1]

    # c is the vector pointing from 1 to 3
    c = cell.point[3] - cell.point[1]

    volume = triple_scalar_product(a, b, c) / 6
end
