# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
using LinearAlgebra

"""
    Hex20(nodes, point)

3D hexahedral cell with 20 `nodes` and `point` coordinates.
"""
struct Hex20 <: Hex
    nodes::Vector{Int}
    # FIXME: should use SubArray but adaptive refinement needs local coordinates
    # point::SubArray{Point{3}}
    point::AbstractArray{Point{3}, 1}
end

# ------------------------------------------------------------------------------
# Properties

# Geometric constants for Hex20
n_nodes(::Type{Hex20}) = 20
n_sides(::Type{Hex20}) = 6
n_edges(::Type{Hex20}) = 12
n_children(::Type{Hex20}) = 8
n_nodes_per_side(::Type{Hex20}) = 8
n_nodes_per_edge(::Type{Hex20}) = 3

reference_coordinates(::Type{Hex20}) = [
    [-1., -1., -1.], [ 1., -1., -1.], [ 1.,  1., -1.], [-1.,  1., -1.],
    [-1., -1.,  1.], [ 1., -1.,  1.], [ 1.,  1.,  1.], [-1.,  1.,  1.],
    [ 0., -1., -1.], [ 1.,  0., -1.], [ 0.,  1., -1.], [-1.,  0., -1.],
    [ 0., -1.,  1.], [ 1.,  0.,  1.], [ 0.,  1.,  1.], [-1.,  0.,  1.],
    [-1., -1.,  0.], [ 1., -1.,  0.], [ 1.,  1.,  0.], [-1.,  1.,  0.],
]

# ------------------------------------------------------------------------------
# Functions

function volume(cell::Hex20)
    # See: libmesh/src/geom/cell_hex20.C
    # The node ordering here is slightly different (same as VTK and Abaqus)
    # with the last two rows of points swapped.

    point = cell.point

    # Make copies of our points.  It makes the subsequent calculations a bit
    # shorter and avoids dereferencing the same pointer multiple times.
    x0  = point[1];  x1  = point[2];  x2  = point[3];  x3  = point[4];
    x4  = point[5];  x5  = point[6];  x6  = point[7];  x7  = point[8];
    x8  = point[9];  x9  = point[10]; x10 = point[11]; x11 = point[12];
    x16 = point[13]; x17 = point[14]; x18 = point[15]; x19 = point[16];
    x12 = point[17]; x13 = point[18]; x14 = point[19]; x15 = point[20];

    # The constant components of the dx/dr vector,
    # dx/dr = \vec{a000} + \vec{a001}*t + \vec{a002}*t^2 + ...
    # These were copied directly from the output of a Python script.
    # There are at most 17 terms with total degree <=3, but only 12
    # of them are non-zero for each direction.
    dx_dr =
        [
            x0/8 - x1/8 - x11/4 - x12/4 + x13/4 + x14/4 - x15/4 + x17/4 - x19/4 - x2/8 + x3/8 + x4/8 - x5/8 - x6/8 + x7/8 + x9/4,
            x11/4 + x17/4 - x19/4 - x9/4,
            -x0/8 + x1/8 + x12/4 - x13/4 - x14/4 + x15/4 + x2/8 - x3/8 - x4/8 + x5/8 + x6/8 - x7/8,
            x12/4 - x13/4 + x14/4 - x15/4,
            -x0/8 + x1/8 - x2/8 + x3/8 + x4/8 - x5/8 + x6/8 - x7/8,
            x0/8 - x1/8 - x12/4 + x13/4 - x14/4 + x15/4 + x2/8 - x3/8 + x4/8 - x5/8 + x6/8 - x7/8,
            -x0/8 + x1/8 + x11/4 - x17/4 + x19/4 + x2/8 - x3/8 - x4/8 + x5/8 + x6/8 - x7/8 - x9/4,
            x0/8 - x1/8 - x11/4 - x17/4 + x19/4 - x2/8 + x3/8 - x4/8 + x5/8 + x6/8 - x7/8 + x9/4,
            x0/4 + x1/4 - x10/2 - x16/2 - x18/2 + x2/4 + x3/4 + x4/4 + x5/4 + x6/4 + x7/4 - x8/2,
            -x0/4 - x1/4 + x10/2 - x16/2 - x18/2 - x2/4 - x3/4 + x4/4 + x5/4 + x6/4 + x7/4 + x8/2,
            Point(0,0,0),
            -x0/4 - x1/4 - x10/2 + x16/2 - x18/2 + x2/4 + x3/4 - x4/4 - x5/4 + x6/4 + x7/4 + x8/2,
            x0/4 + x1/4 + x10/2 + x16/2 - x18/2 - x2/4 - x3/4 - x4/4 - x5/4 + x6/4 + x7/4 - x8/2,
            Point(0,0,0),
            Point(0,0,0),
            Point(0,0,0),
            Point(0,0,0)
        ]

    # The constant components of the dx/ds vector. These were copied
    # directly from the output of a Python script.  There are at most
    # 17 terms with total degree <=3, but only 12 of them are non-zero
    # for each direction.
    dx_ds =
        [
            x0/8 + x1/8 + x10/4 - x12/4 - x13/4 + x14/4 + x15/4 - x16/4 + x18/4 - x2/8 - x3/8 + x4/8 + x5/8 - x6/8 - x7/8 - x8/4,
            -x10/4 - x16/4 + x18/4 + x8/4,
            -x0/8 - x1/8 + x12/4 + x13/4 - x14/4 - x15/4 + x2/8 + x3/8 - x4/8 - x5/8 + x6/8 + x7/8,
            x0/4 + x1/4 - x11/2 - x17/2 - x19/2 + x2/4 + x3/4 + x4/4 + x5/4 + x6/4 + x7/4 - x9/2,
            -x0/4 - x1/4 + x11/2 - x17/2 - x19/2 - x2/4 - x3/4 + x4/4 + x5/4 + x6/4 + x7/4 + x9/2,
            Point(0,0,0),
            Point(0,0,0),
            Point(0,0,0),
            x12/4 - x13/4 + x14/4 - x15/4,
            -x0/8 + x1/8 - x2/8 + x3/8 + x4/8 - x5/8 + x6/8 - x7/8,
            x0/8 - x1/8 - x12/4 + x13/4 - x14/4 + x15/4 + x2/8 - x3/8 + x4/8 - x5/8 + x6/8 - x7/8,
            -x0/4 + x1/4 + x11/2 - x17/2 + x19/2 + x2/4 - x3/4 - x4/4 + x5/4 + x6/4 - x7/4 - x9/2,
            x0/4 - x1/4 - x11/2 - x17/2 + x19/2 - x2/4 + x3/4 - x4/4 + x5/4 + x6/4 - x7/4 + x9/2,
            Point(0,0,0),
            -x0/8 - x1/8 - x10/4 + x16/4 - x18/4 + x2/8 + x3/8 - x4/8 - x5/8 + x6/8 + x7/8 + x8/4,
            x0/8 + x1/8 + x10/4 + x16/4 - x18/4 - x2/8 - x3/8 - x4/8 - x5/8 + x6/8 + x7/8 - x8/4,
            Point(0,0,0)
        ]

    # The constant components of the dx/dt vector. These were copied
    # directly from the output of a Python script.  There are at most
    # 17 terms with total degree <=3, but only 12 of them are non-zero
    # for each direction.
    dx_dt =
        [
            x0/8 + x1/8 - x10/4 - x11/4 + x16/4 + x17/4 + x18/4 + x19/4 + x2/8 + x3/8 - x4/8 - x5/8 - x6/8 - x7/8 - x8/4 - x9/4,
            x0/4 + x1/4 - x12/2 - x13/2 - x14/2 - x15/2 + x2/4 + x3/4 + x4/4 + x5/4 + x6/4 + x7/4,
            Point(0,0,0),
            -x10/4 - x16/4 + x18/4 + x8/4,
            -x0/4 - x1/4 + x12/2 + x13/2 - x14/2 - x15/2 + x2/4 + x3/4 - x4/4 - x5/4 + x6/4 + x7/4,
            Point(0,0,0),
            -x0/8 - x1/8 + x11/4 - x17/4 - x19/4 - x2/8 - x3/8 + x4/8 + x5/8 + x6/8 + x7/8 + x9/4,
            Point(0,0,0),
            x11/4 + x17/4 - x19/4 - x9/4,
            -x0/4 + x1/4 + x12/2 - x13/2 - x14/2 + x15/2 + x2/4 - x3/4 - x4/4 + x5/4 + x6/4 - x7/4,
            Point(0,0,0),
            -x0/8 + x1/8 - x2/8 + x3/8 + x4/8 - x5/8 + x6/8 - x7/8,
            x0/4 - x1/4 - x12/2 + x13/2 - x14/2 + x15/2 + x2/4 - x3/4 + x4/4 - x5/4 + x6/4 - x7/4,
            x0/8 - x1/8 - x11/4 - x17/4 + x19/4 - x2/8 + x3/8 - x4/8 + x5/8 + x6/8 - x7/8 + x9/4,
            -x0/8 - x1/8 + x10/4 - x16/4 - x18/4 - x2/8 - x3/8 + x4/8 + x5/8 + x6/8 + x7/8 + x8/4,
            Point(0,0,0),
            x0/8 + x1/8 + x10/4 + x16/4 - x18/4 - x2/8 - x3/8 - x4/8 - x5/8 + x6/8 + x7/8 - x8/4,
        ]

    # The integer exponents for each term.
    exponents =
        [
            [1, 1, 1],
            [1, 1, 2],
            [1, 1, 3],
            [1, 2, 1],
            [1, 2, 2],
            [1, 2, 3],
            [1, 3, 1],
            [1, 3, 2],
            [2, 1, 1],
            [2, 1, 2],
            [2, 1, 3],
            [2, 2, 1],
            [2, 2, 2],
            [2, 3, 1],
            [3, 1, 1],
            [3, 1, 2],
            [3, 2, 1]
        ]

    # 3x3 quadrature, exact for bi-quintics
    N = 3
    w = [5/9, 8/9, 5/9]

    # Quadrature point locations raised to powers.  q[1][3] is
    # quadrature point 1, squared, q[2][2] is quadrature point 2 to the
    # first power, etc.
    q = [#^0           ^1     ^2
         [1., -sqrt(15)/5, 15/25],
         [1.,          0.,    0.],
         [1.,  sqrt(15)/5, 15/25]]

    vol = 0.
    for i = 1:N
        for j = 1:N
            for k = 1:N
                # Compute dx_dr, dx_ds, dx_dt at the current quadrature point.
                dx_dr_q = zeros(3)
                dx_ds_q = zeros(3)
                dx_dt_q = zeros(3)
                for c = 1:17
                    coeff =
                        q[i][exponents[c][1]] *
                        q[j][exponents[c][2]] *
                        q[k][exponents[c][3]];

                    dx_dr_q .+= coeff * dx_dr[c]
                    dx_ds_q .+= coeff * dx_ds[c]
                    dx_dt_q .+= coeff * dx_dt[c]
                end

                # Compute scalar triple product, multiply by weight, and accumulate volume.
                vol += w[i] * w[j] * w[k] * triple_scalar_product(dx_dr_q, dx_ds_q, dx_dt_q)
            end
        end
    end

    return vol
end
