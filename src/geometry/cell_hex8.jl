# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
using LinearAlgebra

"""
    Hex8(nodes, point)

3D hexahedral cell with 8 `nodes` and `point` coordinates.
"""
struct Hex8 <: Hex
    nodes::Vector{Int}
    # FIXME: should use SubArray but adaptive refinement needs local coordinates
    # point::SubArray{Point{3}}
    point::AbstractArray{Point{3}, 1}
end

# ------------------------------------------------------------------------------
# Properties

# Geometric constants for Hex8.
n_nodes(::Type{Hex8}) = 8
n_sides(::Type{Hex8}) = 6
n_edges(::Type{Hex8}) = 12
n_children(::Type{Hex8}) = 8
n_nodes_per_side(::Type{Hex8}) = 4
n_nodes_per_edge(::Type{Hex8}) = 2

reference_coordinates(::Type{Hex8}) = [
    [-1., -1., -1.], [ 1., -1., -1.], [ 1.,  1., -1.], [-1.,  1., -1.],
    [-1., -1.,  1.], [ 1., -1.,  1.], [ 1.,  1.,  1.], [-1.,  1.,  1.],
]

# ------------------------------------------------------------------------------
# Functions

function volume(cell::Hex8)

    p = cell.point

    x0 = p[1]; x1 = p[2]; x2 = p[3]; x3 = p[4]
    x4 = p[5]; x5 = p[6]; x6 = p[7]; x7 = p[8]

    q0 =  x0 - x1 + x2 - x3 + x4 - x5 + x6 - x7
    q1 =  x0 - x1 - x2 + x3 - x4 + x5 + x6 - x7
    q2 = -x0 + x1 + x2 - x3 - x4 + x5 + x6 - x7
    q3 =  x0 + x1 - x2 - x3 - x4 - x5 + x6 + x7
    q4 = -x0 - x1 + x2 + x3 - x4 - x5 + x6 + x7
    q5 = -x0 - x1 - x2 - x3 + x4 + x5 + x6 + x7

    return (triple_scalar_product(q0, q4, q3) +
            triple_scalar_product(q2, q0, q1) +
            triple_scalar_product(q1, q3, q5)) / 192. +
            triple_scalar_product(q2, q4, q5) / 64.
end
