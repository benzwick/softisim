# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    create_elements(coord, nodes)

Return vector of elements with connectivity
defined by `nodes` at coordinates `coord`.
"""
function create_elements(coord::Vector{Point{Dim}},
                         nodes::Matrix{Int}) where Dim

    num_nodes = size(nodes, 1)
    num_nodes_per_element = size(nodes, 2)

    if num_nodes_per_element == 4
        Element = Tet4
    elseif num_nodes_per_element == 8
        Element = Hex8
    elseif num_nodes_per_element == 10
        Element = Tet10
    elseif num_nodes_per_element == 20
        Element = Hex20
    else
        throw(ArgumentError("Unsupported number of nodes per element: $num_nodes_per_element"))
    end

    elements = Element[]

    for i in 1:num_nodes
        push!(elements,
              Element(nodes[i,:], view(coord, nodes[i,:])))
    end

    return elements
end

"""
    create_elements(coordinates, element_connectivity, element_types)

Return vector of `Elem`s of various `element_types` with `element_connectivity`.
"""
function create_elements(coordinates::Vector{Point{Dim}},
                         element_connectivity::Vector{Vector{Int}},
                         element_types::Vector{Symbol}) where Dim

    elements = Elem{Dim}[]

    for (con, typ) in zip(element_connectivity, element_types)
        push!(elements, eval(typ)(con, view(coordinates, con)))
    end

    return elements
end
