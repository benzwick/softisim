# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using DataFrames

abstract type OutputField end
abstract type OutputQuadratureField end

"""
    rename_abaqus_to_softisim!(dataframe)

Rename the columns in the `dataframe` by replacing
Abaqus symbols with SofTiSim symbols.
"""
function rename_abaqus_to_softisim!(dataframe::DataFrame)
    abq2softisim = Dict(
        # "Node"   => ,
        "X"      =>  :coord_1,
        "Y"      =>  :coord_2,
        "Z"      =>  :coord_3,
        # "RF"     => ,
        "RF1"    => :forc_tot_1,
        "RF2"    => :forc_tot_2,
        "RF3"    => :forc_tot_3,
        # "U"      => ,
        "U1"     => :disp_1,
        "U2"     => :disp_2,
        "U3"     => :disp_3,
    )

    rename!(dataframe, abq2softisim)
end
