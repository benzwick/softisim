# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

import AbaqusReader
using DataStructures # sort(Dict) -> OrderedDict

"""
    readmesh(filename)

Read mesh from file with name `filename`.

# Supported file formats:
- .inp: Abaqus
"""
function readmesh(filename::String)

    # Determine mesh type from extension
    if endswith(filename, ".inp")
        fileformat = :abaqus
    else
        throw(ArgumentError("Cannot determine format of mesh: $filename"))
    end

    # Read the mesh
    if fileformat == :abaqus
        mesh = AbaqusReader.abaqus_read_mesh(filename)::Dict{String, Dict}

        # Extract items from mesh dict and annotate types
        NODES         = mesh["nodes"]::Dict{Int64, Vector{Float64}}
        ELEMENT_SETS  = mesh["element_sets"]::Dict{String, Vector{Int64}}
        ELEMENT_TYPES = mesh["element_types"]::Dict{Int64,Symbol}
        ELEMENTS      = mesh["elements"]::Dict{Int64,Vector{Int64}}
        SURFACE_SETS  = mesh["surface_sets"]::Dict{String,Array{Tuple{Int64,Symbol},1}}
        SURFACE_TYPES = mesh["surface_types"]::Dict{String,Symbol}
        NODE_SETS     = mesh["node_sets"]::Dict{String,Vector{Int64}}

        # TODO:
        # node numbers
        # rename elements -> cells = connectivity
        # Every mesh requires at least vertices (dim=0) and cells (codim=0)
        # Entity        Dimension       Codimension
        # Vertex                0
        # Edge                  1
        # Face                  2
        # Cell                  3
        # Peak                                    3
        # Ridge                                   2
        # Facet                                   1
        # Cell                                    0 ?
        # NOTE: In geometry a cell is a 3D element: https://en.wikipedia.org/wiki/Polytope
        # TODO: Mesh type
        # coordinates
        # vertices
        # edges
        # faces
        # cells

        node_ids = vcat(transpose(collect(keys(sort!(OrderedDict(NODES)))))...)::Vector{Int}
        node_coordinates = [Point{3}(x) for x in collect(values(sort!(OrderedDict(NODES))))]::Vector{Point{3}}
        element_ids = vcat(transpose(collect(keys(sort!(OrderedDict(ELEMENTS)))))...)::Vector{Int}
        element_connectivity = vcat(transpose(collect(values(sort!(OrderedDict(ELEMENTS)))))...)::Matrix{Int}
        element_connectivity = convert(Matrix{Int}, element_connectivity)::Matrix{Int}

        # These (Dict and OrderedDict) have similar performance
        # OrderedDict is more convienient so use that
        node_sets = OrderedDict(mesh["node_sets"])::OrderedDict{String, Vector{Int}}
        element_sets = OrderedDict(mesh["element_sets"])::OrderedDict{String, Vector{Int}}
        # node_sets = mesh["node_sets"]::Dict{String, Vector{Int}}
        # element_sets = mesh["element_sets"]::Dict{String, Vector{Int}}
    end

    return Mesh{3}(
        node_coordinates,
        node_ids             = node_ids,
        node_sets            = node_sets,
        element_connectivity = element_connectivity,
        element_ids          = element_ids,
        element_sets         = element_sets)
end
