# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

import DataFrames
import CSV

mutable struct OutputHistory
    filename::String
    frequency::Int
    functions::NamedTuple
    data::NamedTuple

    function OutputHistory(;
                           frequency = 10,
                           user_functions = NamedTuple(),
                           )

        output = new()

        output.frequency = frequency

        # Default functions
        # TODO: add more default variables (e.g. total energy)
        default_functions = (
            increment = glob -> glob.increment,
            time = glob -> glob.time,
            total_strain_energy = glob -> glob.total_strain_energy,
        )

        # Merge default and user functions
        output.functions = merge(default_functions, user_functions)

        # Initialise data vectors
        num_float64_cols = length(default_functions) + length(user_functions) - 1
        columns = Any[]
        append!(columns, [Int[]])  # for stepnum
        append!(columns, [Float64[] for i in 1:num_float64_cols])
        output.data = NamedTuple{keys(output.functions)}(columns)

        return output
    end
end

function initialise_output!(output::OutputHistory;
                            filename::String,
                            mesh)

    output.filename = filename
end

function finish(output::OutputHistory)

    df = DataFrames.DataFrame()

    for key in keys(output.data)
        df[!, key] = output.data[key]
    end

    CSV.write(output.filename * ".history.csv", df)

    return df
end
