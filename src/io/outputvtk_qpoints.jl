# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Printf: @sprintf
import WriteVTK

mutable struct OutputQuadratureFieldVtk <: OutputQuadratureField
    filename::String
    interval::Int
    variables::Set{String}

    pvdfile::WriteVTK.CollectionFile
    coordinates::Array{Float64,2}
    cells::Array{WriteVTK.MeshCell,1}

    # Quadrature point field output variables
    qpoint_output

    function OutputQuadratureFieldVtk(
        ;
        interval::Int,
        include_variables = [
            # NOTE: ensure that all of these are initialized below
            "deformation_gradient_comp_11",
            "deformation_gradient_comp_12",
            "deformation_gradient_comp_13",
            "deformation_gradient_comp_21",
            "deformation_gradient_comp_22",
            "deformation_gradient_comp_23",
            "deformation_gradient_comp_31",
            "deformation_gradient_comp_32",
            "deformation_gradient_comp_33",
            "strain_almansi_comp_11",
            "strain_almansi_comp_12",
            "strain_almansi_comp_13",
            "strain_almansi_comp_21",
            "strain_almansi_comp_22",
            "strain_almansi_comp_23",
            "strain_almansi_comp_31",
            "strain_almansi_comp_32",
            "strain_almansi_comp_33",
            "strain_almansi_prin_max_value",
            "strain_almansi_prin_mid_value",
            "strain_almansi_prin_min_value",
            "strain_almansi_prin_max_vector",
            "strain_almansi_prin_mid_vector",
            "strain_almansi_prin_min_vector",
            "strain_green_comp_11",
            "strain_green_comp_12",
            "strain_green_comp_13",
            "strain_green_comp_21",
            "strain_green_comp_22",
            "strain_green_comp_23",
            "strain_green_comp_31",
            "strain_green_comp_32",
            "strain_green_comp_33",
            "strain_green_prin_max_value",
            "strain_green_prin_mid_value",
            "strain_green_prin_min_value",
            "strain_green_prin_max_vector",
            "strain_green_prin_mid_vector",
            "strain_green_prin_min_vector",
            "stretch_left_prin_max_value",
            "stretch_left_prin_mid_value",
            "stretch_left_prin_min_value",
            "stretch_left_prin_max_vector",
            "stretch_left_prin_mid_vector",
            "stretch_left_prin_min_vector",
            "stretch_right_prin_max_value",
            "stretch_right_prin_mid_value",
            "stretch_right_prin_min_value",
            "stretch_right_prin_max_vector",
            "stretch_right_prin_mid_vector",
            "stretch_right_prin_min_vector",
        ],
        exclude_variables = []
    )

        output = new()
        output.interval = interval
        output.variables = setdiff!(Set{String}(include_variables),
                                    Set{String}(exclude_variables))
        return output
    end
end

function initialise_output!(output::OutputQuadratureFieldVtk;
                            filename::String,
                            qpoint_coords)

    output.filename = filename * ".qfield"
    output.pvdfile = WriteVTK.paraview_collection(output.filename)

    output.coordinates = eltocols(qpoint_coords)

    output.cells = [WriteVTK.MeshCell(WriteVTK.VTKCellTypes.VTK_VERTEX, [i])
                    for i in 1:length(qpoint_coords)]

    # Initialize storage for saved output variables
    num_qpoints = size(output.coordinates, 2)
    output.qpoint_output = Dict(
        "deformation_gradient_comp_11" => zeros(num_qpoints),
        "deformation_gradient_comp_12" => zeros(num_qpoints),
        "deformation_gradient_comp_13" => zeros(num_qpoints),
        "deformation_gradient_comp_21" => zeros(num_qpoints),
        "deformation_gradient_comp_22" => zeros(num_qpoints),
        "deformation_gradient_comp_23" => zeros(num_qpoints),
        "deformation_gradient_comp_31" => zeros(num_qpoints),
        "deformation_gradient_comp_32" => zeros(num_qpoints),
        "deformation_gradient_comp_33" => zeros(num_qpoints),
        "strain_almansi_comp_11" => zeros(num_qpoints),
        "strain_almansi_comp_12" => zeros(num_qpoints),
        "strain_almansi_comp_13" => zeros(num_qpoints),
        "strain_almansi_comp_21" => zeros(num_qpoints),
        "strain_almansi_comp_22" => zeros(num_qpoints),
        "strain_almansi_comp_23" => zeros(num_qpoints),
        "strain_almansi_comp_31" => zeros(num_qpoints),
        "strain_almansi_comp_32" => zeros(num_qpoints),
        "strain_almansi_comp_33" => zeros(num_qpoints),
        "strain_almansi_prin_max_value" => zeros(num_qpoints),
        "strain_almansi_prin_mid_value" => zeros(num_qpoints),
        "strain_almansi_prin_min_value" => zeros(num_qpoints),
        "strain_almansi_prin_max_vector" => [zeros(3) for _ in 1:num_qpoints],
        "strain_almansi_prin_mid_vector" => [zeros(3) for _ in 1:num_qpoints],
        "strain_almansi_prin_min_vector" => [zeros(3) for _ in 1:num_qpoints],
        "strain_green_comp_11" => zeros(num_qpoints),
        "strain_green_comp_12" => zeros(num_qpoints),
        "strain_green_comp_13" => zeros(num_qpoints),
        "strain_green_comp_21" => zeros(num_qpoints),
        "strain_green_comp_22" => zeros(num_qpoints),
        "strain_green_comp_23" => zeros(num_qpoints),
        "strain_green_comp_31" => zeros(num_qpoints),
        "strain_green_comp_32" => zeros(num_qpoints),
        "strain_green_comp_33" => zeros(num_qpoints),
        "strain_green_prin_max_value" => zeros(num_qpoints),
        "strain_green_prin_mid_value" => zeros(num_qpoints),
        "strain_green_prin_min_value" => zeros(num_qpoints),
        "strain_green_prin_max_vector" => [zeros(3) for _ in 1:num_qpoints],
        "strain_green_prin_mid_vector" => [zeros(3) for _ in 1:num_qpoints],
        "strain_green_prin_min_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_left_prin_max_value" => zeros(num_qpoints),
        "stretch_left_prin_mid_value" => zeros(num_qpoints),
        "stretch_left_prin_min_value" => zeros(num_qpoints),
        "stretch_left_prin_max_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_left_prin_mid_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_left_prin_min_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_right_prin_max_value" => zeros(num_qpoints),
        "stretch_right_prin_mid_value" => zeros(num_qpoints),
        "stretch_right_prin_min_value" => zeros(num_qpoints),
        "stretch_right_prin_max_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_right_prin_mid_vector" => [zeros(3) for _ in 1:num_qpoints],
        "stretch_right_prin_min_vector" => [zeros(3) for _ in 1:num_qpoints],
    )

end

function write_output(output::OutputQuadratureFieldVtk, glob::GlobalVariables)

    vtkfile = WriteVTK.vtk_grid(@sprintf("%s-%06i.vtu", output.filename, glob.increment),
                                output.coordinates, output.cells)

    results = output.qpoint_output

    for k in keys(results)
        if k in output.variables
            if occursin("_comp_", k) || occursin("_value", k)
                WriteVTK.vtk_point_data(vtkfile, results[k], k)
            elseif occursin("_vector", k)
                data = (getindex.(results[k], 1), getindex.(results[k], 2), getindex.(results[k], 3))
                WriteVTK.vtk_point_data(vtkfile, data, k)
            end
        end
    end

    WriteVTK.vtk_save(vtkfile)

    WriteVTK.collection_add_timestep(output.pvdfile, vtkfile, glob.increment)
end

function finish(output::OutputQuadratureFieldVtk)
    WriteVTK.vtk_save(output.pvdfile)
end
