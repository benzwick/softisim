# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

# using LinearAlgebra
# using LinearAlgebra.BLAS: gemm!

using StaticArrays

using SofTiSim:
    spkstress

"""

Assemble internal force contribution from each integration point.
"""
function assemble_force!(
    forc_int::Matrix{Float64},
    force_threads::Vector{Matrix{Float64}},
    dnod_curr::Matrix{Float64},
    nodes::Vector{Vector{Int}},
    JxW::Vector{Float64},
    derivatives::Vector{Matrix{Float64}},
    material::Vector{Material},
    qpoint_strain_energy::Vector{Float64},
    qpoint_gradient::Vector{MMatrix{3, 3, Float64}},
    data,
    increment::Int,
    save_quadrature_output::Bool,
    save_history_output::Bool,
    output                      # FIXME: add type
)::Nothing

    num_qpoints = length(JxW)

    # Keep track of errors
    global WARN_SMALL_VOLUME
    global WARN_ZERO_VOLUME
    warn_small_volume = false
    warn_zero_volume = false
    continuing = false
    deleting = false
    terminate = false
    exception = nothing
    tol_small_volume = 0.1
    has_small_volume = zeros(Bool, num_qpoints)
    has_zero_volume  = zeros(Bool, num_qpoints)

    Threads.@threads for qp in 1:num_qpoints
        # Update matrix of u derivatives w.r.t x (transposed)
        # Vectorised:
        # HT .= derivatives[qp]' * disp[nodes[qp],:]
        # Slow:
        # gemm!('T', 'N', 1.0, derivatives[qp], disp[nodes[qp],:], 0.0, HT)
        # mul!(HT, derivatives[qp]', disp[nodes[qp],:])
        # mul!(HT, transpose(derivatives[qp]), disp[nodes[qp],:])
        # Devectorised:
        HT = compute_displacement_gradient(derivatives[qp], dnod_curr, nodes[qp])
        # Save the (non-transposed) displacement gradient for each qp
        qpoint_gradient[qp] = transpose(HT)

        # Deformation gradient (transposed)
        FT = I + HT
        # C .= I + HT  + HT' + HT*HT'

        # Determinant of deformation gradient
        # FIXME: FT is recomputed in spkstress.
        #        Pass deformation data as a struct to spkstress instead and use getters
        #        that check if anything is already computed or needs to be updated.
        detFT = det(FT)

        # Save strain measures before attempting to compute stress
        if save_quadrature_output
            # Deformation gradient
            let
                F = transpose(FT)
                for i = 1:3, j = 1:3
                    output["deformation_gradient_comp_" * string(i,j)][qp] = F[i,j]
                end
            end

            # Stretches (left)
            # B = F*F' = \sum_{i=1}^3 \lamba_i^2 ni*ni
            let
                B = transpose(FT) * FT # left Cauchy-Green
                ev = eigen(B)
                output["stretch_left_prin_min_value"][qp] = sqrt(ev.values[1])
                output["stretch_left_prin_mid_value"][qp] = sqrt(ev.values[2])
                output["stretch_left_prin_max_value"][qp] = sqrt(ev.values[3])
                output["stretch_left_prin_min_vector"][qp] .= ev.vectors[:, 1]
                output["stretch_left_prin_mid_vector"][qp] .= ev.vectors[:, 2]
                output["stretch_left_prin_max_vector"][qp] .= ev.vectors[:, 3]
            end

            # Stretches (right)
            # C = F'*F = \sum_{i=1}^3 \lamba_i^2 Ni*Ni
            let
                C = FT * transpose(FT) # right Cauchy-Green
                ev = eigen(C)
                output["stretch_right_prin_min_value"][qp] = sqrt(ev.values[1])
                output["stretch_right_prin_mid_value"][qp] = sqrt(ev.values[2])
                output["stretch_right_prin_max_value"][qp] = sqrt(ev.values[3])
                output["stretch_right_prin_min_vector"][qp] .= ev.vectors[:, 1]
                output["stretch_right_prin_mid_vector"][qp] .= ev.vectors[:, 2]
                output["stretch_right_prin_max_vector"][qp] .= ev.vectors[:, 3]
            end

            # Almansi strain
            let
                FTinv = inv(FT)
                c = FTinv * transpose(FTinv)   # or c = inv(B)
                e = 1/2*(I - c)
                for i = 1:3, j = 1:3
                    output["strain_almansi_comp_" * string(i,j)][qp] = e[i,j]
                end
                # Almansi principal strain
                ev = eigen(e)
                # NOTE: save values and vectors separately because the sign (+/-)
                #       is needed with magnitude for denoting tension/compression
                output["strain_almansi_prin_min_value"][qp] = ev.values[1]
                output["strain_almansi_prin_mid_value"][qp] = ev.values[2]
                output["strain_almansi_prin_max_value"][qp] = ev.values[3]
                output["strain_almansi_prin_min_vector"][qp] .= ev.vectors[:, 1]
                output["strain_almansi_prin_mid_vector"][qp] .= ev.vectors[:, 2]
                output["strain_almansi_prin_max_vector"][qp] .= ev.vectors[:, 3]
            end

            # Green strain
            let
                C = FT * transpose(FT)
                E = 1/2 * (C - I)
                for i = 1:3, j = 1:3
                    output["strain_green_comp_" * string(i,j)][qp] = E[i,j]
                end
                # Green principal strain
                ev = eigen(E)
                # NOTE: save values and vectors separately because the sign (+/-)
                #       is needed with magnitude for denoting tension/compression
                output["strain_green_prin_min_value"][qp] = ev.values[1]
                output["strain_green_prin_mid_value"][qp] = ev.values[2]
                output["strain_green_prin_max_value"][qp] = ev.values[3]
                output["strain_green_prin_min_vector"][qp] .= ev.vectors[:, 1]
                output["strain_green_prin_mid_vector"][qp] .= ev.vectors[:, 2]
                output["strain_green_prin_max_vector"][qp] .= ev.vectors[:, 3]
            end
        end

        # Check for negative Jacobian determinant of deformation
        if detFT < tol_small_volume
            has_small_volume[qp] = true
            warn_small_volume = true
            if detFT < 0
                has_zero_volume[qp] = true
                warn_zero_volume = true
                if material[qp].when_zero_volume == :terminate
                    exception = :(NegativeJacobianDeterminantError())
                    terminate = true
                elseif material[qp].when_zero_volume == :continue
                    continuing = true
                elseif material[qp].when_zero_volume == :delete
                    FT *= 0
                    deleting = true
                end
            end
        end

        # Use material law to compute the stress
        # S .= spkstress(FT, material[qp])
        # S .= spkstress(C, material[qp])
        try
            S = spkstress(FT, material[qp], data)

            # Integrate stress to find force on element using guassian integration
            # and update global force vector (scatter)
            # force[nodes[qp],:] .+= derivatives[qp] * S' * FT * JxW[qp]
            # Devectorised:
            compute_and_scatter_force!(force_threads[Threads.threadid()],
                                       derivatives[qp], S, FT,
                                       JxW[qp], nodes[qp])

            # Compute strain energy if output_history requested
            if save_history_output
                qpoint_strain_energy[qp] = strain_energy(FT, material[qp]) * JxW[qp]
            end

        catch exc
            if isa(exc, DomainError) || isa(exc, ErrorException)
                # FIXME: ErrorException is only handled here because
                #        eigen from StaticArrays throws it
                #        instead of a DomainError.
                @error "Something went wrong while assembling internal forces:\n" *
                    "Inc:  $increment\nTime: $(data.time)\nqp: $qp\nFT: $FT\nC:  $(FT*FT')\n" *
                    sprint(showerror, exc, catch_backtrace())
                # FIXME: This is not necessarily a NegativeJacobianDeterminantError
                #        Maybe change name to AssemblyError instead?
                exception = :(NegativeJacobianDeterminantError())
                terminate = true
                break
            else
                rethrow(exc)
            end
        end
    end

    # Scatter and reset threads force
    for thread = 1:Threads.nthreads()
        forc_int .+= force_threads[thread]
        force_threads[thread] .= 0
    end

    # Warnings and errors
    if warn_small_volume && WARN_SMALL_VOLUME
        @warn "Inc $increment: " *
            "det(F) < $tol_small_volume " *
            "at $(sum(has_small_volume)) quadrature points."
        WARN_SMALL_VOLUME = false
    end
    if warn_zero_volume && WARN_ZERO_VOLUME
        @warn "Inc $increment: " *
            "det(F) < 0 " *
            "at $(sum(has_zero_volume)) quadrature points."
        if continuing
            @warn "Inc $increment: Continuing with some quadrature points that have negative volume."
        end
        if deleting
            @warn "Inc $increment: Deleting some quadrature points that have negative volume by setting F = [0]"
        end
        if terminate
            @error "Inc $increment: Terminating analysis because det(F) < 0"
        end
        WARN_ZERO_VOLUME = false
    end

    # Wait for tasks to finish and assemble global force for debugging
    # before throwing the exception
    if terminate
        throw(eval(exception))
    end

    return nothing
end

"""

Integrate stress to find elemental stress force and scatter the force.

fint = B0⋅S⋅FT⋅w

- fint: internal nodal forces
- B0: shape function derivatives
- S: Second Piola-Kirchhoff stress (3x3)
- FT: deformation gradient (3x3) (transposed)
- JxW: Mapped quadrature weight.
"""
@inline function compute_and_scatter_force!(
    fint::Array{Float64,2},
    B0::Array{Float64,2},
    S::SMatrix{3, 3, Float64, 9},
    FT::SMatrix{3, 3, Float64, 9},
    JxW::Float64,
    nodes::Array{Int, 1})

    # Fastest (lap: 0.56s)
    @inbounds begin
        for j = 1:3
            f1 = S[1,1]*FT[1,j] + S[2,1]*FT[2,j] + S[3,1]*FT[3,j]
            f2 = S[1,2]*FT[1,j] + S[2,2]*FT[2,j] + S[3,2]*FT[3,j]
            f3 = S[1,3]*FT[1,j] + S[2,3]*FT[2,j] + S[3,3]*FT[3,j]
            for i = 1:length(nodes)
                n = nodes[i]
                fint[n,j] += (B0[i,1]*f1 + B0[i,2]*f2 + B0[i,3]*f3) * JxW
            end
        end
    end

    # Slower:

    # # Compute and scatter (lap: 2.6s)
    # flocal = zeros(length(nodes), 3) # allocate local forces matrix
    # flocal .= B0 * (S' * FT * JxW)   # compute (brackets increase speed by 2x)
    # fint[nodes, :] .+= flocal        # scatter

    # fint[nodes, :] .+= view(B0, :, :) * (S' * FT * JxW)  #  (lap: 1.9s)

    # fint[nodes, :] .+= B0 * (S' * FT * JxW)              # adjoint ' (lap: 1.9s)

    # fint[nodes, :] .+= B0 * (transpose(S) * FT * JxW)    # transpose (lap: 2.0s)
end

@doc raw"""
Compute gradient of displacement field (transposed)
```math
H^T = grad(u)^T = (\frac{\partial u}{\partial x})^T = B_0 u
```
where $B_0$ is the matrix of material derivatives of shape functions
and $u$ is the matrix of nodal displacements.

HT = B0' * u[nodes,:]

- B0: Matrix of material derivatives of shape functions
- u: Matrix of components of displacement at node Nodal displacements
"""
@inline function compute_displacement_gradient(
    B0::Array{Float64, 2},
    u::Array{Float64, 2},
    nodes::Array{Int, 1})

    # NOTE: timing with 260 nodes, 4056 qpoints, support nodes (10, 36)

    # Fastest (lap: 0.56s)
    HT = @MMatrix zeros(Float64, 3, 3)
    @inbounds begin
        for k = 1:length(nodes)
            n = nodes[k]
            for j = 1:3
                for i = 1:3
                    HT[i,j] += B0[k,i] * u[n,j]
                end
            end
        end
    end
    return SMatrix(HT)

    # Slower
    # SMatrix{3, 3, Float64, 9}(BLAS.gemm('T', 'N', B0, u[nodes, :]))
    # SMatrix{3, 3, Float64, 9}(B0' * view(u, nodes, :))            # (lap: 0.81s)
    # SMatrix{3, 3, Float64, 9}(transpose(B0) * view(u, nodes, :))  # (lap: 0.92s)
    # SMatrix{3, 3, Float64, 9}(transpose(B0) * u[nodes, :])        # (lap: 1.26s)
end

function assemble_mass_rowsum!(
    mass::Vector{Float64},
    nodes::Vector{Vector{Int}},
    JxW::Vector{Float64},
    density::Vector{Float64},
    mass_scaling_factor::Vector{Float64}
)::NamedTuple{
    (:total_mass_nodes,
     :total_mass_qpoints,
     :total_mass_qpoints_unscaled),
    Tuple{Float64,Float64,Float64}}

    num_qpoints = length(JxW)

    total_mass_nodes = 0.
    total_mass_qpoints = 0.
    total_mass_qpoints_unscaled = 0.

    for qp in 1:num_qpoints
        # Before scaling
        m = density[qp] * JxW[qp]
        total_mass_qpoints_unscaled += m

        # After scaling
        m *= mass_scaling_factor[qp]
        total_mass_qpoints += m
        mass[nodes[qp]] .+= m / length(nodes[qp])
    end

    total_mass_nodes = sum(mass)

    return (total_mass_nodes = total_mass_nodes,
            total_mass_qpoints = total_mass_qpoints,
            total_mass_qpoints_unscaled = total_mass_qpoints_unscaled)
end

# TODO: assemble_mass_consistent!()
