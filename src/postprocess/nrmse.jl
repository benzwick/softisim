# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Statistics

"""
    nrmse(y, Y)

Normalised root mean square residual norm.
"""
function nrmse(y, Y)
    return sqrt(mean((y-Y).^2)) / (maximum(Y)-minimum(Y))
end
