# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

# using PyPlot
using Makie

# Find nodal positions for non-interpolating shape functions
xd = x2 + u_t2;
#x_int = mls_sf_4iteration*xd;

x_int = xd;
for n = 1:nonum_tot
    xn = x2[n, :];

    closest_nodes_xi, shape_func_vals, _ = InterpMLS_3D(
        x2, xn, support_radius,
        model[:use_exact_SF_derivatives], model[:use_base_functions])

    x_nodes = xd[closest_nodes_xi, :];
    x_int[n,:] = sum(x_nodes.*[shape_func_vals shape_func_vals shape_func_vals], dims=1);
end

figure()
scatter3D(x2[:,1], x2[:,2], x2[:,3], "b.", s=28)
scene = Makie.mesh(x2, faces, color=(:white, 0.5), shading=false)
Makie.wireframe!(scene, color = (:black, 0.6), linewidth = 3)
Makie.scatter!(scene, x2, markersize=0.5)
# DrawFaces( x2, faces, ones(size(x2, 1), 3), 1 )
axis("equal")
title("Un-deformed object")
xlabel("x")
ylabel("y")
zlabel("z")

figure()
scatter3D(x_int[:,1], x_int[:,2], x_int[:,3], "b.", s=28)
# DrawFaces( x_int, faces, ones(size(x2, 1), 3), 1)
axis("equal")
title("Deformed object")
xlabel("x")
ylabel("y")
zlabel("z")

# Display convergence rate
figure()
plot(1:stepnum_total, ro_time[1:stepnum_total], 1:stepnum_total, ro_used_time[1:stepnum_total])
title("Convergence rate")
xlabel("Iteration")
legend("Estimated", "Used")

# Display maximum displacement variation
figure()
plot(1:stepnum_total, Max_disp_variation_time[1:stepnum_total])
title("Maximum displacement variation")
xlabel("Iteration")

# Displacement vectors (quiver)
pygui(true)
fig = figure()
ax = fig[:gca](projection="3d")
ax[:quiver](x2[:,1], x2[:,2], x2[:,3], u_t2[:,1], u_t2[:,2], u_t2[:,3])
# DrawFaces( x2, faces, ones(size(x2, 1), 3), 1)
axis("equal")
title("Displacement")
xlabel("x")
ylabel("y")
zlabel("z")

color = sqrt.(u_t2[:,1].^2 + u_t2[:,2].^2 + u_t2[:,3].^2);
scene = Makie.mesh(xd, faces, color=color, shading=false)
Makie.wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)
Makie.mesh!(x2, faces, color = (:white, 1.0), linewidth = 3, shading=false)
Makie.wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)
# mesh(x2[:,1], x2[:,2], x2[:,3], faces)
title("Displacement")
xlabel("x")
ylabel("y")
zlabel("z")
