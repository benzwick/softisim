# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Roots

# TODO: rename this function as it applies to other shapes besides cubes.
"""
    exact_cube_uniaxial(coord, material, stretch)

Compute the exact displacement field at `coord`
using `material` with given uniaxial `stretch`.
The stretch is applied along the z-axis.

# Examples

```
julia> mesh = meshbox(Point(0,0,0), Point(0.1,0.1,0.1), 2, 2, 2, Hex8);

julia> exact_cube_uniaxial(mesh.coordinates, NeoHookean(elastic_moduli(E=3000, nu=0.49)), 0.8)
27×3 Array{Float64,2}:
 0.0         0.0         -0.0
 0.00578733  0.0         -0.0
 0.0115747   0.0         -0.0
 0.0         0.00578733  -0.0
 0.00578733  0.00578733  -0.0
 0.0115747   0.00578733  -0.0
 0.0         0.0115747   -0.0
 0.00578733  0.0115747   -0.0
 0.0115747   0.0115747   -0.0
 0.0         0.0         -0.01
 0.00578733  0.0         -0.01
 0.0115747   0.0         -0.01
 0.0         0.00578733  -0.01
 0.00578733  0.00578733  -0.01
 0.0115747   0.00578733  -0.01
 0.0         0.0115747   -0.01
 0.00578733  0.0115747   -0.01
 0.0115747   0.0115747   -0.01
 0.0         0.0         -0.02
 0.00578733  0.0         -0.02
 0.0115747   0.0         -0.02
 0.0         0.00578733  -0.02
 0.00578733  0.00578733  -0.02
 0.0115747   0.00578733  -0.02
 0.0         0.0115747   -0.02
 0.00578733  0.0115747   -0.02
 0.0115747   0.0115747   -0.02

```

```julia-repl
julia> exact_cube_uniaxial(glob.coord, model[:sections][:all][:material], 0.8)
189×3 Array{Float64,2}:
 0.00578733  0.00578733  -0.0
...
```
"""
function exact_cube_uniaxial(
    coord::Vector{Point{3}},
    material::NeoHookean,
    stretch::Float64,
)
    C = 1/2*material.mu
    D = 1/2*material.kappa
    f(J) = D*J^(8/3) - D*J^(5/3) + C/(3*stretch)*J - (C*stretch^2)/3;
    # f(J) = D*(J^(8/3) - J^(5/3)) + C*(1/(3*stretch)*J - (stretch^2)/3);
    J = find_zero(f, (0.01, 10))

    # Principal stretches in x, y, z directions
    lambda_1 = sqrt(J/stretch)
    lambda_2 = sqrt(J/stretch)
    lambda_3 = stretch

    x = getindex.(coord ,1);
    y = getindex.(coord ,2);
    z = getindex.(coord ,3);

    ux = (x .- minimum(x)) * (lambda_1 - 1);
    uy = (y .- minimum(y)) * (lambda_2 - 1);
    uz = (z .- minimum(z)) * (lambda_3 - 1);

    # TODO: change to vcat if transposing columns
    return hcat(ux, uy, uz);
end

# TODO: add exact solution for Ogden with different alpha
function exact_cube_uniaxial(
    coord::Vector{Point{3}},
    material::Ogden,
    stretch::Float64,
)
    if isapprox(material.alpha, 2)
        return exact_cube_uniaxial(coord,
                                   NeoHookean(material.mu, material.kappa),
                                   stretch)
    else
        throw(ArgumentError("Exact solution for Ogden model only implemented for alpha=2 " *
                            "but your material has alpha=$(material.alpha)"))
    end
end

# TODO: rename this function as it applies to other shapes besides cubes.
# TODO: remove repeated computations and combine with above functions.
"""
    exact_cube_uniaxial(material, stretch)

Compute the exact deformation gradient
using `material` with given uniaxial `stretch`.
The stretch is applied along the z-axis.
"""
function exact_cube_uniaxial_deformation_gradient(
    material::NeoHookean,
    stretch::Float64,
)
    C = 1/2*material.mu
    D = 1/2*material.kappa
    f(J) = D*J^(8/3) - D*J^(5/3) + C/(3*stretch)*J - (C*stretch^2)/3;
    # f(J) = D*(J^(8/3) - J^(5/3)) + C*(1/(3*stretch)*J - (stretch^2)/3);
    J = find_zero(f, (0.01, 10))

    # Principal stretches in x, y, z directions
    lambda_1 = sqrt(J/stretch)
    lambda_2 = sqrt(J/stretch)
    lambda_3 = stretch

    F = [lambda_1 0 0;
         0 lambda_2 0;
         0 0 lambda_3]

    return F
end

# ------------------------------------------------------------------------------
# Testing

if false
    coord = glob.coord;
    material = model[:sections][:all][:material];
    uexact = hcat(ux, uy, uz);

    nrmse(glob.disp, uexact)
    nrmse(glob.disp[:,3], uexact[:,3])

    glob.disp[mesh.nsets["xmid"]]
    uexact[mesh.nsets["xmid"]]

    glob.disp[mesh.nsets["xmid"]] - uexact[mesh.nsets["xmid"]]

    norm(glob.disp[mesh.nsets["xmid"]] - uexact[mesh.nsets["xmid"]])

    # check cube not at origin (displacements should be the same)
    a = exact_cube_uniaxial(glob.coord, material, 0.8)
    b = exact_cube_uniaxial([x + Point(-0.05, -3, 2) for x in glob.coord], material, 0.8)
    norm(a - b)
end
