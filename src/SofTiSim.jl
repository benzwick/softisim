# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

__precompile__() # this module is safe to precompile
module SofTiSim

# Logging with Memento
# https://invenia.github.io/Memento.jl/latest/faq/pkg-usage/
import Logging
# See init.jl (initialize_output) instead
# import Memento  # requires a minimum of Memento 0.5
# # Create our module level logger (this will get precompiled)
# const LOGGER = Memento.getlogger(@__MODULE__)
const LOGGER_DEFAULT = Ref{Logging.AbstractLogger}()

# Python modules
# Workaround for ebciem.jl which requires scipy.spatial.Delaunay
# See: https://github.com/JuliaPy/PyCall.jl#using-pycall-from-julia-modules
using PyCall
const scipy_spatial = PyNULL()

const SOFTISIM_DIR = abspath(joinpath(dirname(pathof(@__MODULE__)), ".."))
@info string("SOFTISIM_DIR: ", SOFTISIM_DIR)

# Options
const SHOW_PROGRESS_METER = true

# Global variables
# TODO: make the types of these constant
# These bools keep track of when warnings should be shown
# between different intervals of increments
global WARN_SMALL_VOLUME = true
global WARN_ZERO_VOLUME = true

# Get package version using Git
import LibGit2
try
    global SOFTISIM_VERSION_STRING = string(
        LibGit2.headname(LibGit2.GitRepo(SOFTISIM_DIR)),
        " (", string(LibGit2.head_oid(LibGit2.GitRepo(SOFTISIM_DIR))), ")")
    @info string("SOFTISIM_VERSION_STRING: ", SOFTISIM_VERSION_STRING)
catch
    global SOFTISIM_VERSION_STRING = "***Unknown***"
    @warn string("SOFTISIM_VERSION_STRING: ", SOFTISIM_VERSION_STRING)
end

function __init__()
    # Save the default logger to change logger when needed
    LOGGER_DEFAULT[] = Logging.global_logger()
    # Register the module level logger at runtime
    # Memento.register(LOGGER)
    # Use Memento for all logging (not a good idea, users might prefer default)
    # Memento.substitute!() # see init.jl (initialize_output) instead

    # Set options
    if haskey(ENV, "SOFTISIM_SHOW_PROGRESS_METER")
        # If set to anything besides 0, then the progress meter is shown
        global SHOW_PROGRESS_METER = ENV["SOFTISIM_SHOW_PROGRESS_METER"] == "0" ? false : true
    elseif any([haskey(ENV, k) for k in ["GITLAB_CI", "TRAVIS"]])
        global SHOW_PROGRESS_METER = false
    else # default
        global SHOW_PROGRESS_METER = true
    end
    @debug string("SOFTISIM_SHOW_PROGRESS_METER: ", ENV["SOFTISIM_SHOW_PROGRESS_METER"])
    @debug string("SofTiSim.SHOW_PROGRESS_METER: ", SHOW_PROGRESS_METER)

    # Python
    copy!(scipy_spatial, pyimport_conda("scipy.spatial", "scipy"))
end

include("functions.jl")
include("exports.jl")

include("common/error.jl")
include("common/tictoc.jl")
include("common/math.jl")
include("common/util.jl")

# Types
include("geometry/geometry.jl")
include("geometry/create_elements.jl")
include("materials/material.jl")
include("mesh/mesh.jl")
include("integration/quadraturepoint.jl")
include("solvers/globalvariables.jl")

# Approximation and interpolation methods
include("approx/approx.jl")     # includes all approximations
include("approx/mapping.jl")
include("approx/supportradius.jl")

include("materials/elastic_moduli.jl")
# include("materials/neohookean.jl")
include("materials/neohookean_softfem.jl")
include("materials/ogden.jl")
include("materials/swelling.jl")

include("functions/adaptive_tetra_divisions.jl")
include("functions/FEMSF_Triangular.jl")
include("functions/gauss_tetra4.jl")
include("functions/gauss3_tri.jl")
include("functions/GetFaces3D_Tetr.jl")
include("functions/InterpMLS_3D_1.jl")
include("functions/ShapeFuncDerNorm.jl")

include("integration/integrationfunctions.jl")
include("integration/gauss_tet.jl")
include("integration/gauss_hex.jl")

include("io/io.jl")
include("io/readmesh.jl")
include("io/outputvtk.jl")
include("io/outputvtk_qpoints.jl")
include("io/outputhistory.jl")

include("mesh/meshbox.jl")
include("mesh/meshtools.jl")

include("postprocess/nrmse.jl")
include("postprocess/verification.jl")

include("assembly/assemble.jl")

include("solvers/user.jl")
include("solvers/dynamicrelaxation.jl")
include("solvers/dynamicrelaxation_explicitsim.jl")

include("init.jl")
include("mtled.jl")

include("bcs/amplitude.jl")
include("bcs/dirichletbc.jl")
include("bcs/ebciem.jl")

end
