# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Parameters

"""
DynamicRelaxationParameters

Parameters for dynamic relaxation solver.
"""
@with_kw struct DynamicRelaxationParameters
    """Configured time step (predefined by the user)"""
    configured_time_step::Float64 = Inf

    """Critical time increment size safety factor"""
    time_increment_safety_factor::Float64 = 1.5

    """Time for loading application."""
    time_load::Float64 = 1.0

    """Time for solution to reach equilibrium in dynamic relaxation.
    Measure unit: [s]"""
    time_eq::Float64 = 1.0

    """Maximum number of time increments before terminating analysis."""
    max_num_increments::Int = 1e6

    """Convergence rate during loading application."""
    DR_INITIAL_CONVERGENCE_RATE::Float64 = 0.999

    """Convergence rate after loading application (during dynamic relaxation)."""
    DR_CONVERGENCE_RATE_AFTER_LOADING::Float64 = 0.998

    """Number of time steps after which convergence rate updating stops
    to avoid truncation errors."""
    DR_NO_STEPS_UPDATE_CONVERGENCE_RATE::Int = 1000

    """Deviation to check convergence rate stability."""
    DR_CONV_RATE_DEVIATION::Float64 = 0.0001

    """Number of steps after which forces and displacements are updated."""
    DR_FORCE_DISP_UPDATE_INTERVAL::Int = 200

    """Number of consecutive steps after which convergence rate
    stability requirement is considered to have been met."""
    DR_NUM_CONV_RATE_SAMPLES::Int = 20

    """Deviation to decide if the convergence rate has been
    stabilized between the updates."""
    TERMINATION_CONV_RATE_DEVIATION::Float64 = 0.002

    """Estimated error in convergence rate for simulation termination."""
    TERMINATION_CONV_RATE_ERROR::Float64 = 0.2

    """Absolute error for simulation termination."""
    TERMINATION_ABSOLUTE_ERROR::Float64 = 0.01

    """Number of consecutive steps for which termination criteria must be
    satisfied before ending the simulation."""
    TERMINATION_NUM_STEPS::Int = 50
end

mutable struct DynamicRelaxationVariables
    converged::Bool

    # variables for result storage
    "estimated convergence rate versus time"
    ro_time::Vector{Float64}
    "used convergence rate versus time"
    ro_used_time::Vector{Float64}
    "max. displacement variation versus time"
    max_disp_variation_time::Vector{Float64}

    # DR variables
    convergence_rate_stabilized::Bool
    convergence_displacement_updated::Bool
    termination_count::Int
    num_samples::Int
    num_steps_no_update::Int
    "damping for load application"
    convergence_rate::Float64
    conv_rate::Float64
    old_conv_rate::Float64

    saved_dnod::Matrix{Float64}
    saved_forc::Matrix{Float64}
    diff_disp::Matrix{Float64}
    diff_forc::Matrix{Float64}

    function DynamicRelaxationVariables(
        prm::DynamicRelaxationParameters,
        ndims::Int,
        nnodes::Int,
        stepnum_total::Int
    )
        dr = new()

        dr.converged = false

        dr.ro_time = zeros(stepnum_total)
        dr.ro_used_time = zeros(stepnum_total)
        dr.max_disp_variation_time = zeros(stepnum_total)

        dr.convergence_rate_stabilized = false
        dr.convergence_displacement_updated = false
        dr.termination_count = 0
        dr.num_samples = 0
        dr.num_steps_no_update = 0
        dr.convergence_rate = prm.DR_INITIAL_CONVERGENCE_RATE
        dr.conv_rate = prm.DR_INITIAL_CONVERGENCE_RATE
        dr.old_conv_rate = 0.

        dr.saved_dnod = zeros(Float64, nnodes, ndims)
        dr.saved_forc = zeros(Float64, nnodes, ndims)
        dr.diff_disp  = zeros(Float64, nnodes, ndims)
        dr.diff_forc  = zeros(Float64, nnodes, ndims)

        return dr
    end
end

# # TODO
# function solve(model, prm::DynamicRelaxationParametersExplicitSim)
#     # convert prm
#     # then call solution = solve(model, prm::DynamicRelaxationParameters)
# end

# TODO
# function solve(model::Model, prm::DynamicRelaxationParameters)
#     u = [0]
#     solution = (u=u)
#     return solution
# end

function solve!(
    glob::GlobalVariables{Dim},
    uservars::Union{AbstractUserVariables, Nothing},
    toc::Function, # add this to glob?
    time_step_size::Float64,
    stepnum_total::Int,
    progress_num_steps::Int,
    stepnum_load::Int,
    use_EBCIEM::Bool,
    bcs::NamedTuple,
    output_field::Union{OutputField, Nothing},
    output_qfield::Union{OutputQuadratureField, Nothing},
    output_history::Union{OutputHistory, Nothing},
    prm::DynamicRelaxationParameters
)::NamedTuple where Dim

    @info "Solve"
    @info string(prm)

    # Warnings (mostly about bugs)
    @warn "glob.disp is only updated during field output or if use_EBCIEM == true"

    # Variable to keep track of failure
    failed = false
    # Variable to keep track of final increment after convergence is achieved
    # to ensure that output is written at the end of the simulation
    # i.e. one additional increment is performed after solution has converged.
    final_increment = false

    # Length used to check for displacement divergence
    max_length = norm(maximum(glob.coord, dims=1) - minimum(glob.coord, dims=1))
    @info string("Maximum distance between points of bounding box: ", max_length)

    dr = DynamicRelaxationVariables(prm, Dim, n_nodes(glob), stepnum_total)

    dnod_correction = zeros(Float64, n_nodes(glob), Dim) # move to EBCIEM type?

    # Temporary arrays for parallel assembly using multi-threading
    forc_threads = [zeros(n_nodes(glob), Dim) for i=1:Threads.nthreads()] # move to DR type

    # TODO: split mtled.jl into small scripts that call functions from SofTiSim
    # e.g. create_qpoints(glob)
    # TODO: split this file into small functions and modules

    # TODO: is this needed?
    # Apply loading displacement at first step
    # if (model[:use_EBCIEM] == 1)
    #     u_imposed_disp[idx_imposed_disp] = load2*disp_t[1];
    # else
    #     u_tnew2[load2_idx] = load2*disp_t[1];
    # end

    glob.increment = 0
    glob.time = 0.
    total_time = prm.time_load + prm.time_eq

    @time while glob.time + time_step_size < total_time
    # while stepnum < stepnum_total
    # for stepnum = 1:stepnum_total # begin time loop

        glob.increment += 1
        glob.time += time_step_size
        # TODO: add this to glob or make loading and relaxation 2 steps with step_time like abaqus
        bc_time = min(glob.time, prm.time_load)

        if mod(glob.increment, progress_num_steps) == 0
            @info toc(string("Inc ", glob.increment, " out of ", stepnum_total))
            # Reset global flags
            global WARN_SMALL_VOLUME = true
            global WARN_ZERO_VOLUME = true
        end

        #--- Update displacements (note the order)
        glob.dnod_prev .= glob.dnod_curr           # update old displacment
        glob.dnod_curr .= glob.dnod_next           # update current displacement

        # Data passed to material models (add more variables as required)
        # TODO: pass glob instead
        # add time_total, time_step to glob e.g. loading step and relaxation step
        data = (increment=glob.increment, time=bc_time,)

        if !(output_qfield isa Nothing) && (failed || final_increment || mod(glob.increment, output_qfield.interval) == 0)
            save_quadrature_output = true
        else
            save_quadrature_output = false
        end

        if !(output_history isa Nothing) && (dr.converged || mod(glob.increment, output_history.frequency) == 0)
            save_history_output = true
        else
            save_history_output = false
        end

        # Loop over integration points to calculate nodal forces due to element stress
        glob.forc_int .= 0
        try
            assemble_force!(
                glob.forc_int,
                forc_threads,
                glob.dnod_curr,
                glob.qpoints.nodes,
                glob.qpoints.JxW,
                glob.qpoints.derivatives,
                glob.qpoints.material,
                glob.qpoints.strain_energy,
                glob.qpoints.gradient,
                data,
                glob.increment,
                save_quadrature_output,
                save_history_output,
                isa(output_qfield, Nothing) ? Nothing : output_qfield.qpoint_output)
        catch exc
            if isa(exc, NegativeJacobianDeterminantError)
                failed = true
                @goto finalize_after_failure
            else
                rethrow(exc)
            end
        end

        # Update total force
        # TODO: add external forces glob.forc_ext (body forces and Neumann BCs)
        glob.forc_tot .= glob.forc_int

        # For DR
        dr.old_conv_rate = 0
        if (glob.increment == stepnum_load)
        # keep using stepnum in DR code because need to count number of steps - maybe can use a counter instead
        # if time > prm.load_time -> start DR # i think this part should only be done once - check!
            # init variables for DR
            dr.convergence_rate = prm.DR_CONVERGENCE_RATE_AFTER_LOADING; # damping for DR
            dr.saved_dnod .= glob.dnod_curr;
            dr.saved_forc .= glob.forc_tot;
            dr.old_conv_rate = dr.convergence_rate;
            @info string("Inc ", glob.increment, ": begin relaxation with convergence rate ", dr.convergence_rate)
        end

        # TODO: reassemble mass matrix using volume scaling
        # need to use the jacobian of deformation gradient?

        # Use forces to update displacements using explicit integration and
        # mass proportional damping (Dynamic Relaxation)
        tmp = (dr.convergence_rate+1)*time_step_size/2;
        a = tmp*tmp;
        b = 1+dr.convergence_rate*dr.convergence_rate;
        glob.dnod_next .= -a*(glob.mass\glob.forc_tot)
        glob.dnod_next .-= (b-1)*glob.dnod_prev
        glob.dnod_next .+= b*glob.dnod_curr

        # check for divergence
        max_disp = maximum(abs.(glob.dnod_next[:]))
        # TODO: make this an optional model parameter
        # i.e. solver.max_disp
        # i.e. solver.max_dnod
        # if solver.max_disp == Inf
        #     max_disp = 5
        # end
        # if solver.max_dnod == Inf
        #     max_dnod = 10
        # end
        max_disp_factor = 10
        # TODO: check both disp and dnod
        if max_disp > max_disp_factor * max_length
            @warn string("Inc ", glob.increment, ": excessive displacement: ", max_disp)
        end

        # Apply essential boundary conditions
        if use_EBCIEM
            # Displacement correction
            glob.disp .= glob.phi * glob.dnod_next
            dnod_correction .= 0.
            for bc in bcs
                apply!(dnod_correction, bc, glob)
            end
            for i = 1:Dim
                dnod_correction[:, i] .=                      # d_correction
                    glob.ebc_transforms[i] *                  # P matrix
                    (dnod_correction[glob.ebc_nodes[i], i] .- # u_bar (prescribed)
                     glob.disp[glob.ebc_nodes[i], i])         # d_tilde (predicted)
            end
            glob.dnod_next .+= dnod_correction

            # Force due to dnod_correction on EBCs (for output only)
            glob.forc_ebc .= 1/a * glob.mass * dnod_correction
        else
            for bc in bcs
                apply!(glob.dnod_next, bc, glob)
            end
        end

        # Apply user-defined essential boundary conditions
        user_bc!(uservars, glob, prm, time_step_size)

        # Update dynamic relaxation variables
        update!(dr, prm, stepnum_load, glob, bcs, time_step_size)

        # Goto here if an error is caught during the analysis
        # to complete writing output and to return variables.
        @label finalize_after_failure

        # Output field
        # TODO: allow use of multiple output requests
        # for output in output_field:
        if !(output_field isa Nothing) && (failed || final_increment || mod(glob.increment, output_field.interval) == 0)
            @info string("Inc ", glob.increment, ": writing field output")

            # Compute displacement only when output is requested (to save time)
            # FIXME: this is not required for interpolating methods such as FEM
            # FIXME: should this use dnod_curr or dnod_next (EBCIEM uses dnod_next)
            glob.disp .= glob.phi * glob.dnod_curr

            write_output(output_field, glob)

            # Reset glob.disp to avoid erroneous output
            # FIXME: glob.disp should be updated when output_history is requested
            # as well but this is very time consuming and the fix is non-trivial.
            # As a temporary fix just replace G.disp with G.phi * G.dnod_curr
            # for all output_history requests in the input file.
            # See the warning at the beginning of this function.
            glob.disp .= Inf # FIXME: (see ***)
        end

        # Output quadrature point field
        if save_quadrature_output
            @info string("Inc ", glob.increment, ": writing quadrature field output")

            write_output(output_qfield, glob)

            # FIXME: is this needed or delete?
            # # Reset glob.qpoint_output to avoid erroneous output
            # for k in keys(output_qfield.qpoint_output)
            #     if occursin("_comp_", k)
            #         output_qfield.qpoint_output[k] .= Inf
            #     # elseif occursin("_principal", k)
            #     #     data = (getindex.(results[k], 1), getindex.(results[k], 2), getindex.(results[k], 3))
            #     #     WriteVTK.vtk_point_data(vtkfile, data, k)
            #     end
            # end
        end

        # Output history
        # FIXME: make glob a mutable struct that contains all simulation data
        # (see many comments above)
        if save_history_output
            # FIXME: (see ***) we need glob.phi here (OK when using EBCIEM but reset at (***)
            glob.total_strain_energy = sum(glob.qpoints.strain_energy)

            for key in keys(output_history.functions)
                push!(output_history.data[key], output_history.functions[key](glob))
            end
        end

        if failed
            @error string("Inc ", glob.increment, ": Analysis failed.")
            break
        elseif final_increment  # success: this is the last increment
            @info string("Inc ", glob.increment, ": Analysis completed (converged)")
            break
        elseif dr.converged     # success: this is the second last increment
            final_increment = true
        elseif glob.increment > prm.max_num_increments
            @warn string("Inc ", glob.increment, ": Analysis terminated",
                         " (maximum number of increments exceeded)")
            break
        end
    end

    # Update the displacements using final nodal values from last iteration
    # FIXME: see similar code above for output field
    glob.disp .= glob.phi * glob.dnod_curr

    if (glob.increment == stepnum_total)
        @warn string("Inc ", glob.increment, ": Tolerance not satisfied yet. Increase the number of steps!")
    else
        stepnum_total = glob.increment;
    end
    @info string("Inc ", glob.increment, ": Convergence rate at the end of the simulation: ", dr.convergence_rate)

    # TODO: store these in DynamicRelaxationVariables mutable struct instead
    dr_info = (
        success = !failed,

        ro_time = dr.ro_time,
        ro_used_time = dr.ro_used_time,
        max_disp_variation_time = dr.max_disp_variation_time,

        convergence_rate_stabilized = dr.convergence_rate_stabilized,
        convergence_displacement_updated = dr.convergence_displacement_updated,
        termination_count = dr.termination_count,
        num_samples = dr.num_samples,
        num_steps_no_update = dr.num_steps_no_update,
        convergence_rate = dr.convergence_rate,
        conv_rate = dr.conv_rate,
    )
    return dr_info
end

function update!(
    dr::DynamicRelaxationVariables,
    prm::DynamicRelaxationParameters,
    stepnum_load::Int,
    glob::GlobalVariables,
    bcs,
    time_step_size::Float64
)

    # save intermediate results
    dr.ro_time[glob.increment] = dr.conv_rate
    dr.ro_used_time[glob.increment] = dr.convergence_rate

    # compute maximum displacement variation
    max_disp_var = maximum(abs.(glob.dnod_next[:] .- glob.dnod_curr[:]))
    dr.max_disp_variation_time[glob.increment] = max_disp_var

    # Termination criteria and convergence rate
    if glob.increment > stepnum_load
        # if time > prm.load_time
        # Estimate the lower oscilation frequency
        dr.diff_disp .= glob.dnod_curr .- dr.saved_dnod
        dr.diff_forc .= glob.forc_tot .- dr.saved_forc
        for bc in bcs
            dr.diff_forc[bc.nodes, bc.dofs] .= 0
        end

        Ksum = sum(dr.diff_disp .* dr.diff_forc)
        Msum = sum(dr.diff_disp .* (glob.mass * dr.diff_disp))

        # Update convergence rate adaptively
        if glob.increment < (stepnum_load + prm.DR_NO_STEPS_UPDATE_CONVERGENCE_RATE)
            Ksum = abs(Ksum); # make sure this is positive
            if (Msum > 10e-14) && (Ksum > 10e-14)
                dr.num_steps_no_update = 0
                # minimum frequency
                tmp = sqrt(Ksum/Msum)
                # Kmat condition number (square root)
                Kii = 2/(tmp*time_step_size)
                # Convergence rate
                dr.conv_rate = (Kii - 1)/(Kii + 1)
                if abs(dr.conv_rate - dr.old_conv_rate) < prm.DR_CONV_RATE_DEVIATION
                    dr.num_samples = dr.num_samples + 1
                    if dr.num_samples >= prm.DR_NUM_CONV_RATE_SAMPLES
                        if dr.convergence_displacement_updated
                            dr.convergence_displacement_updated = false
                            if (abs(dr.conv_rate - dr.convergence_rate) < prm.TERMINATION_CONV_RATE_DEVIATION)
                                dr.convergence_rate_stabilized = true
                            end
                            dr.convergence_rate = dr.conv_rate;
                        end
                    end
                else
                    dr.num_samples = 0;
                end
                dr.old_conv_rate = dr.conv_rate;
            end
        else
            dr.num_steps_no_update = dr.num_steps_no_update + 1;
            if dr.num_steps_no_update >= 10*prm.DR_NUM_CONV_RATE_SAMPLES
                dr.convergence_rate_stabilized = true;
            end
        end

        # check termination criteria
        if dr.convergence_rate_stabilized
            # adjust convergence rate value
            estimated_conv_rate = dr.convergence_rate + prm.TERMINATION_CONV_RATE_ERROR * (1 - dr.convergence_rate)
            estimated_error = max_disp_var * estimated_conv_rate / (1 - estimated_conv_rate)
            if estimated_error < prm.TERMINATION_ABSOLUTE_ERROR
                dr.termination_count = dr.termination_count + 1
                if (dr.termination_count >= prm.TERMINATION_NUM_STEPS)
                    # error criteria satisfied - end iterations
                    @info string("Inc ", glob.increment, ": tolerance satisfied")
                    dr.converged = true
                end
            else
                dr.termination_count = 0
            end
        end

        # update saved forces and displacements
        if mod((glob.increment - stepnum_load), prm.DR_FORCE_DISP_UPDATE_INTERVAL) == 0
            dr.saved_dnod .= glob.dnod_curr
            dr.saved_forc .= glob.forc_tot
            dr.convergence_displacement_updated = true
        end
    end
end
