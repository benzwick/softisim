# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Parameters

"""
DynamicRelaxationParameters

Parameters for dynamic relaxation solver using ExplicitSim variable names.
"""
@with_kw struct DynamicRelaxationParametersExplicitSim
    """Time for loading application."""
    LoadTime::Float64 = 2.0

    """Time for solution to reach equilibrium in dynamic relaxation.
    Measure unit: [s]"""
    EquilibriumTime::Float64 = 10.0

    """Convergence rate during loading application."""
    LoadConvRate::Float64 = 0.999

    """Convergence rate after loading application (during dynamic relaxation)."""
    AfterLoadConvRate::Float64 = 0.99

    """Number of time steps after which convergence rate updating stops
    to avoid truncation errors."""
    StopUpdateConvRateStepsNum::Int = 2000

    """Deviation to check convergence rate stability."""
    ConvRateDeviation::Float64 = 0.0001

    """Number of steps after which forces and displacements are updated."""
    ForceDispUpdateStepsNum::Int = 200

    """Number of consecutive steps after which convergence rate
    stability requirement is considered to have been met."""
    StableConvRateStepsNum::Int = 20

    """Deviation to decide if the convergence rate has been
    stabilized between the updates."""
    ConvRateStopDeviation::Float64 = 0.002

    """Estimated error in convergence rate for simulation termination."""
    StopConvRateError::Float64 = 0.2

    """Absolute error for simulation termination."""
    StopAbsError::Float64 = 0.00001

    """Number of consecutive steps for which termination criteria must be
    satisfied before ending the simulation."""
    StopStepsNum::Int = 100
end

function convert_to_explicitsim(prm::DynamicRelaxationParameters)
    return DynamicRelaxationParametersExplicitSim(
        LoadTime                   = prm.time_load,
        EquilibriumTime            = prm.time_eq,
        LoadConvRate               = prm.DR_INITIAL_CONVERGENCE_RATE,
        AfterLoadConvRate          = prm.DR_CONVERGENCE_RATE_AFTER_LOADING,
        StopUpdateConvRateStepsNum = prm.DR_NO_STEPS_UPDATE_CONVERGENCE_RATE,
        ConvRateDeviation          = prm.DR_CONV_RATE_DEVIATION,
        ForceDispUpdateStepsNum    = prm.DR_FORCE_DISP_UPDATE_INTERVAL,
        StableConvRateStepsNum     = prm.DR_NUM_CONV_RATE_SAMPLES,
        ConvRateStopDeviation      = prm.TERMINATION_CONV_RATE_DEVIATION,
        StopConvRateError          = prm.TERMINATION_CONV_RATE_ERROR,
        StopAbsError               = prm.TERMINATION_ABSOLUTE_ERROR,
        StopStepsNum               = prm.TERMINATION_NUM_STEPS)
end

function convert_from_explicitsim(prm::DynamicRelaxationParametersExplicitSim)
    return DynamicRelaxationParameters(
        time_load                           = prm.LoadTime,
        time_eq                             = prm.EquilibriumTime,
        DR_INITIAL_CONVERGENCE_RATE         = prm.LoadConvRate,
        DR_CONVERGENCE_RATE_AFTER_LOADING   = prm.AfterLoadConvRate,
        DR_NO_STEPS_UPDATE_CONVERGENCE_RATE = prm.StopUpdateConvRateStepsNum,
        DR_CONV_RATE_DEVIATION              = prm.ConvRateDeviation,
        DR_FORCE_DISP_UPDATE_INTERVAL       = prm.ForceDispUpdateStepsNum,
        DR_NUM_CONV_RATE_SAMPLES            = prm.StableConvRateStepsNum,
        TERMINATION_CONV_RATE_DEVIATION     = prm.ConvRateStopDeviation,
        TERMINATION_CONV_RATE_ERROR         = prm.StopConvRateError,
        TERMINATION_ABSOLUTE_ERROR          = prm.StopAbsError,
        TERMINATION_NUM_STEPS               = prm.StopStepsNum)
end
