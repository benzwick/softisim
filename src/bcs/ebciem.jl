# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using LinearAlgebra
using SparseArrays

function delaunay(x)
    return scipy_spatial.Delaunay(x).simplices .+ 1
end

function transformation_matrix(M, V, phi)

    # NOTE: sparse matrix must be converted to dense matrix for ldiv
    # V must be a dense matrix

    # FIXME: this uses too much memory
    inv_M_V = M \ V
    P = sparse(Matrix(inv_M_V) / (phi*inv_M_V))
    # NxL =            NxN*NxL /  LxN* NxN*NxL  (matrix dimensions)

    return P
end

function compute_V_with_triangulation(bcs, nodes, x2, mls_interp, dofv)

    error("Not implemented")

end

function compute_V_simplified(nodes, coord, create_basis)

    V = zeros(size(coord, 1), length(nodes))

    for (i, n) in enumerate(nodes)
        neighbors, basis = create_basis(coord[n])
        V[neighbors, i] = basis
    end

    return V
end
