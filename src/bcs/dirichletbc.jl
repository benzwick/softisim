# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Parameters

"""
Dirichlet boundary condition.
"""
@with_kw struct DirichletBC
    nodes::Vector{Int}
    dofs::Vector{Int}
    magnitude::Union{Float64, Function} = 0.
    amplitude::Function = time -> 1.
end

"""
    apply!(x, bc::DirichletBC, t)

Apply Dirichlet boundary condition `bc` to vector `x` at time `t`.
"""
function apply!(dnod::Matrix{Float64},
                bc::DirichletBC,
                glob::GlobalVariables)

    amp = bc.amplitude(glob.time)

    if bc.magnitude isa Number
        dnod[bc.nodes, bc.dofs] .= amp * bc.magnitude
    else # Function
        # NOTE: glob is passed to magnitude (be careful)
        for node in bc.nodes
            dnod[node, bc.dofs] .= amp * bc.magnitude(glob.coord[node],
                                                      glob.time,
                                                      glob,
                                                      node)
        end
    end
end
