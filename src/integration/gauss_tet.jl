# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    gauss(::Tet, degree)

Gauss quadrature point coordinates and weights for tetrahedron.
"""
function gauss(::Tet, degree::Int)::QuadratureRule{3}

    if degree >= 0 && degree <= 1 # 1 point

        points = [Point(1/4, 1/4, 1/4)]

        weights = [1/6]

    elseif degree <= 2 # 4 points

        a = 0.58541019662496845446  # 1/4 + 3*sqrt(5)/20
        b = 0.13819660112501051518  # 1/4 - sqrt(5)/20

        points = [Point(a, b, b),
                  Point(b, a, b),
                  Point(b, b, a),
                  Point(b, b, b)]

        weights = 1/24 * ones(4)

    elseif degree <= 3 # 5 points (negative weight)

        points = [Point(1/4, 1/4, 1/4),
                  Point(1/2, 1/6, 1/6),
                  Point(1/6, 1/2, 1/6),
                  Point(1/6, 1/6, 1/2),
                  Point(1/6, 1/6, 1/6)]

        weights = [-2/15, 3/40, 3/40, 3/40, 3/40]

    else
        error("Quadrature rule of degree ", degree, " not supported!")
    end

    return (p = points, w = weights)
end
