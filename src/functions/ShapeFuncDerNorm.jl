# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    ShapeFuncDerNorm(centers, support, epoint, ...)

Compute the shape function derivative norm.
"""
function ShapeFuncDerNorm(
    centers::Vector{Point{3}},
    support::Vector{Float64},
    epoints::Vector{Point{3}},
    exact_derivatives::Bool,
    basic_function::BasicFunction,
    InterpMLS_3D_temp::Function
)::Matrix{Float64}

    ni = size(epoints, 1)
    norm = zeros(ni, 3)
    for i = 1:ni
        _, _, sfd = InterpMLS_3D_temp(
            centers,
            support,
            epoints[i],
            exact_derivatives,
            basic_function)

        norm[i, :] = [sum(sfd[:,1].^2) sum(sfd[:,2].^2) sum(sfd[:,3].^2)]
    end
    return norm
end
