# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    GetFaces3D_Tetr(elements)

Transform a list of 4-node tetrahedron elements
into a list of 3-node triangle faces.

The 4 nodes of an element must be numbered in a given way (Abaqus)
The 3 nodes of triangles are listed in an anti-clockwise order.
"""
function GetFaces3D_Tetr(elements)
    fc = [1 2 4
          1 3 2
          2 3 4
          1 4 3];
    x = size(elements);
    faces = zeros(Int, 4*x[1], 3);
    poz = 1;
    for kk = 1:x[1]
        el = elements[kk,:];            # get the element
        faces[poz:poz+3,:] = el[fc];    # get the faces of this element
        poz = poz + 4;
    end

    return faces
end
