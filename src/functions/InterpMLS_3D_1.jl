# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
using LinearAlgebra

"""
    InterpMLS_3D_1(glob, xi, R_node, exact_derivatives)
"""
function InterpMLS_3D_1(
    glob::GlobalVariables,
    xi::Point{3},
    exact_derivatives::Bool)

    X2 = eltorows(glob.coord)

    # Compute distance between nodes and the selected point
    Y2 = zeros(size(X2))
    Y2[:,1] = X2[:,1] .- xi[1];
    Y2[:,2] = X2[:,2] .- xi[2];
    Y2[:,3] = X2[:,3] .- xi[3];
    dist = sqrt.(Y2[:,1].^2 + Y2[:,2].^2 + Y2[:,3].^2);

    # Find the support nodes which influence this point
    closest_nodes = findall(dist .< glob.support_radius);

    R_node = @view glob.support_radius[closest_nodes];
    # Compute MLS shape functions's derivatives
    # Assemble the matrices
    coord = X2[closest_nodes,:];
    X = coord[:,1];
    Y = coord[:,2];
    Z = coord[:,3];
    # find weights: quartic spline weight funciton is used (Meshfree book P72)
    dx1 = X.-xi[1];
    dy1 = Y.-xi[2];
    dz1 = Z.-xi[3];
    Di = sqrt.(dx1.*dx1 + dy1.*dy1 + dz1.*dz1);
    # "R_node" is the support radius for each and defines the domain where Wi!=0
    ri = Di./R_node;
    Wi = 1 .- 6*ri.^2 + 8*ri.^3 - 3*ri.^4;

    # Allocate arrays
    # "m" is the numebr of polynomial; "n" is the number of support nodes
    n = length(closest_nodes)
    A = zeros(4, 4)
    B = zeros(4, n)
    pxi = @MVector zeros(4)
    # if exact_derivatives
    dA_dx = @MMatrix zeros(4, 4)
    dA_dy = @MMatrix zeros(4, 4)
    dA_dz = @MMatrix zeros(4, 4)
    dB_dx = zeros(4, n)
    dB_dy = zeros(4, n)
    dB_dz = zeros(4, n)

    # compute weighted moment matrix and B matrix
    for i = 1:n
        pxi[1] = 1.
        pxi[2] = X[i]
        pxi[3] = Y[i]
        pxi[4] = Z[i]
        # FIXME: slow (use a loop here instead to avoid memory allocation)
        A .+= Wi[i] * pxi * pxi'
        B[:,i] = Wi[i] * pxi
    end

    factA = factorize(A)
    coefT = factA\B;
    coef = coefT';
    if exact_derivatives != 0   # compute exact shape functions derivatives
        # compute derivative of weight function
        dWi_dri_div_ri = -12 .+ 24*ri - 12*ri.^2; # (dWi/dri)/ri
        dri_dx_mul_ri = -dx1./(R_node.*R_node);   # (dri/dx)*ri
        dri_dy_mul_ri = -dy1./(R_node.*R_node);   # (dri/dy)*ri
        dri_dz_mul_ri = -dz1./(R_node.*R_node);   # (dri/dz)*ri
        dWi_dx = (dWi_dri_div_ri).*dri_dx_mul_ri; # dwi/dx = (dwi/dri)*(dri/dx)
        dWi_dy = (dWi_dri_div_ri).*dri_dy_mul_ri; # dwi/dy
        dWi_dz = (dWi_dri_div_ri).*dri_dz_mul_ri; # dwi/dz
        # compute derivatives of A and B matrices
        for i = 1:n
            pxi[1] = 1.
            pxi[2] = X[i]
            pxi[3] = Y[i]
            pxi[4] = Z[i]
            # FIXME: slow (use a loop here instead to avoid memory allocation)
            dA_dx .+= dWi_dx[i] * pxi * pxi'
            dA_dy .+= dWi_dy[i] * pxi * pxi'
            dA_dz .+= dWi_dz[i] * pxi * pxi'
            dB_dx[:,i] = dWi_dx[i] * pxi
            dB_dy[:,i] = dWi_dy[i] * pxi
            dB_dz[:,i] = dWi_dz[i] * pxi
        end
        # compute sfd
        B1x = dB_dx - dA_dx*coefT;
        B1y = dB_dy - dA_dy*coefT;
        B1z = dB_dz - dA_dz*coefT;
        coefT1x = factA\B1x;
        coefT1y = factA\B1y;
        coefT1z = factA\B1z;
        coef1x = coefT1x';
        coef1y = coefT1y';
        coef1z = coefT1z';
        sfd_x = (coef[:,2] + coef1x[:,1] + coef1x[:,2]*xi[1] +
                 coef1x[:,3]*xi[2] + coef1x[:,4]*xi[3]);
        sfd_y = (coef[:,3] + coef1y[:,1] + coef1y[:,2]*xi[1] +
                 coef1y[:,3]*xi[2] + coef1y[:,4]*xi[3]);
        sfd_z = (coef[:,4] + coef1z[:,1] + coef1z[:,2]*xi[1] +
                 coef1z[:,3]*xi[2] + coef1z[:,4]*xi[3]);
    else   # compute diffuse shape functions derivatives
        sfd_x = coef[:,2];
        sfd_y = coef[:,3];
        sfd_z = coef[:,4];
    end
    sfd = [sfd_x sfd_y sfd_z];

    # compute shape functions values
    sfval = coef[:,1] + coef[:,2]*xi[1] + coef[:,3]*xi[2] + coef[:,4]*xi[3];

    return closest_nodes, sfval, sfd
end
