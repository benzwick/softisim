# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

function FEMSF_Triangular(h, xi)

    xbar = zeros(3,3)
    xk = h[1,1]; yk = h[1,2];  # coord of node k
    xl = h[2,1]; yl = h[2,2];  # coord of node l
    xm = h[3,1]; ym = h[3,2];  # coord of node m

    xbar = [  1   1   1;
              xk  xl  xm;
              yk  yl  ym];

    xinv = inv(transpose(xbar))

    fem_sf_val = xinv[1,:] + xinv[2,:]*xi[1] + xinv[3,:]*xi[2];

    return fem_sf_val
end
