# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

import CSV
import DataFrames
import Dates
import Logging
import ProgressMeter
import PyPlot
import SparseArrays
import Statistics
import WriteVTK

function solve(
    model::Dict,
    pmesh,
    solver::DynamicRelaxationParameters;
    # Options:
    outdir::String = "",        # no ouput if this string is empty (default)
    overwrite::Bool = false,    # overwrite output directory without asking
    plot_show::Bool = false,    # "Show plots interactively"
    plot_spy::Bool = false,     # "Plot sparsity patterns of matrices"
    qmesh = nothing,            # TODO: replace model[:cells] with qmesh here. And add deprecation error if haskey(cells)
    benchmark_field::Union{Nothing, DataFrames.DataFrame} = nothing,
    disp_exact_function = nothing # function (x, t) -> exact solution
)#::Tuple{GlobalVariables, String, DataFrames.DataFrame} # is this needed here; probably not

# ------------------------------------------------------------
# TODO: Check model inputs

# Generate output filename and create output directory
output_filename = initialize_output(outdir, overwrite)

# Initialise timer
tic = TicToc.tic
toc = TicToc.toc
@info "Start timing"; tic()

# ------------------------------------------------------------

@info toc()
@info "MTLED"

# ------------------------------------------------------------------------------
# Initialise global variables

# TODO: add similar functionality for user defined global variables e.g.
# user = UserVariables() where UserVariables is a mutable struct defined by user

glob = GlobalVariables{pmesh.dim}(pmesh.coordinates)

# ------------------------------------------------------------------------------
# Initialise user defined variables

if haskey(model, :uservars)
    uservars = model[:uservars]
    @info "User Variables: YES"
else
    uservars = nothing
    @info "User Variables: NO"
end

# ------------------------------------------------------------------------------
# Meshes

# TODO: if pmesh is a point cloud create the pcells by triangulation
#       (pcells are required for visualization output and supportradius)
# TODO: rename glob.coord as pmesh
# TODO: rename qcells as qmesh (quadrature mesh)? Why?
# TODO: use qmesh in place of qcells

# Quadrature mesh
if qmesh isa Nothing
    @info "Integration: qmesh is pmesh"
    qmesh = pmesh
else
    @warn "Integration: qmesh defined by USER"
end

# Create elements (cells) for approximation (pcells) and quadrature (qcells)
pcells = create_elements(pmesh.node_coordinates, pmesh.element_connectivity, pmesh.element_types)
qcells = create_elements(qmesh.node_coordinates, qmesh.element_connectivity, qmesh.element_types)

@info toc()

@info "Get 3-node triangular faces of 4-node tetrahedron elements"
faces = GetFaces3D_Tetr(pmesh.elements)

@info toc()

# ------------------------------------------------------------------------------

if !haskey(model, :approx)
    @info "Compute support radius for each node for mesh-free method"

    # FIXME: only allow variable or fixed not both
    if model[:use_variable_support_radius]
        # FIXME: if pcells are hexas split them into tetras or triangulate points
        #        to create simplices and pass those to the supportradius function
        #        because this requires tetras to compute neighbours.
        glob.support_radius = model[:support_radius_dilatation] *
            supportradius(pmesh.coordinates, pcells)
        simplices = nothing
    elseif haskey(model, :aver_node_space)
        glob.support_radius = model[:support_radius_dilatation] *
            model[:aver_node_space]*ones(n_nodes(glob));
    else
        error("Cannot create 'support_radius'")
    end

    @info toc()

    # FIXME: use approx for meshfree methods as well
    # FIXME: move this to input file
    model[:approx] = MLS(pmesh.coordinates)
    # FIXME: initialize here in mtled.jl (maybe not actually required)
    # initialize(model[:approx])
end

# ------------------------------------------------------------------------------
# Verify model

let
    if !haskey(model, :approx)
        error("Model has no :approx")
    end

    # Keys that can be used for all models
    # true:  this key is required
    # false: this key is optional
    template = Dict(
        :approx                => true,
        :bcs                   => true,
        :mass_scaling_timestep => false,
        :output_field          => false,
        :output_qfield         => false,
        :output_history        => false,
        :output_ppoints        => false,
        # :output_psupport       => false, # nodes in support of each ppoint (not implemented)
        :output_qinfluence     => false, # nodes influencing each qpoint
        :output_qmesh          => false,
        :output_qpoints        => false,
        :progress_num_steps    => true,
        :quadrature_degree     => true,
        :scale_mass            => true,
        :sections              => true,
        :uservars              => false,
    )

    # Additional keys specific to different approximations
    # NOTE: keys defined above will be overwritten here.
    if is_meshfree(model[:approx])
        merge!(template, Dict(
            :aver_node_space                 => false,
            :basic_function                  => false,
            :integration_eps                 => false,
            :number_of_tetrahedral_divisions => false,
            :support_radius_dilatation       => true,
            :use_EBCIEM                      => true,
            :use_Simplified_EBCIEM           => true,
            :use_adaptive_integration        => false,
            :use_base_functions              => true,
            :use_exact_SF_derivatives        => true,
            :use_variable_support_radius     => true,
        ))
        # TODO: some keys are not always required
        # if model[:use_EBCIEM]
        #     template[:use_Simplified_EBCIEM => true]
        # end
    elseif model[:approx] isa FEInterp
        merge!(template, Dict(
            # Add FE model keys here
        ))
    else
        error("Model has invalid :approx")
    end

    # Make a copy of model so we can pop keys
    verif = copy(model)

    # Check that all required keys exist and keep track of any extras
    err = false
    for k in keys(template)
        if k in keys(verif)
            # Remove keys as we go; then check what is left over at the end
            pop!(verif, k)
            pop!(template, k)
        elseif template[k]
            # This key is required but not provided so set error flag
            err = true
        else
            # This key is optional and was not provided so ignore it
            pop!(template, k)
        end
    end

    # Too few
    if err
        error("Model is missing these keys: ", keys(template))
    end

    # Too many
    if length(verif) > 0
        error("Model has these extraneous keys: ", keys(verif))
    end
end

# ------------------------------------------------------------------------------
# FIXME: this should not be required (move support from glob to meshfree approx)
if model[:approx] isa FEInterp
    glob.support_radius = zeros(n_nodes(glob))
end

# ------------------------------------------------------------------------------
################################################################################
# TODO: generalise this to allow any approximation e.g. finite elements
# create_approximation_basis(x, xi, approx::Approximation)
# where approx is a struct that contains all the
# parameters e.g. use_exact_SF_derivatives and
# values e.g. R_node
################################################################################

if is_meshfree(model[:approx])
    if haskey(model, :approximation)
        @warn "Approximation method: USER"
        InterpMLS_3D_temp = model[:approximation]
    else
        @info "Approximation method: DEFAULT (MMLS)"
        if model[:use_base_functions] == 1
            @info "MLS: linear basis"
            InterpMLS_3D_temp = InterpMLS_3D_1
        elseif model[:use_base_functions] == 2
            @info "MLS: quadratic basis"
            InterpMLS_3D_temp = InterpMLS_3D_2
        else
            @error "Unsupported number of base functions", model[:use_base_functions]
        end
    end
    if !haskey(model, :basic_function)
        @info "Basic Function: DEFAULT"
        model[:basic_function] = BasicQuarticSpline()
    else
        @info "Basic Function: USER"
    end
    @info "Basic Function: " * string(model[:basic_function])
else
    InterpMLS_3D_temp = nothing
end

# ------------------------------------------------------------------------------
@info "Create integration points"

# FIXME: only pass approx here (it should contain all the required info about approx/interp)
glob.qpoints = create_qpoints(glob.coord,
                              glob.support_radius,
                              qcells,
                              model,
                              InterpMLS_3D_temp,
                              model[:approx])

glob.qpoints.gradient = [@MMatrix zeros(Float64, dim(glob), dim(glob))
                         for i in 1:length(glob.qpoints)]

@info toc()

@info string("Approximation: pmesh has ", size(pmesh.coordinates,1), " nodes and ", size(pmesh.elements,1), " elements.")
@info string("Integration:   qmesh has ", size(qmesh.coordinates,1), " nodes and ", size(qmesh.elements,1), " elements.")
@info string("Integration:   number of qpoints: ", length(glob.qpoints))

@info string("Integration:   influencing nodes per qpoint (min, mean, max): (",
             minimum(length.(glob.qpoints.nodes)), ", ",
             round(Int, Statistics.mean(length.(glob.qpoints.nodes))), ", ",
             maximum(length.(glob.qpoints.nodes)), ")")

@info string("Integration:   Estimated memory for qpoints: ",
             round(Base.summarysize(glob.qpoints)/(2^20), digits=3), " MiB")

# ------------------------------------------------------------------------------
# Sections

@info "Sections:"

let
    num_qpoints = length(glob.qpoints)

    if !haskey(model, :sections)
        error("Section: model[:sections] not defined")
    elseif length(model[:sections]) < 1
        error("Section: model[:sections] has length < 1")
    else
        glob.qpoints.section_id = zeros(num_qpoints)
        for (i, section) in enumerate(model[:sections])
            @info string("Section: ", i, " (", keys(model[:sections])[i], ")")

            # check number of domain types defined for this section
            n_domains = sum([haskey(section, key) for key in [:elset, :region]])
            if n_domains < 1
                error("Section: domain not defined for this section.")
            elseif n_domains > 1
                error("Section: multiple domains defined for this section.")
            end

            # domain
            if haskey(section, :elset)
                for qp = 1:num_qpoints
                    if glob.qpoints.element_id[qp] in section.elset
                        if glob.qpoints.section_id[qp] == 0
                            glob.qpoints.section_id[qp] = i
                        else
                            error("Quadrature point ", qp,
                                  " already has a section assigned")
                        end
                    end
                end
            elseif haskey(section, :region)
                if section.region == :all
                    @info "Section: uniform section for all elements"
                    glob.qpoints.section_id = ones(num_qpoints)
                elseif section.region isa Function
                    error("Section: functions not yet supported.")
                else
                    error("Section: domain not defined.")
                end
            else
                error("Section: domain not defined.")
            end
        end

        if minimum(glob.qpoints.section_id) < 1
            error("Some integration points do not have a section assigned.")
        end
    end
end

# ------------------------------------------------------------------------------
# Materials

let
    num_qpoints = length(glob.qpoints)

    # Material properties
    glob.qpoints.density = Vector{Float64}(undef, num_qpoints)
    glob.qpoints.material = Vector{SofTiSim.Material}(undef, num_qpoints)
    glob.qpoints.wave_speed = Vector{Float64}(undef, num_qpoints)
    glob.qpoints.strain_energy = zeros(Float64, num_qpoints)

    for qp = 1:num_qpoints
        section_id = glob.qpoints.section_id[qp]
        density = float(model[:sections][section_id][:density])
        material = model[:sections][section_id][:material]

        glob.qpoints.density[qp] = density
        glob.qpoints.material[qp] = material
        glob.qpoints.wave_speed[qp] = wave_speed(density, material)
    end
end

# ------------------------------------------------------------------------------
# Critical time step and default mass scaling

glob.qpoints.mass_scaling_factor = ones(length(glob.qpoints))
glob.qpoints.critical_time_step = SofTiSim.critical_time_step(glob.qpoints)

critical_time_step_min = minimum(glob.qpoints.critical_time_step)
critical_time_step_max = maximum(glob.qpoints.critical_time_step)

@info string("Critical time steps before mass scaling (min, max): (",
             critical_time_step_min, ", ",
             critical_time_step_max, ")")

@info toc()

# ------------------------------------------------------------------------------
# Mass scaling and stable time step size

if !haskey(model, :scale_mass) || model[:scale_mass] == false
    @info "Mass scaling: OFF"
else
    @info "Mass scaling: ON"

    # Define function for computing global stable time step used for mass scaling
    # Alternative functions can be defined in input file if required e.g.
    # model[:mass_scaling_timestep = dt -> Statistics.mean(dt)
    if haskey(model, :mass_scaling_timestep)
        @warn "Mass scaling stable timestep function: USER"
        mass_scaling_timestep = model[:mass_scaling_timestep]
    else
        @info "Mass scaling stable timestep function: DEFAULT"
        mass_scaling_timestep(dt) = maximum(dt)
    end
    @assert mass_scaling_timestep isa Function

    @info "Compute mass scaling factor for each integration point"
    # mass_scaling_factors!(glob.qpoints, mass_scaling_timestep)
    glob.qpoints.mass_scaling_factor .= mass_scaling_factors(
        glob.qpoints.critical_time_step,
        mass_scaling_timestep)

    # Critical time step size AFTER mass scaling
    glob.qpoints.critical_time_step .= critical_time_step(glob.qpoints)::Vector{Float64}

    critical_time_step_min = minimum(glob.qpoints.critical_time_step)
    critical_time_step_max = maximum(glob.qpoints.critical_time_step)
end

@info string("Mass scaling factors (min, max): (",
             minimum(glob.qpoints.mass_scaling_factor), ", ",
             maximum(glob.qpoints.mass_scaling_factor), ")")

@info string("Critical time steps after mass scaling (min, max): (",
             critical_time_step_min, ", ",
             critical_time_step_max, ")")

@info string("Stable time step size: ", critical_time_step_min)

if solver.configured_time_step < Inf
    # Use time step size configured by user
    time_step_size = solver.configured_time_step
else
    # use the computed stable time step (with a safety factor)
    time_step_size = critical_time_step_min / solver.time_increment_safety_factor
end
@info string("Used time step size: ", time_step_size)

# Compute number of steps
stepnum_load = round(Int, solver.time_load / time_step_size)
stepnum_eq = round(Int, solver.time_eq / time_step_size)
stepnum_total = stepnum_load + stepnum_eq

@info string("Number of steps for load application: ", stepnum_load)
@info string("Number of steps for dynamic relaxation: ", stepnum_eq)

@info toc()

# ------------------------------------------------------------------------------
@info "Assemble Mass matrix"

let
    mass = zeros(n_nodes(glob))

    data = assemble_mass_rowsum!(
        mass,
        glob.qpoints.nodes,
        glob.qpoints.JxW,
        glob.qpoints.density,
        glob.qpoints.mass_scaling_factor)

    glob.total_mass_nodes            = data.total_mass_nodes
    glob.total_mass_qpoints          = data.total_mass_qpoints
    glob.total_mass_qpoints_unscaled = data.total_mass_qpoints_unscaled

    # Compare total mass at integration points and nodes
    @info string("Total mass at nodes:              ", glob.total_mass_nodes)
    @info string("Total mass at qpoints:            ", glob.total_mass_qpoints)
    @info string("Total mass at qpoints: (unscaled) ", glob.total_mass_qpoints_unscaled)
    @info string("Total mass scaling factor (m/m0): ", glob.total_mass_qpoints / glob.total_mass_qpoints_unscaled)

    # Check that all nodes have mass
    if minimum(glob.total_mass_nodes) < 10*eps()
        @error "Some nodes have mass close to machine precision."
    end

    # Check that mass at integration points is the same as at nodes
    if !isapprox(glob.total_mass_nodes, glob.total_mass_qpoints)
        @error "Mass at nodal points and quadrature points does not agree."
    end

    # TODO: check that min/max mass is within sane limits (0 << mass << Inf)
    @info string("Mass at nodes (min, max): ($(minimum(mass)), $(maximum(mass)))")

    # Create diagonal mass MATRIX from mass VECTOR to simplify matrix computations
    glob.mass = Diagonal(mass)
    mass = nothing
end

# ------------------------------------------------------------------------------
# MLS shape functions used for interpolation uh=phi*u and EBCIEM BCs

if is_interpolating(model[:approx])
    glob.phi = Diagonal(ones(n_nodes(glob)))
else
    @info "Create MLS shape functions"
    phi = zeros(n_nodes(glob), n_nodes(glob))
    if SHOW_PROGRESS_METER
        progress = ProgressMeter.Progress(n_nodes(glob), desc="Shape functions at nodes: ")
    end
    Threads.@threads for n = 1:n_nodes(glob)
        neighbors, phi_i, _ = InterpMLS_3D_temp(
            glob.coord,
            glob.support_radius,
            glob.coord[n],
            model[:use_exact_SF_derivatives],
            model[:basic_function])
        phi[n, neighbors] = transpose(phi_i)
        if SHOW_PROGRESS_METER
            ProgressMeter.next!(progress)
        end
    end
    glob.phi = SparseArrays.sparse(phi)
    phi = nothing
    @info toc()
end

@info string("Estimated memory for glob.phi: ",
             round(Base.summarysize(glob.phi)/(2^20), digits=3), " MiB")

# ------------------------------------------------------------------------------
# EBCIEM

if !is_meshfree(model[:approx])
    # Do not use EBCIEM with non-meshfree methods
    if haskey(model, :use_EBCIEM)
        error("Cannot use EBCIEM without meshfree method")
    else
        model[:use_EBCIEM] = false
    end
else
    if model[:use_EBCIEM]
        @info "EBCIEM: Create transformation matrices"

        nodes = [Set(), Set(), Set()]
        for bc in model[:bcs]
            for dof in bc.dofs
                union!(nodes[dof], bc.nodes)
            end
        end
        glob.ebc_nodes = [sort(collect(nodes[i])) for i = 1:3]

        # Create V matrix
        create_basis(x) = InterpMLS_3D_temp(
            glob.coord,
            glob.support_radius,
            x,
            model[:use_exact_SF_derivatives],
            model[:basic_function])

        if model[:use_Simplified_EBCIEM]
            # Lumped external force on EB. Numerical Integration on EB: NO
            @info "EBCIEM: external force is LUMPED"
            V = [compute_V_simplified(glob.ebc_nodes[i], glob.coord, create_basis)
                 for i = 1:dim(glob)]
        else
            # Distributed external force on EB. Numerical Integration on EB: YES
            @info "EBCIEM: external force is DISTRIBUTED"
            error("EBCIEM not implemented use SEBCIEM!")
        end
        # Create EBCIEM correction transformation matrix for each dim
        glob.ebc_transforms =
            [transformation_matrix(glob.mass, V[i], glob.phi[glob.ebc_nodes[i], :])
             for i = 1:dim(glob)]

        # Info
        for i = 1:dim(glob)
            @info string("EBCIEM V" * string(i) * " ",
                         "size: ", size(V[i]), ", ")
            @info string("EBCIEM P" * string(i) * " ",
                         "size: ", size(glob.ebc_transforms[i]), ", ",
                         "length: ", length(glob.ebc_transforms[i]), ", ",
                         "nonzeros: ", length(nonzeros(glob.ebc_transforms[i])))
        end
        # TODO: print memory usage for EBCIEM matrices

        @info toc()

        # Spy
        if plot_spy
            @info "EBCIEM: Plot transformation matrix sparsity patterns"
            if !plot_show
                @info "FIXME: disable interactive plotting"
            end
            fig, ax = PyPlot.subplots(1, 2*dim(glob), figsize=(10, 6))
            d = dim(glob)
            for i = 1:d
                ax[i].spy(V[i])
                ax[i].set_xlabel("\$V_"*string(i)*"\$")
                ax[i].yaxis.set_ticks([0, size(V[i], 1)])
                ax[i].xaxis.set_ticks([0, size(V[i], 2)])
            end
            for i = 1:d
                ax[d+i].spy(glob.ebc_transforms[i])
                ax[d+i].set_xlabel("\$P_"*string(i)*"\$")
                ax[d+i].yaxis.set_ticks([0, size(glob.ebc_transforms[i], 1)])
                ax[d+i].xaxis.set_ticks([0, size(glob.ebc_transforms[i], 2)])
            end
            fig.savefig(output_filename * ".plot.ebciem-matrices.pdf")
            @info toc()
        end
    end
end

# ------------------------------------------------------------------------------
# User pre solve

user_pre_solve!(uservars, glob)

# ------------------------------------------------------------------------------
# Initialise output

# Nodal point field output (pfield)
if haskey(model, :output_field) && (model[:output_field] isa OutputField)
    if outdir ==""
        error("Attempted to initialize output_field without an output directory.")
    end
    @info "Output Field: Initialize"
    output_field = model[:output_field]
    initialise_output!(output_field, filename=output_filename, mesh=pmesh,
                       disp_exact_function=disp_exact_function)
    @info string("Output Field: variables == ", output_field.variables)
    @info "Output Field: " * toc()
else
    output_field = nothing
    @info "Output Field: nothing"
end

# Quadrature point field output (qfield)
if haskey(model, :output_qfield) && (model[:output_qfield] isa OutputQuadratureField)
    if outdir ==""
        error("Attempted to initialize output_qfield without an output directory.")
    end
    @info "Output quadrature field: Initialize"
    output_qfield = model[:output_qfield]
    initialise_output!(output_qfield, filename=output_filename,
                       qpoint_coords=glob.qpoints.coordinates)
    @info string("Output quadrature field: variables == ", output_qfield.variables)
    @info "Output quadrature field: " * toc()
else
    output_qfield = nothing
    @info "Output quadrature field: nothing"
end

# History output
# TODO: allow history_output without outdir for returning the output_history_dataframe
if haskey(model, :output_history) && (model[:output_history] isa OutputHistory)
    if outdir ==""
        error("Attempted to initialize output_history without an output directory.")
    end
    @info "Output History: Initialize"
    output_history = model[:output_history]
    initialise_output!(output_history, filename=output_filename, mesh=pmesh)
    # FIXME: This prints the entire mesh to the log file!
    # @info string("Output History: functions == ", output_history.functions)
    @info "Output History: " * toc()
else
    output_history = nothing
    @info "Output History: nothing"
end

# ------------------------------------------------------------------------------
# Output pre-computed data

# ppoints: approximation/interpolation points output
if haskey(model, :output_ppoints)
    let
        @info "Writing ppoints (approximation node coordinates and data) to file"

        vtkfile = WriteVTK.vtk_grid(output_filename * ".ppoints.vtu",
                                    eltocols(glob.coord),
                                    [WriteVTK.MeshCell(WriteVTK.VTKCellTypes.VTK_VERTEX, [i])
                                     for i in 1:length(glob.coord)])

        # Add nodal point data
        vtkfile["node", WriteVTK.VTKPointData()]           = 1:n_nodes(glob)
        # TODO: check that mass is correct (esp. when non-diagonal):
        vtkfile["mass", WriteVTK.VTKPointData()]           = sum(glob.mass, dims=2)
        vtkfile["phi_i", WriteVTK.VTKPointData()]          = diag(glob.phi)
        vtkfile["phi_rowsum", WriteVTK.VTKPointData()]     = sum(glob.phi, dims=2)
        vtkfile["support_radius", WriteVTK.VTKPointData()] = glob.support_radius

        WriteVTK.vtk_save(vtkfile)
    end
end

# qinfluence: nodes influencing each quadrature point output
if haskey(model, :output_qinfluence)
    let
        @info "Writing qinfluence (nodes infuencing each quadrature point) to files"
        # NOTE: model[:output_qinfluence] should be a list of quadrature point ids
        for qp in model[:output_qinfluence]
            # nodes that have this qp in their support (they influence the values at this qp)
            inf_nodes = glob.qpoints.nodes[qp]
            vtkfile = WriteVTK.vtk_grid(output_filename * ".qinfluence-$(qp).vtu",
                                        eltocols(glob.coord[inf_nodes]),
                                        [WriteVTK.MeshCell(WriteVTK.VTKCellTypes.VTK_VERTEX, [i])
                                         for i in 1:length(inf_nodes)])

            # Add data
            vtkfile["node_id", WriteVTK.VTKPointData()]        = inf_nodes
            vtkfile["support_radius", WriteVTK.VTKPointData()] = glob.support_radius[inf_nodes]

            WriteVTK.vtk_save(vtkfile)
        end
    end
end

# qpoints: quadrature points output
if haskey(model, :output_qpoints)
    let
        @info "Writing qpoints (quadrature point coordinates and data) to file"
        vtkfile = WriteVTK.vtk_grid(output_filename * ".qpoints.vtu",
                                    eltocols(glob.qpoints.coordinates),
                                    [WriteVTK.MeshCell(WriteVTK.VTKCellTypes.VTK_VERTEX, [i])
                                     for i in 1:length(glob.qpoints)])

        # Add quadrature point data
        vtkfile["qpoint_id", WriteVTK.VTKPointData()]           = collect(1:length(glob.qpoints))
        vtkfile["element_id", WriteVTK.VTKPointData()]          = glob.qpoints.element_id
        vtkfile["section_id", WriteVTK.VTKPointData()]          = glob.qpoints.section_id
        vtkfile["JxW", WriteVTK.VTKPointData()]                 = glob.qpoints.JxW
        vtkfile["num nodes", WriteVTK.VTKPointData()]           = length.(glob.qpoints.nodes)
        # TODO: add some data about derivatives
        vtkfile["density", WriteVTK.VTKPointData()]             = glob.qpoints.density
        vtkfile["wave_speed", WriteVTK.VTKPointData()]          = glob.qpoints.wave_speed
        vtkfile["critical_time_step", WriteVTK.VTKPointData()]  = glob.qpoints.critical_time_step
        vtkfile["mass_scaling_factor", WriteVTK.VTKPointData()] = glob.qpoints.mass_scaling_factor

        WriteVTK.vtk_save(vtkfile)
    end
end

# qmesh: quadrature mesh output
if haskey(model, :output_qmesh)
    let
        @info "Writing qmesh (quadrature mesh) to file"

        # Get the corresponding VTK cell type assuming
        # all element types are the same as the first.
        # FIXME: this does not support mesh with mixed cell types
        celltype = Dict(
            :Tet4  => WriteVTK.VTKCellTypes.VTK_TETRA,
            :Hex8  => WriteVTK.VTKCellTypes.VTK_HEXAHEDRON,
            :Tet10 => WriteVTK.VTKCellTypes.VTK_QUADRATIC_TETRA,
            :Hex20 => WriteVTK.VTKCellTypes.VTK_QUADRATIC_HEXAHEDRON,
        )[qmesh.element_types[1]]

        vtkcells = WriteVTK.MeshCell[]
        for elem in qmesh.element_connectivity
            push!(vtkcells, WriteVTK.MeshCell(celltype, elem))
        end

        vtkfile = WriteVTK.vtk_grid(output_filename * ".qmesh.vtu",
                                    eltocols(qmesh.node_coordinates),
                                    vtkcells)

        # TODO: need to do a mapping from qpoints to qcells
        # vtkfile["element_id", WriteVTK.VTKCellData()] = glob.qpoints.element_id

        WriteVTK.vtk_save(vtkfile)
    end
end

# ------------------------------------------------------------------------------
@info "Solve"

@time results = solve!(
    glob,
    uservars,
    toc,
    time_step_size,
    stepnum_total,
    model[:progress_num_steps],
    stepnum_load,
    model[:use_EBCIEM],
    model[:bcs],
    output_field,
    output_qfield,
    output_history,
    solver)

# ------------------------------------------------------------------------------
# Finish output

if !(output_field isa Nothing)
    finish(output_field)
end

if !(output_qfield isa Nothing)
    finish(output_qfield)
end

if !(output_history isa Nothing)
    output_history_dataframe = finish(output_history)
else
    output_history_dataframe = nothing
end

# ------------------------------------------------------------------------------
# User post solve

user_post_solve!(uservars, glob)

# ------------------------------------------------------------------------------
# TODO: make a function or move somewhere else?

if length(output_filename) > 0
    @info "Write final field variables to csv file"

    df = DataFrames.DataFrame()

    df[!, :nodes] = pmesh.nodes
    df[!, :coord_1] = getindex.(pmesh.coordinates, 1)
    df[!, :coord_2] = getindex.(pmesh.coordinates, 2)
    df[!, :coord_3] = getindex.(pmesh.coordinates, 3)
    df[!, :disp_1] = glob.disp[:, 1]
    df[!, :disp_2] = glob.disp[:, 2]
    df[!, :disp_3] = glob.disp[:, 3]
    df[!, :dnod_curr_1] = glob.dnod_curr[:, 1]
    df[!, :dnod_curr_2] = glob.dnod_curr[:, 2]
    df[!, :dnod_curr_3] = glob.dnod_curr[:, 3]
    df[!, :dnod_next_1] = glob.dnod_next[:, 1]
    df[!, :dnod_next_2] = glob.dnod_next[:, 2]
    df[!, :dnod_next_3] = glob.dnod_next[:, 3]
    df[!, :forc_tot_1] = glob.forc_tot[:, 1]
    df[!, :forc_tot_2] = glob.forc_tot[:, 2]
    df[!, :forc_tot_3] = glob.forc_tot[:, 3]
    df[!, :forc_ebc_1] = glob.forc_ebc[:, 1]
    df[!, :forc_ebc_2] = glob.forc_ebc[:, 2]
    df[!, :forc_ebc_3] = glob.forc_ebc[:, 3]

    # Write to CSV file
    # CSV.write(output_filename * ".field." * string(glob.increment) * ".csv", df)
    # Add increment to file name only if not the final increment
    # otherwise there will be confusion when trying to find the final file
    # TODO: create a OutputFieldCSV writer to use instead of VTK (not urgent)
    # We only need the final (static) solution so just use this:
    CSV.write(output_filename * ".field.csv", df)
end

# ------------------------------------------------------------------------------

if benchmark_field isa DataFrames.DataFrame
    let
        @info "Writing benchmark field comparison to file"
        # TODO: refactor - similar code in outputvtk.jl

        # Get the corresponding VTK cell type assuming
        # all element types are the same as the first.
        # FIXME: this does not support mesh with mixed cell types
        celltype = Dict(
            :Tet4  => WriteVTK.VTKCellTypes.VTK_TETRA,
            :Hex8  => WriteVTK.VTKCellTypes.VTK_HEXAHEDRON,
            :Tet10 => WriteVTK.VTKCellTypes.VTK_QUADRATIC_TETRA,
            :Hex20 => WriteVTK.VTKCellTypes.VTK_QUADRATIC_HEXAHEDRON,
        )[pmesh.element_types[1]]

        vtkcells = WriteVTK.MeshCell[]
        for elem in pmesh.element_connectivity
            push!(vtkcells, WriteVTK.MeshCell(celltype, elem))
        end

        vtkfile = WriteVTK.vtk_grid(output_filename * ".benchmark_field.vtu",
                                    eltocols(pmesh.node_coordinates),
                                    vtkcells)

        # Fields to output (same as fields in glob and prefix used in field.csv)
        fields = [:disp, :forc_tot]
        for col in fields
            a = getfield(glob, col)
            b = hcat(benchmark_field[!, Symbol(col, "_1")],
                     benchmark_field[!, Symbol(col, "_2")],
                     benchmark_field[!, Symbol(col, "_3")])

            # Errors
            eabs = abs.(a .- b)    # absolute error
            erel = eabs ./ abs.(b) # relative error

            WriteVTK.vtk_point_data(vtkfile, (a[:,1], a[:,2], a[:,3]), "$(col)")
            WriteVTK.vtk_point_data(vtkfile, (b[:,1], b[:,2], b[:,3]), "$(col)_bench")
            WriteVTK.vtk_point_data(vtkfile, (eabs[:,1], eabs[:,2], eabs[:,3]), "$(col)_bench_error_absolute")
            WriteVTK.vtk_point_data(vtkfile, (erel[:,1], erel[:,2], erel[:,3]), "$(col)_bench_error_relative")
        end
        WriteVTK.vtk_save(vtkfile)
    end
end

# ------------------------------------------------------------------------------
@info toc()
@info "MTLED: Finished"
@info string("End time: ", string(Dates.now()))

# TODO: put this in finalize.jl
# Reset logger
Logging.global_logger(LOGGER_DEFAULT[])

return glob, output_filename, output_history_dataframe, results

end
