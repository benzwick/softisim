# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    elastic_moduli(kwargs...)

Compute other elastic moduli, given any pair of elastic moduli.

# Examples
```jldoctest
julia> elastic_moduli(E=3000, nu=0.49)
Dict{Symbol,Real} with 6 entries:
  :lambda => 49328.9
  :M      => 51342.3
  :G      => 1006.71
  :nu     => 0.49
  :K      => 50000.0
  :E      => 3000
```

```jldoctest
julia> elastic_moduli(E=200, nu=0.3)[:lambda]
115.38461538461539
```
"""
function elastic_moduli(; kwargs...)
    if length(kwargs) != 2
        throw(ArgumentError("elastic_moduli requires 2 moduli not $(length(kwargs))"))
    end

    parameters = Dict(kwargs)

    K      = get(parameters, :K, nothing)
    E      = get(parameters, :E, nothing)
    lambda = get(parameters, :lambda, nothing)
    G      = get(parameters, :G, nothing)
    nu     = get(parameters, :nu, nothing)
    M      = get(parameters, :M, nothing)

    # FIXME: sort(t::Tuple) should be added to Julia soon; use that instead
    given = tuple(sort([keys(kwargs)...])...)

    if (:E, :K) === given
        return (
            K      = K,
            E      = E,
            lambda = (3*K*(3*K-E))/(9*K-E),
            G      = (3*K*E)/(9*K-E),
            nu     = (3*K-E)/(6*K),
            M      = (3*K*(3*K+E))/(9*K-E))

    elseif (:K, :lambda) === given
        return (
            K      = K,
            E      = (9*K*(K-lambda))/(3*K-lambda),
            lambda = lambda,
            G      = (3*(K-lambda))/2,
            nu     = lambda/(3*K-lambda),
            M      = 3*K-2*lambda)

    elseif (:G, :K) === given
        return (
            K      = K,
            E      = (9*K*G)/(3*K+G),
            lambda = K-(2*G)/3,
            G      = G,
            nu     = (3*K-2*G)/(2*(3*K+G)),
            M      = K+(4*G)/3)

    elseif (:K, :nu) === given
        return (
            K      = K,
            E      = 3*K*(1-2*nu),
            lambda = (3*K*nu)/(1+nu),
            G      = (3*K*(1-2*nu))/(2*(1+nu)),
            nu     = nu,
            M      = (3*K*(1-nu))/(1+nu))

    elseif (:K, :M) === given
        return (
            K      = K,
            E      = (9*K*(M-K))/(3*K+M),
            lambda = (3*K-M)/2,
            G      = (3*(M-K))/4,
            nu     = (3*K-M)/(3*K+M),
            M      = M)

    elseif (:E, :lambda) === given
        R = sqrt(E^2 + 9*lambda^2 + 2*E*lambda)
        return (
            K      = (E + 3*lambda + R)/6,
            E      = E,
            lambda = lambda,
            G      = (E-3*lambda+R)/4,
            nu     = (2*lambda)/(E+lambda+R),
            M      = (E-lambda+R)/2)

    elseif (:E, :G) === given
        return (
            K      = (E*G)/(3*(3*G-E)),
            E      = E,
            lambda = (G*(E-2*G))/(3*G-E),
            G      = G,
            nu     = E/(2*G)-1,
            M      = (G*(4*G-E))/(3*G-E))

    elseif (:E, :nu) === given
        return (
            K      = E/(3*(1-2*nu)),
            E      = E,
            lambda = (E*nu)/((1+nu)*(1-2*nu)),
            G      = E/(2*(1+nu)),
            nu     = nu,
            M      = (E*(1-nu))/((1+nu)*(1-2*nu)))

    elseif (:E, :M) === given
        # Note:
        # There are two valid solutions for S:
        # The plus sign leads to nu >= 0;
        # The minus sign leads to nu <= 0.
        # Here nu is assumed to be positive.
        @warn "Poisson's ratio is assumed to be positive"
        S = sqrt(E^2 + 9*M^2 - 10*E*M)
        return (
            K      = (3*M-E+S)/6,
            E      = E,
            lambda = (M-E+S)/4,
            G      = (3*M+E-S)/8,
            nu     = (E-M+S)/(4*M),
            M      = M)

    elseif (:G, :lambda) === given
        return (
            K      = lambda + (2*G)/3,
            E      = (G*(3*lambda+2*G))/(lambda+G),
            lambda = lambda,
            G      = G,
            nu     = lambda/(2*(lambda + G)),
            M      = lambda+2*G)

    elseif (:lambda, :nu) === given
        # Note: Cannot be used when nu=0 and/or lambda=0).
        if lambda == 0 || nu == 0
            throw(ArgumentError("Poisson's ratio and/or lambda cannot be zero"))
        end
        return (
            K      = (lambda*(1+nu))/(3*nu),
            E      = (lambda*(1+nu)*(1-2*nu))/nu,
            lambda = lambda,
            G      = (lambda*(1-2*nu))/(2*nu),
            nu     = nu,
            M      = (lambda*(1-nu))/nu)

    elseif (:M, :lambda) === given
        return (
            K      = (M+2*lambda)/3,
            E      = ((M-lambda)*(M+2*lambda))/(M+lambda),
            lambda = lambda,
            G      = (M-lambda)/2,
            nu     = lambda/(M+lambda),
            M      = M)

    elseif (:G, :nu) === given
        return (
            K      = (2*G*(1+nu))/(3*(1-2*nu)),
            E      = 2*G*(1+nu),
            lambda = (2*G*nu)/(1-2*nu),
            G      = G,
            nu     = nu,
            M      = (2*G*(1-nu))/(1-2*nu))

    elseif (:G, :M) === given
        return (
            K      = M - (4*G)/3,
            E      = (G*(3*M-4*G))/(M-G),
            lambda = M-2*G,
            G      = G,
            nu     = (M-2*G)/(2*M-2*G),
            M      = M)

    elseif (:M, :nu) === given
        return (
            K      = (M*(1+nu))/(3*(1-nu)),
            E      = (M*(1+nu)*(1-2*nu))/(1-nu),
            lambda = (M*nu)/(1-nu),
            G      = (M*(1-2*nu))/(2*(1-nu)),
            nu     = nu,
            M      = M)

    else
        throw(ArgumentError("Unknown combination of parameters: $given"))
    end
end
