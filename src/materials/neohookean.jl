# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using LinearAlgebra

# NOTE: call this NeoHookean material
# and store the density separately. Then can combine
# viscous with elastic and plastic etc. like abaqus
# all integration points (materials) have density.
# Density is not a material/consitutive parameter
"""
Neo-Hookean material.
"""
struct NeoHookean <: Material
    """Lamé's second parameter μ (shear modulus)"""
    mu::Float64
    """Bulk modulus κ"""
    kappa::Float64
end

"""
    spkstress(FT, material)

Compute second Piola--Kirchoff stress for a neo-Hookean material.
"""
function spkstress(FT::Array{Float64, 2}, material::NeoHookean)
# function spkstress(C::Array{Float64, 2}, material::NeoHookean)

    J = det(FT)    # Determinant of deformation gradient (transposed)
    C = FT*FT'     # Right Cauchy Green deformation tensor
    # J = sqrt(det(C))
    Cinv = inv(C)  # Inverse of right Cauchy Green Deformation
    I1 = tr(C)     # First invariant

    return material.mu*J^(-2/3)*(I - I1/3*Cinv) + material.kappa*J*(J-1)*Cinv
end

"""
    spkstress!(spks_mat, FT, material)

Compute second Piola--Kirchoff stress for a neo-Hookean material.
"""
function spkstress!(spks_mat::Array{Float64, 2}, FT::Array{Float64, 2}, material::NeoHookean)

    mu_qp = material.mu
    kappa_qp = material.kappa

    # J = det(F)     # Determinant of Def Grad
    # C = F*F'       # Right Cauchy Green deformation tensor
    C = zeros(Float64, 3, 3)
    Cinv = zeros(Float64, 3, 3)
    # C = AxAT3x3!(C, F)       # Right Cauchy Green deformation tensor
    # C = rightcauchygreen(F)
    # Cinv = inv3x3(C)  # Inverse of the Right Cauchu Green Deformation
    # I1 = trace3x3(C)     # First invariant

    # Calculate invariants using FMT
    J = det3x3(FT)         # Determinant of mechanical def grad
    AxAT3x3!(C, FT)        # Right Cauchy-Green (C = F' * F == FT * FT')
    inv3x3!(Cinv, C)       # Inverse of Right Cauchy-Green
    I1 = trace3x3(C)       # First invariant
    JJ = J * J             # J^2

    # # # Calculate invariants using FT
    # J = det3x3(FT) / (swstretch * swstretch * swstretch) # Determinant of mechanical def grad
    # AxAT3x3!(C, FT)              # Right Cauchy-Green (C = F' * F == FT * FT')
    # inv3x3!(Cinv, C)             # Inverse of Right Cauchy-Green
    # I = trace3x3(C) / (swstretch * swstretch) # First invariant
    # JJ = J * J                              # J^2

    # Modified neo-Hookean material SPK stress calculation

    # Contribution of the hydrostatic work term dU/dJ to the stress Sij
    # (only one of these is used to calculate SPK
    # Use either u1 or u2 to define the contribution from the hydrostatic work
    # u1 Derived from Simo & Hughes eq. 9.2.3 (21-Jun-2014)
    # u2 Derived from Zienkiewiecz (20-Jun-2014) - equivalent to Abaqus material
    u1 = 0.5 * (JJ - 1)             # Simo & Hughes U(J) = 1/2*(1/2(J^2-1) - ln(J))
    u2 = J * (J - 1)                # Zienkiewicz   U(J) = 1/2*(J-1)^2

    x1 = mu_qp / cbrt(JJ)
    # NOTE: Use u1 or u2 in this equation depending on the model
    x2 = (-x1 * I1 / 3.0) + (kappa_qp * u2)

    # Calculate SPK stress wrt intermediate configuration
    # (stress is caused by mechanical strain only)
    x0 = 1 # swstretch
    spks_mat[1,1] = x0 * (x1 + x2 * Cinv[1,1])
    spks_mat[2,1] = x0 * (     x2 * Cinv[2,1])
    spks_mat[3,1] = x0 * (     x2 * Cinv[3,1])
    spks_mat[1,2] = x0 * (     x2 * Cinv[1,2])
    spks_mat[2,2] = x0 * (x1 + x2 * Cinv[2,2])
    spks_mat[3,2] = x0 * (     x2 * Cinv[3,2])
    spks_mat[1,3] = x0 * (     x2 * Cinv[1,3])
    spks_mat[2,3] = x0 * (     x2 * Cinv[2,3])
    spks_mat[3,3] = x0 * (x1 + x2 * Cinv[3,3])

    # println(spks_mat)

    # return spks_mat
    # return mu_qp*J^(-2/3)*(I - I1/3*Cinv) + kappa_qp*J*(J-1)*Cinv
end

function det3x3(A::Array{Float64,2})
# Calculate the determinant of a 3x3 matrix "A".
# Ref: http://www.mathworks.com.au/help/aeroblks/determinantof3x3matrix.html

    detA = A[1,1] * (A[2,2]*A[3,3] - A[2,3]*A[3,2]) -
           A[1,2] * (A[2,1]*A[3,3] - A[2,3]*A[3,1]) +
           A[1,3] * (A[2,1]*A[3,2] - A[2,2]*A[3,1])

    return detA
end

function trace3x3(A::Array{Float64,2})
    # Calculate the trace of a 3x3 matrix "A".
    traceA = A[1,1] + A[2,2] + A[3,3]

    return traceA
end

function inv3x3(A::Array{Float64,2})
    # Calculate the inverse of a 3x3 matrix "A".
    Ainv = zeros(3,3)

    inv3x3!(Ainv, A)

    # # For testing the function against inbuilt inv()
    # if sum(abs(Ainv .- inv(A))) > 1e-16
    #     println(Ainv)
    #     println(inv(A))
    #     error("Problem with matrix inverse")
    # end

    return Ainv
end

function inv3x3!(Ainv::Array{Float64,2}, A::Array{Float64,2})
    # Calculate the inverse of a 3x3 matrix "A" and write results to "Ainv".
    # Ref: http://mathworld.wolfram.com/MatrixInverse.html

    c = 1/det3x3(A)

    A11 = A[1,1]; A12 = A[1,2]; A13 = A[1,3]
    A21 = A[2,1]; A22 = A[2,2]; A23 = A[2,3]
    A31 = A[3,1]; A32 = A[3,2]; A33 = A[3,3]

    Ainv[1,1] = c*(A33*A22-A23*A32)
    Ainv[2,1] = c*(A13*A32-A12*A33)
    Ainv[3,1] = c*(A12*A23-A13*A22)
    Ainv[1,2] = c*(A23*A31-A21*A33)
    Ainv[2,2] = c*(A11*A33-A13*A31)
    Ainv[3,2] = c*(A13*A21-A11*A23)
    Ainv[1,3] = c*(A21*A32-A22*A31)
    Ainv[2,3] = c*(A12*A31-A11*A32)
    Ainv[3,3] = c*(A11*A22-A12*A21)
end

function AxAT3x3!(AAT::Array{Float64,2}, A::Array{Float64,2})
    # Calculate A * transpose(A) for 3x3 matrix
    AAT[1,1] = A[1,1]*A[1,1] + A[1,2]*A[1,2] + A[1,3]*A[1,3]
    AAT[1,2] = A[1,1]*A[2,1] + A[1,2]*A[2,2] + A[1,3]*A[2,3]
    AAT[1,3] = A[1,1]*A[3,1] + A[1,2]*A[3,2] + A[1,3]*A[3,3]
    AAT[2,2] = A[2,1]*A[2,1] + A[2,2]*A[2,2] + A[2,3]*A[2,3]
    AAT[2,3] = A[2,1]*A[3,1] + A[2,2]*A[3,2] + A[2,3]*A[3,3]
    AAT[3,3] = A[3,1]*A[3,1] + A[3,2]*A[3,2] + A[3,3]*A[3,3]
    # Product is symmetric
    AAT[2,1] = AAT[1,2]
    AAT[3,1] = AAT[1,3]
    AAT[3,2] = AAT[2,3]
end
