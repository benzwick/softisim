# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
Swelling material.
"""
struct Swelling <: Material
    """Elastic material constitutive model"""
    elastic::Material
    """Swelling function"""
    # TODO: f(t) or function of something else,
    # or update each time depending on other variables?
    swelling::Function

    """Default constructor"""
    function Swelling(elastic::Material, swelling::Function)
        new(elastic, swelling)
    end
end

function wave_speed(density, material::Swelling)
    return wave_speed(density, material.elastic)
end

function spkstress(FT::SMatrix{3, 3, Float64, 9},
                   material::Swelling,
                   data)

    # This is uniform swelling, but could easily be defined by QP
    # or define/create QPs using sections;
    # then some might have swelling others not.
    # TODO: use Fs or lambdas or something else here?
    Fs = material.swelling(data.time)
    Js = det(Fs)

    F = transpose(FT)           # FIXME: simplify to eliminate this variable

    Fe = F * inv(Fs)

    FeT = transpose(Fe)         # FIXME: simplify to eliminate this variable

    Se = spkstress(FeT, material.elastic, data)

    S = Js * inv(Fs) * Se * transpose(inv(Fs))
end
