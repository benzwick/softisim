# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

#------------------------------------------------------------------------------

"""Material abstract base type."""
abstract type Material end

#------------------------------------------------------------------------------
# kinematics

# TODO: use derivatives directly instead of F to avoid rounding errors
function rightcauchygreen(F)
    return transpose(F) * F
end

function leftcauchygreen(F)
    return transpose(F) * F
end

# function invariant1
# function invariant2
# function invariant3

# Derivatives of strain energy w.r.t. to invariants of C
# function dwdIc
# function dwdIIc
# function dwdIIIc
