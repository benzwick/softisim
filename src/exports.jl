# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

export
    #
    dim,
    is_interpolating,
    is_meshfree,
    n_nodes,

    # approx
    BasicFunction,
    BasicGaussian,
    BasicQuarticSpline,
    FELagrange,
    MLS,

    # bcs/amplitude
    smooth345,
    # bcs/...
    DirichletBC,

    # geometry
    # abstract elements
    Point,
    Elem,
    Edge,
    Face,
    Cell,
    Hex,
    Tet,
    # properties
    n_dims,
    n_nodes,
    n_sides,
    n_edges,
    n_children,
    n_nodes_per_side,
    n_nodes_per_edge,
    reference_coordinates,
    reference_coordinates_as_matrix,
    reference_coordinates_as_points,
    reference_element,
    # functions
    plot_element,
    volume,
    # concrete elements
    Hex8,
    Hex20,
    Tet4,
    Tet10,

    # io
    OutputFieldVtk,
    OutputQuadratureFieldVtk,
    OutputHistory,
    rename_abaqus_to_softisim!,

    # materials
    elastic_moduli,
    NeoHookean,
    Ogden,
    Swelling,
    wave_speed,

    # postprocess
    nrmse,
    exact_cube_uniaxial,
    exact_cube_uniaxial_deformation_gradient,

    # mesh
    Mesh,
    mean_spacing,
    meshbox,
    nodes_on_bounds,
    readmesh,
    rotate_mesh!,
    rotate_mesh_x!,
    rotate_mesh_y!,
    rotate_mesh_z!,

    # solvers
    DynamicRelaxationParameters,
    solve
