# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    meshbox(p0, p1, nx, ny, nz, element_type)

Generate a uniform mesh
of the 3D rectangular prism
spanned by two points `p0` and `p1`
using `element_type` elements.
Given the number of elements (`nx`, `ny`, `nz`) in each direction,
the total number of tetrahedra will be `6*nx*ny*nz` and
the total number of hexahedra will be `nx*ny*nz`.
The total number of vertices will be `(nx + 1)*(ny + 1)*(nz + 1)`.
The order of the two points is not important
in terms of minimum and maximum coordinates.

# Examples
```jldoctest
julia> mesh = meshbox(Point(0,0,0), Point(0.1,0.1,0.1), 2, 3, 4, Tet4);

julia> mesh.nodes[1:3]
3-element Array{Int64,1}:
 1
 2
 3

julia> mesh.element_ids[1:3]
3-element Array{Int64,1}:
 1
 2
 3

julia> mesh.elements[1:3, :]
3×4 Array{Int64,2}:
 1   2   5  17
 1   2  17  14
 1  14  17  13

julia> mesh.nsets
OrderedCollections.OrderedDict{Any,Any} with 9 entries:
  "xmin" => [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58]
  "xmid" => [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59]
  "xmax" => [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60]
  "ymin" => [1, 2, 3, 13, 14, 15, 25, 26, 27, 37, 38, 39, 49, 50, 51]
  "ymid" => Int64[]
  "ymax" => [10, 11, 12, 22, 23, 24, 34, 35, 36, 46, 47, 48, 58, 59, 60]
  "zmin" => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  "zmid" => [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]
  "zmax" => [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]

```
"""
function meshbox(
    p0::Point{3},
    p1::Point{3},
    nx::Integer,
    ny::Integer,
    nz::Integer,
    element_type::Union{Type{Tet4},
                  Type{Hex8}}
)

    @info string("Generating mesh of box spanned by points $p0 and $p1 "*
                 "with ($nx, $ny, $nz) elements along the (x, y, z) axes.")
    TicToc.tic()

    # minimum and maximum coordinates
    x0 = min(p0[1], p1[1])
    x1 = max(p0[1], p1[1])
    y0 = min(p0[2], p1[2])
    y1 = max(p0[2], p1[2])
    z0 = min(p0[3], p1[3])
    z1 = max(p0[3], p1[3])

    a = x0
    b = x1
    c = y0
    d = y1
    e = z0
    f = z1

    if (abs(x0 - x1) < eps() || abs(y0 - y1) < eps() || abs(z0 - z1) < eps())
        throw(ArgumentError("Box has zero width, height or depth. Check your dimensions"))
    end

    if (nx < 1 || ny < 1 || nz < 1)
        throw(ArgumentError("Non-positive number of vertices in some dimension: "*
                            "number of vertices must be at least 1 in each dimension"))
    end

    # Storage for vertex coordinates
    x = zeros(3)

    # Create vertices
    # FIXME: initialize with size (nx + 1)*(ny + 1)*(nz + 1)
    node_coordinates = Point{3}[]
    node_ids = Int[]

    vertex = 1
    for iz = 0:nz
        x[3] = e + iz*(f-e) / nz
        for iy = 0:ny
            x[2] = c + iy*(d-c) / ny
            for ix = 0:nx
                x[1] = a + ix*(b-a) / nx
                push!(node_coordinates, Point(x[1], x[2], x[3]))
                push!(node_ids, vertex)
                vertex += 1
            end
        end
    end

    if element_type == Tet4
        # Create 4-node tetrahedra
        # TODO: when mesh type has proper elements
        # elements = Vector{Tet4}(undef, 6*nx*ny*nz)  OR  Tet4[] and push
        element_connectivity = zeros(Int, 6*nx*ny*nz, 4)
        id = 0
        for iz = 1:nz
            for iy = 1:ny
                for ix = 1:nx
                    v1 = (iz - 1)*(nx + 1)*(ny + 1) + (iy - 1)*(nx + 1) + ix
                    v2 = v1 + 1
                    v3 = v1 + (nx + 1)
                    v4 = v2 + (nx + 1)
                    v5 = v1 + (nx + 1)*(ny + 1)
                    v6 = v2 + (nx + 1)*(ny + 1)
                    v7 = v3 + (nx + 1)*(ny + 1)
                    v8 = v4 + (nx + 1)*(ny + 1)

                    # Note that v1 < v2 < v3 < v4 < vmid
                    element_connectivity[id + 1, :] .= [v1, v2, v4, v8]
                    element_connectivity[id + 2, :] .= [v1, v2, v8, v6]
                    element_connectivity[id + 3, :] .= [v1, v6, v8, v5]
                    element_connectivity[id + 4, :] .= [v1, v4, v3, v8]
                    element_connectivity[id + 5, :] .= [v1, v7, v5, v8]
                    element_connectivity[id + 6, :] .= [v1, v3, v7, v8]

                    id += 6
                end
            end
        end
    elseif element_type == Hex8
        # Create 8-node hexahedra
        #
        # "engineering" order        "math" order
        #
        #       8--------7             7--------8
        #      /|       /|            /|       /|
        #     / |      / |           / |      / |
        #    5--+-----6  |          5--+-----6  |
        #    |  4-----|--3          |  3-----|--4
        #    | /      | /           | /      | /
        #    |/       |/            |/       |/
        #    1--------2             1--------2
        #
        element_connectivity = zeros(Int, nx*ny*nz, 8)
        cell = 1
        v = zeros(Int, 8)
        for iz = 1:nz
            for iy = 1:ny
                for ix = 1:nx
                    v[1] = ((iz - 1)*(ny + 1) + (iy - 1))*(nx + 1) + ix
                    v[2] = v[1] + 1
                    v[3] = v[1] + (nx + 1)
                    v[4] = v[2] + (nx + 1)
                    v[5] = v[1] + (nx + 1)*(ny + 1)
                    v[6] = v[2] + (nx + 1)*(ny + 1)
                    v[7] = v[3] + (nx + 1)*(ny + 1)
                    v[8] = v[4] + (nx + 1)*(ny + 1)
                    # Reorder elements
                    # TODO: add an option to switch between these node orders
                    if true
                        # "Engineering" node ordering
                        # (same as SofTiSim, Abaqus, ANSYS, VTK hexahedron)
                        v[3], v[4] = v[4], v[3]
                        v[7], v[8] = v[8], v[7]
                        # else
                        # "Math" node ordering
                        # (same as FEniCS, VTK voxel)
                    end
                    element_connectivity[cell, :] .= v
                    cell += 1
                end
            end
        end
    end

    element_ids = collect(1:length(element_connectivity))

    nsets = OrderedDict{String, Vector{Int}}()
    elsets = OrderedDict{String, Vector{Int}}()

    # Create node sets for all surfaces and mid planes
    x = getindex.(node_coordinates, 1)
    y = getindex.(node_coordinates, 2)
    z = getindex.(node_coordinates, 3)
    xm = (x1-x0)/2
    ym = (y1-y0)/2
    zm = (z1-z0)/2
    push!(nsets, "xmin" => findall(x0 - eps() .<= x .<= x0 + eps()))
    push!(nsets, "xmid" => findall(xm - eps() .<= x .<= xm + eps()))
    push!(nsets, "xmax" => findall(x1 - eps() .<= x .<= x1 + eps()))
    push!(nsets, "ymin" => findall(y0 - eps() .<= y .<= y0 + eps()))
    push!(nsets, "ymid" => findall(ym - eps() .<= y .<= ym + eps()))
    push!(nsets, "ymax" => findall(y1 - eps() .<= y .<= y1 + eps()))
    push!(nsets, "zmin" => findall(z0 - eps() .<= z .<= z0 + eps()))
    push!(nsets, "zmid" => findall(zm - eps() .<= z .<= zm + eps()))
    push!(nsets, "zmax" => findall(z1 - eps() .<= z .<= z1 + eps()))

    @info TicToc.toc()

    return Mesh{3}(
        node_coordinates,
        node_ids             = node_ids,
        node_sets            = nsets,
        element_connectivity = element_connectivity,
        element_ids          = element_ids,
        element_sets         = elsets)
end
