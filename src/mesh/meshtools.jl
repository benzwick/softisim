using NearestNeighbors
using Statistics: mean

"""
    mean_spacing(points)

Return the mean of the distance between each point and its closest neighbour
for the given list of `points`.
"""
function mean_spacing(points)
    tree = KDTree(points)

    # Find the closest 2 points because closest is node itself
    idxs, dists = knn(tree, points, 2)
    # then keep only the first column containg the nonzero distances
    dists = getindex.(dists, 1)

    return mean(dists)
end

"""
    nodes_on_bounds(mesh)

Return a collection of node sets that contain nodes on the bounding box
and mid planes of the `mesh`.

# Examples
```jldoctest
julia> mesh = meshbox(Point(0,0,0), Point(0.1,0.1,0.1), 2, 3, 4, Tet4);

julia> empty!(mesh.nsets);

julia> mesh.nsets
OrderedCollections.OrderedDict{Any,Any} with 0 entries

julia> merge!(mesh.nsets, nodes_on_bounds(mesh));

julia> mesh.nsets
OrderedCollections.OrderedDict{Any,Any} with 9 entries:
  "xmin" => [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58]
  "xmid" => [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 53, 56, 59]
  "xmax" => [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60]
  "ymin" => [1, 2, 3, 13, 14, 15, 25, 26, 27, 37, 38, 39, 49, 50, 51]
  "ymid" => Int64[]
  "ymax" => [10, 11, 12, 22, 23, 24, 34, 35, 36, 46, 47, 48, 58, 59, 60]
  "zmin" => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  "zmid" => [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]
  "zmax" => [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]

"""
function nodes_on_bounds(mesh)
    # TODO: replace duplicate code in meshbox.jl with call to this function.
    # TODO: add a test for this function.

    # Create node sets for all surfaces and mid planes
    x = getindex.(mesh.coordinates, 1)
    y = getindex.(mesh.coordinates, 2)
    z = getindex.(mesh.coordinates, 3)

    x0 = minimum(x)
    y0 = minimum(y)
    z0 = minimum(z)
    x1 = maximum(x)
    y1 = maximum(y)
    z1 = maximum(z)

    xm = (x1-x0)/2
    ym = (y1-y0)/2
    zm = (z1-z0)/2

    nsets = OrderedDict()
    push!(nsets, "xmin" => findall(x0 - eps() .<= x .<= x0 + eps()))
    push!(nsets, "xmid" => findall(xm - eps() .<= x .<= xm + eps()))
    push!(nsets, "xmax" => findall(x1 - eps() .<= x .<= x1 + eps()))
    push!(nsets, "ymin" => findall(y0 - eps() .<= y .<= y0 + eps()))
    push!(nsets, "ymid" => findall(ym - eps() .<= y .<= ym + eps()))
    push!(nsets, "ymax" => findall(y1 - eps() .<= y .<= y1 + eps()))
    push!(nsets, "zmin" => findall(z0 - eps() .<= z .<= z0 + eps()))
    push!(nsets, "zmid" => findall(zm - eps() .<= z .<= zm + eps()))
    push!(nsets, "zmax" => findall(z1 - eps() .<= z .<= z1 + eps()))

    return nsets
end

"""
    rotate_mesh!(mesh, R)

Rotate the `mesh` using the rotation matrix `R`.
"""
function rotate_mesh!(mesh, R)
    mesh.coordinates .= [R*p for p in mesh.coordinates]
end

"""
    rotate_mesh_x!(mesh, angle, units = :rad)

Rotate the `mesh` about the x axis by the given `angle`
(in `units` of `:rad` or `:deg`).
"""
function rotate_mesh_x!(mesh, angle, units = :rad)
    if units == :deg
        angle = deg2rad(angle)
    end
    Rx = [ 1           0           0
           0           cos(angle) -sin(angle)
           0           sin(angle)  cos(angle)]
    mesh.coordinates .= rotate_mesh!(mesh, Rx)
end

"""
    rotate_mesh_y!(mesh, angle, units = :rad)

Rotate the `mesh` about the y axis by the given `angle`
(in `units` of `:rad` or `:deg`).
"""
function rotate_mesh_y!(mesh, angle, units = :rad)
    if units == :deg
        angle = deg2rad(angle)
    end
    Ry = [ cos(angle)  0           sin(angle)
           0           1           0
          -sin(angle)  0           cos(angle)]
    mesh.coordinates .= rotate_mesh!(mesh, Ry)
end

"""
    rotate_mesh_z!(mesh, angle, units = :rad)

Rotate the `mesh` about the z axis by the given `angle`
(in `units` of `:rad` or `:deg`).
"""
function rotate_mesh_z!(mesh, angle, units = :rad)
    if units == :deg
        angle = deg2rad(angle)
    end
    Rz = [ cos(angle) -sin(angle)  0
           sin(angle)  cos(angle)  0
           0           0           1]
    mesh.coordinates .= rotate_mesh!(mesh, Rz)
end
