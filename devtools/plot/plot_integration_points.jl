# Use this in mtled.jl after create_qpoints!(...)

using PyPlot

# P = getfield.(cells, :point)
# p = collect(hcat(p...)')

n = glob.coord
plot3D(n[:,1], n[:,2], n[:,3], "bo")
plot3D(n[:,1], n[:,2], n[:,3], "bo")

q = getfield.(glob.qpoints, :coordinates)
q = collect(hcat(q...)')
plot3D(q[:,1], q[:,2], q[:,3], "rx")
