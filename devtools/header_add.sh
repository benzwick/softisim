#!/bin/bash

# Reference:
# https://stackoverflow.com/questions/151677/tool-for-adding-license-headers-to-source-files

for i in $(find ../src -name '*.jl');
do
    if ! grep -q Copyright $i
    then
        echo "Adding: $i"
        cat header.txt $i > $i.new_header && mv $i.new_header $i
    else
        echo "Skipping: $i"
    fi
done
