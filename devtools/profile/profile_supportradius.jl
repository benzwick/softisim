using BenchmarkTools
using Profile, ProfileView
using SofTiSim: Tet4

Profile.clear()
@profile supportradius(mesh.coordinates, cells);
Profile.print(maxdepth=8)
ProfileView.view()

@benchmark supportradius(mesh.coordinates, cells);
@benchmark supportradius(mesh.coordinates, cells::Vector{Tet4});

@time @profile supportradius(mesh.coordinates, cells);
@time @profile model[:support_radius_dilatation]*supportradius(mesh.coordinates, cells);

@time supportradius(mesh.coordinates, cells::Vector{Tet4});
@time supportradius(mesh.coordinates, cells);
