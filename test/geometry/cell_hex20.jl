using SofTiSim

# Volume

@test volume(reference_element(Hex20)) ≈ 8

# FIXME: add another test (these only check affine element, others may still fail)
points = [Point(0,   0, 0  ), Point( 1, 0,   0), Point(1,   3, 0  ), Point(0, 3,   0  ),
          Point(0,   0, 1  ), Point( 1, 0,   2), Point(1,   3, 2  ), Point(0, 3,   1  ),
          Point(0.5, 0, 0  ), Point( 1, 1.5, 0), Point(0.5, 3, 0  ), Point(0, 1.5, 0  ),
          Point(0.5, 0, 1.5), Point( 1, 1.5, 2), Point(0.5, 3, 1.5), Point(0, 1.5, 1  ),
          Point(0,   0, 0.5), Point( 1, 0,   1), Point(1,   3, 1  ), Point(0, 3,   0.5)]

@test volume(Hex20(1:20, points)) ≈ 4.5
