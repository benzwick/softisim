using SofTiSim

element_types = [
    Hex8,
    Hex20,
    Tet4,
    Tet10,
]

# Most of these functions take an actual element or an element type
# as an argument so cross-test to test both methods.

for type in element_types
    println("Element: $type")

    elem = reference_element(type)

    # Properties that are integers
    @test n_dims(type) isa Integer
    @test n_nodes(type) isa Integer
    @test n_sides(type) isa Integer
    @test n_edges(type) isa Integer
    @test n_children(type) isa Integer
    @test n_nodes_per_side(type) isa Integer
    @test n_nodes_per_edge(type) isa Integer
    # compare methods for elem and type
    @test n_dims(elem) == n_dims(type)
    @test n_nodes(elem) == n_nodes(type)
    @test n_sides(elem) == n_sides(type)
    @test n_edges(elem) == n_edges(type)
    @test n_children(elem) == n_children(type)
    @test n_nodes_per_side(elem) == n_nodes_per_side(type)
    @test n_nodes_per_edge(elem) == n_nodes_per_edge(type)
    if elem isa Edge  # 1D
        n_dims(elem) >= 1
    end
    if elem isa Face  # 2D
        n_dims(elem) >= 2
    end
    if elem isa Cell  # 3D
        n_dims(elem) >= 3
    end

    # Reference elements
    @test elem.nodes == reference_element(elem).nodes
    @test elem.nodes == 1:n_nodes(type)
    @test elem.nodes == 1:n_nodes(elem)

    @test elem.point == reference_element(elem).point
    @test elem.point == reference_coordinates(type)
    @test elem.point == reference_coordinates(elem)

    # Reference coordinates
    @test typeof(reference_coordinates(type)) == Vector{Vector{Float64}}
    @test typeof(reference_coordinates_as_matrix(type)) == Matrix{Float64}
    @test typeof(reference_coordinates_as_points(type)) == Vector{Point{n_dims(type)}}
    @test size(reference_coordinates_as_matrix(elem)) == (n_dims(type), n_nodes(type))
    @test size(reference_coordinates_as_matrix(type)) == (n_dims(elem), n_nodes(elem))
    @test reference_coordinates_as_matrix(elem) == hcat(reference_coordinates(type)...)
    @test reference_coordinates_as_matrix(type) == hcat(reference_coordinates(elem)...)
    # Check that the coordinates are stored in the same order in memory as matrix or vector
    @test reference_coordinates_as_matrix(elem)[:] == vcat(reference_coordinates(type)...)
    @test reference_coordinates_as_matrix(type)[:] == vcat(reference_coordinates(elem)...)
    @test reference_coordinates_as_matrix(elem)[:] == vcat(reference_coordinates_as_points(type)...)
    @test reference_coordinates_as_matrix(type)[:] == vcat(reference_coordinates_as_points(elem)...)

    @test length(reference_coordinates_as_points(elem)) == n_nodes(type)
    @test length(reference_coordinates_as_points(type)) == n_nodes(elem)

    # Elements from coordinates (as vectors, matrix, points)
    a = type(1:n_nodes(type), reference_coordinates(type))
    b = type(1:n_nodes(type), reference_coordinates_as_matrix(type))
    c = type(1:n_nodes(type), reference_coordinates_as_points(type))
    @test a.nodes == b.nodes
    @test b.nodes == c.nodes
    @test a.point == b.point
    @test b.point == c.point

    # Functions
    @test volume(elem) isa Float64
end
