import SofTiSim
import Dates

using SafeTestsets: @safetestset
using Test: @testset
const LONGER_TESTS = false

const GROUP = get(ENV, "SOFTISIM_TEST_GROUP", "All")
const is_APPVEYOR = Sys.iswindows() && haskey(ENV,"APPVEYOR")
const is_TRAVIS = haskey(ENV,"TRAVIS")

# Temporary directory for files generated during testing
# NOTE: User can set this environment variable to a different location
#       to keep the test output data.
if !haskey(ENV, "SOFTISIM_TEST_OUTPUT_DIR")
    if haskey(ENV, "GITLAB_CI")
        ENV["SOFTISIM_TEST_OUTPUT_DIR"] = mkdir(joinpath(SofTiSim.SOFTISIM_DIR, "test_output"))
    else
        if haskey(ENV, "SOFTISIM_TEST_CLEANUP_TMP_OUTPUT_DIR")
            cleanup = ENV["SOFTISIM_TEST_CLEANUP_TMP_OUTPUT_DIR"] == "0" ? false : true
        else
            cleanup = true
        end
        ENV["SOFTISIM_TEST_OUTPUT_DIR"] = mktempdir(prefix="softisim_", cleanup=cleanup)
    end
end
printstyled("SOFTISIM_TEST_OUTPUT_DIR: ",
            ENV["SOFTISIM_TEST_OUTPUT_DIR"],
            "\n", bold=true, color=:magenta)

# Overwrite directory: 1 -> true; else -> false (default)
if !haskey(ENV, "SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR")
    ENV["SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR"] = "0"
end
printstyled("SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR: ",
            ENV["SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR"],
            "\n", bold=true, color=:magenta)

# File for saving test results to be processed later
# NOTE: User can set this environment variable to a different location
#       to keep the test results.
if !haskey(ENV, "SOFTISIM_TEST_RESULTS_FILENAME")
    ENV["SOFTISIM_TEST_RESULTS_FILENAME"] = joinpath(ENV["SOFTISIM_TEST_OUTPUT_DIR"], "test_results.txt")
end
printstyled("SOFTISIM_TEST_RESULTS_FILENAME: ",
            ENV["SOFTISIM_TEST_RESULTS_FILENAME"],
            "\n", bold=true, color=:magenta)
# Write header
open(ENV["SOFTISIM_TEST_RESULTS_FILENAME"], "w") do io
    println(io, "SofTiSim: ", SofTiSim.SOFTISIM_VERSION_STRING)
    println(io, "Julia: ", VERSION)
    println(io, "Number of threads: ", Threads.nthreads())
    println(io, "Start time: ", string(Dates.now()))
    println(io, "Start testing...")
end

# This macro is used below to include tests
"""
    includetest(testname)

Include and time the test with `testname` and filename = `testname.jl`
relative to the current directory using `@safetestset`.
"""
macro includetest(testname)
    quote
        printstyled("Test: " * $testname * '\n', bold = true, color = :magenta)
        @time @safetestset $testname begin
            include($testname * ".jl")
        end
    end
end

#Start Test Script
@time begin

if GROUP == "All" || GROUP == "Geometry"
    @testset "geometry" begin
        @includetest "geometry/geometry"
        @includetest "geometry/cell_hex8"
        @includetest "geometry/cell_hex20"
        @includetest "geometry/cell_tet4"
        @includetest "geometry/cell_tet10"
    end
end

if GROUP == "All" || GROUP == "Mesh"
    @testset "mesh" begin
        @includetest "mesh/meshbox"
    end
end

if GROUP == "All" || GROUP == "Material"
    @testset "material" begin
        @includetest "material/elastic_moduli"
    end
end

if GROUP == "All" || GROUP == "Verification"
    @testset "verification" begin
        @includetest "postprocess/verification"
    end
end

if GROUP == "All" || GROUP == "Approx"
    @testset "approx" begin
        # Hex8
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss1_BROKEN"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss1"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss3_CONTINUE"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss3_DELETE"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss3_TERMINATE"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_hex8_gauss3"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_hex8_gauss3"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_hex20_gauss3"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_hex20_gauss5"
        @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss1_BROKEN"
        @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss1"
        # FIXME: this test causes segmentation fault
        # @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss3_CONTINUE"
        # FIXME: this test errors; see issue #24
        # @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss3_DELETE"
        @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss3_TERMINATE"
        @includetest "approx/cube_uniaxial_ogden_fe_lagrange_3d_hex8_gauss3"

        # Tet4
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_tet4_gauss1"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_tet4_gauss2"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_tet4_gauss2"
        @includetest "approx/cube_uniaxial_neohookean_fe_lagrange_3d_tet4_gauss3"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_tet10_gauss2"
        @includetest "approx/cylinder_compression_neohookean_fe_lagrange_3d_tet10_gauss3"

        # BasicQuarticSpline
        # Hex8
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_quarticspline_hex8_gauss3"
        @includetest "approx/cylinder_compression_neohookean_mls_3d_quarticspline_hex8_gauss3"
        # Tet4
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_quarticspline_tet4_gauss2_noebciem"
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_quarticspline_tet4_gauss2"
        @includetest "approx/cylinder_compression_neohookean_mls_3d_quarticspline_tet4_gauss2"
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_quarticspline_tet4_gauss1_usercells"
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_quarticspline_tet4_adaptive4_gauss2"

        # BasicGaussian
        # Hex8
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_gaussian_hex8_gauss3"
        @includetest "approx/cylinder_compression_neohookean_mls_3d_gaussian_hex8_gauss3"
        # Tet4
        @includetest "approx/cube_uniaxial_neohookean_mls_3d_gaussian_tet4_gauss2"
        @includetest "approx/cylinder_compression_neohookean_mls_3d_gaussian_tet4_gauss2"
    end
end

end # @time

# Write footer
open(ENV["SOFTISIM_TEST_RESULTS_FILENAME"], "a") do io
    println(io, "\n\nEnd time: ", string(Dates.now()))
    println(io, "Finished testing.")
end
