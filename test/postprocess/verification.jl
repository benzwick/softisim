using SofTiSim

height = 0.1;
stretch = 0.8;
nx = 8;
mesh = meshbox(Point(0,0,0), Point(2*height, 0.5*height, height), nx, nx, nx, SofTiSim.Tet4);
coord = mesh.coordinates;

# ------------------------------------------------------------------------------
# NeoHookean

material = NeoHookean(elastic_moduli(E=3000, nu=0.49));

# Displacement at the mid plane
u = exact_cube_uniaxial(coord, material, stretch);
@test all(isapprox.(u[mesh.nsets["zmid"], 3], height*(stretch-1)/2, atol=2*eps()))

# Cube shifted from the origin (displacements should be the same)
us = exact_cube_uniaxial([x + Point(-0.5, 1, 2) for x in coord], material, stretch);
@test all(isapprox(u, us, atol=10*eps()))

# ------------------------------------------------------------------------------
# Ogden

# alpha ≈ 2
material = Ogden(2.00000001, elastic_moduli(E=3000, nu=0.49));

# Displacement at the mid plane
u = exact_cube_uniaxial(coord, material, stretch);
@test all(isapprox.(u[mesh.nsets["zmid"], 3], height*(stretch-1)/2, atol=2*eps()))

# Cube shifted from the origin (displacements should be the same)
us = exact_cube_uniaxial([x + Point(-0.5, 1, 2) for x in coord], material, stretch);
@test all(isapprox(u, us, atol=10*eps()))

@test_throws ArgumentError exact_cube_uniaxial(coord, Ogden(2.0000001, material.mu, material.kappa), stretch);
